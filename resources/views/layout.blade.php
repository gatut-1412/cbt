<?php
$isLoggedIn = Session::get('username');
$isLoggedIn1 = Session::get('walikelas');
// var_dump($isLoggedIn1);
if ($isLoggedIn != null){    
?>

<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
    @include('partials.head')
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading kt-footer--fixed">

    <!-- begin:: Page -->
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="{{ url ('/') }}">
               <!--  <img alt="Logo" src="{{asset('assets/media/logos/logo-light.png')}}" width="150px" /> -->
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler" id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>

    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <!-- begin:: Aside -->

            @include('partials.menu')


           <!-- end:: Aside -->
           <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('partials.header')
            <!-- end:: Header -->

            <!-- smoy konten and sub header -->
            @yield('content')
            <!-- smoy konten and sub header -->

            <!-- begin:: Footer -->
            @include('partials.footer')                  
            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Quick Panel -->
<!-- smoy hapus -->
<!-- end::Quick Panel -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- begin::Sticky Toolbar -->
<!-- smoy hapus -->
<!-- end::Sticky Toolbar -->

<!-- begin::Demo Panel -->
<!-- smoy hapus -->
<!-- end::Demo Panel -->

<!--Begin:: Chat-->
<!-- smoy hapus -->
<!--ENd:: Chat-->

<!-- smoy footer js -->
@include('partials.footer_js')

<!-- smoy footer js -->
</body>

<!-- end::Body -->
</html>

<?php   }else {

    $url =url ('/login');
    header('Location: '.$url); /* Redirect browser */
    die();

}?> 