<!DOCTYPE html>
<html >
<head>
    <title>Soal CBT</title>
    <style>
        @page { margin-top: 10px;
        }
        body {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 9pt;
          margin-top: 5px ;
      }

  </style>

</head>
<body>
    <img src="{{asset('images/header.PNG')}}" width="100%">
    <table width="100%" >
        <thead>
            @foreach($detailsoal as $d)
            <tr>
                <td width="50%">Mata Pelajaran : {{ $d->judul_soal }}</td>
                <td width="50%">Durasi         : {{ $d->waktu }} Menit</td>
            </tr>
            <tr>
                <td width="50%">Jumlah Soal    : {{ $d->jumlah }} Soal</td>
                <td width="50%">Kelas          : </td>
            </tr>
             <tr>
                <td width="50%">Jenis Soal     : {{ $d->jenis_soal }}</td>
                <td width="50%"> </td>
            </tr>
            @endforeach
        </thead>
    </table>
    <table   >
        <thead>  
            @php $i=1 @endphp
            @foreach($soal as $p)

            <?php 
            $gambarsoalpg =  $p->gambar_soal ;
            if ($gambarsoalpg == NUll){
                ?>
                <tr>
                    <td width="10px" style="vertical-align: top;">{{ $i++ }}</td>
                    <td colspan="1" ><b>{{ $p->soal_pg }}</b></td>
                </tr>
                <?php 
                $a = $p->pilihan_a;

                $date_a =  substr($a,0,10);
                $format = 'd-m-Y' ;
                $dd = DateTime::createFromFormat($format, $date_a);
                $isdate = $dd && $dd->format($format) === $date_a;

                if (!$isdate){
                    ?>
                    <tr>
                        <td></td>
                        <td>A. {{ $p->pilihan_a }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>B. {{ $p->pilihan_b }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>C. {{ $p->pilihan_c }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>D. {{ $p->pilihan_d }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>E. {{ $p->pilihan_e }}</td>
                    </tr>

                <?php }
                else{ 
                    ?>
                    <tr>
                        <td></td>
                        <td>A. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_a)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>B. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_b)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>C. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_c)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>D. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_d)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>E. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_e)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>

                <?php } ?>

            <?php }
            else{ 
                ?>
                <tr>
                    <td width="10px" style="vertical-align: top;">{{ $i++ }}</td>
                    <td colspan="1" ><b>{{ $p->soal_pg }}</b> <br>
                        <img src="{{asset('soal_cbt/pg/soal/'.$p->gambar_soal)}}" style="vertical-align:middle" height="150px">
                    </td>
                </tr>
                <?php 
                $a = $p->pilihan_a;

                $date_a =  substr($a,0,10);
                $format = 'd-m-Y' ;
                $dd = DateTime::createFromFormat($format, $date_a);
                $isdate = $dd && $dd->format($format) === $date_a;

                if (!$isdate){
                    ?>
                    <tr>
                        <td></td>
                        <td>A. {{ $p->pilihan_a }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>B. {{ $p->pilihan_b }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>C. {{ $p->pilihan_c }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>D. {{ $p->pilihan_d }}</td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>E. {{ $p->pilihan_e }}</td>
                    </tr>

                <?php }
                else{ 
                    ?>
                    <tr>
                        <td></td>
                        <td>A. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_a)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>B. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_b)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>C. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_c)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>D. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_d)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>E. <br> <img src="{{asset('soal_cbt/pg/option/'.$p->pilihan_e)}}" style="vertical-align:middle" width="150px"></td>
                    </tr>

                <?php } ?>
            <?php } ?>

            @endforeach
        </thead>
    </table>
    <!--  <br><img src="{{asset('logo.png')}}" style="vertical-align:middle">  -->
    <?php exit(); ?>
</body>
</html>