<!DOCTYPE html>
<html >
<head>

    <title>Profile Guru</title>
    <style>
        @page { 
            /*margin-top: 10px;*/
            margin: 60px 25px;
        }
        body {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 9pt;
          margin-top: 5px;
          /*background-image:url("{{asset('images/LOGO_MUHAM.jpg')}}");*/
          /*background-opacity: 0.2;*/

      }
      #customers {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
      }
      #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
      }
      #customers tr:nth-child(even){background-color: #f2f2f2;}
      #customers tr:hover {background-color: #ddd;}
      #customers th {
          padding-top: 10px;
          padding-bottom: 10px;
          text-align: left;
          background-color: #4CAF50;
          color: white;
          font-size: 14;
      }

      header {
        position: fixed;
        top: -50px;
        left: 0px;
        right: 0px;
        height: 50px;
        vertical-align: middle;

        /** Extra personal styles **/
        /*background-color: #4CAF50;*/
        /*color: white;*/
        /*text-align: center;*/
        line-height: 35px;
    }

    footer {
        position: fixed; 
        bottom: -60px; 
        left: 0px; 
        right: 0px;
        height: 50px; 

        /** Extra personal styles **/
        background-color: grey;
        color: white;
        text-align: center;
        line-height: 35px;
    }

    /*.demo_wrap {
    opacity: 0.5;
    background-image:url("{{asset('images/LOGO_MUHAM.jpg')}}");*/

}
</style>

</head>
<body>
    <header>
        <img src="{{asset('images/LOGO_MUHAM.jpg')}}"  width="35px" height="35px"> PROFIL GURU SMA MUHAMADIAH 1 PRAMBANAN
    </header>

    <footer>
        Copyright &copy; SMA Muhamadiah 1 Prambanan 
    </footer>

    @foreach($guru as $d)
    <center>
        <img src="{{asset('assets/images/photo/guru/'.$d->pas_photo_guru)}}" width="140px" height="180px">
    </center>
    <div class="demo_wrap">
        <table id="customers">
            <tr>
                <th colspan="2">Data Pribadi</th>
            </tr>
            <tr>
                <td width="20%">NIP</td>
                <td> {{ $d->nip }} </td>

            </tr>
            <tr>
                <td>Nama Guru</td>
                <td>{{ $d->nama_guru }} </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>{{ $d->jenis_kelamin }} </td>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td>{{ $d->tempat_lahir }} </td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td>{{ $d->tanggal_lahir }} </td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>{{ $d->agama }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Kontak</th>
            </tr>
            <tr>
                <td>NIK</td>
                <td> {{ $d->nik }} </td>

            </tr>
            <tr>
                <td>Domisili</td>
                <td> {{ $d->domisili }} </td>

            </tr>
            <tr>
                <td>Provinsi</td>
                <td> {{ $d->provinsi }} </td>

            </tr>
            <tr>
                <td>Kabupaten</td>
                <td> {{ $d->kabupaten }} </td>

            </tr>
            <tr>
                <td>kecamatan</td>
                <td> {{ $d->kecamatan }} </td>

            </tr>
            <tr>
                <td>Desa/Kelurahan</td>
                <td> {{ $d->desakelurahan }} </td>

            </tr>
            <tr>
                <td>Alamat</td>
                <td> {{ $d->alamat }} </td>

            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $d->email_guru }} </td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>{{ $d->notelp }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Sekolah</th>
            </tr>
            <tr>
                <td>Status Kepegawaian</td>
                <td> {{ $d->status_kepegawaian }} </td>

            </tr>
            <tr>
                <td>Jenis PTK</td>
                <td>{{ $d->jenis_ptk }} </td>
            </tr>
            <tr>
                <td>Lembaga Pengangkatan</td>
                <td>{{ $d->lembaga_pengangkatan }} </td>
            </tr>
            <tr>
                <td>No SK</td>
                <td>{{ $d->no_sk }} </td>
            </tr>
            <tr>
                <td>Tanggal Surat</td>
                <td>{{ $d->tgl_surat }} </td>
            </tr>
            <tr>
                <td>NUPTK</td>
                <td>{{ $d->nuptk }} </td>
            </tr>
            <tr>
                <td>TMT Tugas</td>
                <td>{{ $d->tmt_tugas }} </td>
            </tr>
            <tr>
                <td>Tugas Tambahan</td>
                <td>{{ $d->tugas_tambahan }} </td>
            </tr>
            <tr>
                <td>Mengajar</td>
                <td>{{ $d->mengajar }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Keluarga</th>
            </tr>
            <tr>
                <td>Status Pernikahan</td>
                <td> {{ $d->status_nikah }} </td>
            </tr>
            <tr>
                <td>Nama Istri/ Suami</td>
                <td>{{ $d->nama_istrisuami }} </td>
            </tr>
            <tr>
                <td>Nama Ibu</td>
                <td>{{ $d->nama_ibu }} </td>
            </tr>
        </table>
        <!-- <img src="{{asset('images/header.PNG')}}" width="100%"> -->
        @endforeach
    </div>
    <!--  <br><img src="{{asset('logo.png')}}" style="vertical-align:middle">  -->
    <?php //exit(); ?>
</body>
</html>