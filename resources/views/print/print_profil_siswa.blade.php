<!DOCTYPE html>
<html >
<head>

    <title>Profile Siswa</title>
    <style>
        @page { 
            /*margin-top: 10px;*/
            margin: 60px 25px;
        }
        body {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 9pt;
          margin-top: 5px;
          /*background-image:url("{{asset('images/LOGO_MUHAM.jpg')}}");*/
          /*background-opacity: 0.2;*/
        }
        #customers {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
          #customers tr:nth-child(even){background-color: #f2f2f2;}
          #customers tr:hover {background-color: #ddd;}
          #customers th {
              padding-top: 10px;
              padding-bottom: 10px;
              text-align: left;
              background-color: #4CAF50;
              color: white;
              font-size: 14;
          }

      header {
        position: fixed;
        top: -50px;
        left: 0px;
        right: 0px;
        height: 50px;
        vertical-align: middle;

        /** Extra personal styles **/
        /*background-color: #4CAF50;*/
        /*color: white;*/
        /*text-align: center;*/
        line-height: 35px;
    }

    footer {
        position: fixed; 
        bottom: -60px; 
        left: 0px; 
        right: 0px;
        height: 50px; 

        /** Extra personal styles **/
        background-color: grey;
        color: white;
        text-align: center;
        line-height: 35px;
    }

    /*.demo_wrap {
    opacity: 0.5;
    background-image:url("{{asset('images/LOGO_MUHAM.jpg')}}");*/

}
</style>

</head>
<body>
    <header>
        <img src="{{asset('images/LOGO_MUHAM.jpg')}}"  width="35px" height="35px"> PROFIL SISWA SMA MUHAMADIAH 1 PRAMBANAN
    </header>

    <footer>
        Copyright &copy; SMA Muhamadiah 1 Prambanan 
    </footer>

    @foreach($siswa as $d)
    <center>
        <img src="{{asset('assets/images/photo/siswa/'.$d->pas_photo_siswa)}}" width="140px" height="180px">
    </center>
    <div class="demo_wrap">
        <table id="customers">
            <tr>
                <th colspan="2">Data Pribadi</th>
            </tr>
            <tr>
                <td width="20%">NIS</td>
                <td> {{ $d->nis }} </td>

            </tr>
            <tr>
                <td>Nama Siswa</td>
                <td>{{ $d->nis }} </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>{{ $d->jenis_kelamin }} </td>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td>{{ $d->tempat_lahir }} </td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td>{{ $d->tanggal_lahir }} </td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>{{ $d->agama }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Kontak</th>
            </tr>
            <tr>
                <td>Alamat</td>
                <td> {{ $d->alamat }} </td>

            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $d->email_siswa }} </td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>{{ $d->no_telp_siswa }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Sekolah</th>
            </tr>
            <tr>
                <td>Asal Sekolah</td>
                <td> {{ $d->asal_sekolah }} </td>

            </tr>
            <tr>
                <td>Tanggal Dikelas</td>
                <td>{{ $d->tanggal_dikelas }} </td>
            </tr>
            <tr>
                <td>Kelas</td>
                <td>{{ $d->dikelas }} </td>
            </tr>
            <tr>
                <th colspan="2">Data Keluarga</th>
            </tr>
            <tr>
                <td>Status Keluarga</td>
                <td> {{ $d->asal_sekolah }} </td>

            </tr>
            <tr>
                <td>Anak Ke</td>
                <td>{{ $d->anak_ke }} </td>
            </tr>
            <tr>
                <td>Nama Ayah</td>
                <td>{{ $d->nama_ayah }} </td>
            </tr>
            <tr>
                <td>Nama Ibu</td>
                <td>{{ $d->nama_ibu }} </td>
            </tr>
            <tr>
                <td>Alamat Orang Tua</td>
                <td>{{ $d->alamat_orangtua }} </td>
            </tr>
            <tr>
                <td>Pekerjaan Ayah</td>
                <td>{{ $d->pekerjaan_ayah }} </td>
            </tr>
            <tr>
                <td>Pekerjaan Ibu</td>
                <td>{{ $d->pekerjaan_ibu }} </td>
            </tr>
        </table>
        <!-- <img src="{{asset('images/header.PNG')}}" width="100%"> -->
        @endforeach
    </div>
    <!--  <br><img src="{{asset('logo.png')}}" style="vertical-align:middle">  -->
    <?php //exit(); ?>
</body>
</html>