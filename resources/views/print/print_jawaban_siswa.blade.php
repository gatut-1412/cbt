<!DOCTYPE html>
<html >
<head>
    <title>Print Jawaban Siswa</title>
    <style>
        @page { margin-top: 10px;
        }
        body {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 9pt;
          margin-top: 5px ;
      }

  </style>

</head>
<body>
    <img src="{{asset('images/header.PNG')}}" width="100%">
    <table width="100%" >
        <thead>
            @foreach($query as $d)
            <tr>
                <td width="50%">Nama Siswa     : {{ $d->nama_siswa }}</td>
                <td width="50%">Mata Pelajaran : {{ $d->nama_pelajaran }}</td>

            </tr>
            <tr>
                <td width="50%">Kelas          : {{ $d->nama_kelas }}</</td>
                <td width="50%">Jenis Soal     : {{ $d->jenis_soal }}</td>
            </tr>
            <tr>
                <td width="50%">Durasi         : {{ $d->waktu }} Menit</td>
                <td width="50%"></td>
            </tr>

            @endforeach
        </thead>
    </table>
    <table   >
        <thead>  
            @php $i=1 @endphp
            <?php $k = 0; ?>
            @foreach($query1 as $p)
            <?php if ($p->gambar_soal==Null)
            {
                ?>
                <tr>
                    <td width="10px" style="vertical-align: top;">{{ $i++ }}</td>
                    <td colspan="1" ><b>{{ $p->soal_pg }}</b></td>
                </tr>

                <tr>
                    <td></td>
                    <?php 
                    $ab = $jawabann[$k];

                    $date_a =  substr($ab,0,10);
                    $format = 'd-m-Y' ;
                    $dd = DateTime::createFromFormat($format, $date_a);
                    $isdate = $dd && $dd->format($format) === $date_a;
                    if (!$isdate){
                        ?>
                        <td>{{$a[$k]}}. {{ $jawabann[$k] }}</td>
                        <?php
                    }else {
                        ?>
                        <td>{{$a[$k]}}. <br>
                            <img src="{{asset('soal_cbt/pg/option/'.$jawabann[$k])}}" style="vertical-align:middle" width="150px"></td>
                    <?php }
                    ?>
                    
                </tr>
            <?php }

            else{ ?>
                <tr>
                    <td width="10px" style="vertical-align: top;">{{ $i++ }}</td>
                    <td colspan="1" ><b>{{ $p->soal_pg }} <br> 
                        <img src="{{asset('soal_cbt/pg/soal/'.$p->gambar_soal)}}" style="vertical-align:middle" height="150px"></b></td>
                    </tr>

                    <tr>
                        <td></td>
                        <?php 
                        $ab = $jawabann[$k];

                        $date_a =  substr($ab,0,10);
                        $format = 'd-m-Y' ;
                        $dd = DateTime::createFromFormat($format, $date_a);
                        $isdate = $dd && $dd->format($format) === $date_a;
                        if (!$isdate){
                            ?>
                            <td>{{$a[$k]}}. {{ $jawabann[$k] }}</td>
                            <?php
                        }else {
                            ?>
                            <td>{{$a[$k]}}. <br>
                                <img src="{{asset('soal_cbt/pg/option/'.$jawabann[$k])}}" style="vertical-align:middle" width="150px"></td>
                        <?php }
                        ?>
                    </tr>
                <?php } ?>

                <?php $k++; ?>
                @endforeach
            </thead>
        </table>

  <?php  exit();?>      

    </body>
    </html>