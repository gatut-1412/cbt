
<?php
$isuser = Session::get('level');
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Subheader -->
		<!-- end:: Subheader -->

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Catatan Wali Kelas
						</h3>
					</div>
				</div>

				<!--begin::Form-->
				@foreach($catatan_walkes as $kl)
				<div class="kt-portlet__body">
					<div class="form-group row" >
						<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $kl->semester}}" disabled>
						</div>
					</div>

					<input type="hidden" class="form-control form-control-sm" id="id_master_kelas4" value="{{ $kl->id_master_kelas}}" disabled >

					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="kt-portlet">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						@if($message = Session::get('success'))
						<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
							<div class="alert-text">{{ $message }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
						@endif
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="#tambah_catatan_walkes" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									Tambah Data
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!-- <form class="kt-form kt-form--label-right" id="form_users" method="post" action="update"> -->
						{{ csrf_field() }}
						<!--begin: Datatable -->
						<table class="table table-striped- table-bordered table-hover table-checkable" id="table_catatan_walikelas" >
							<thead>
								<tr>
									<th>No</th>
									<th>NIS</th>
									<th width="20%">Nama Siswa</th>
									<th>L/K</th>
									<th>Catatan Wali Kelas</th>	
									<th width="17%">Action</th>										
								</tr>
							</thead>
						</table>

						<!--end: Datatable -->
					</div>
					<!-- <div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" class="btn btn-brand">Simpan</button>
									<button type="reset" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div> -->
					<!-- </form> -->

					<!--end::Form-->
				</div>

				<!--end::Portlet-->
			</div>

			<!-- end:: Content -->
		</div>


		<div class="modal fade" id="tambah_catatan_walkes" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" >Tambah Catatan Walikelas</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="tambah">
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="form-group row kt-margin-t-20">
								<label class="col-form-label col-lg-3 col-sm-12">Siswa</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<select class="form-control kt-select2" id="siswa_cat_walkes" name="siswa">
										<option value=""></option>
										@foreach ($siswa as $p)
										
										{{ $a = $p -> id_kelas_siswa }}
										
										<option value="{{ $p -> id_kelas_siswa }}" 
											@foreach ($siswa2 as $p2) 
											{{ $b = $p2 -> id_kelas_siswa }} 
											<?php if ($a == $b) : echo"disabled"; endif ?> 
											@endforeach 
										> 
											{{ $p -> nama_kelas }}  -  {{ $p -> nis }}  -  {{ $p -> nama_siswa }} 
										</option>
										
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Catatan Walikelas</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<textarea class="form-control" id="message-text" name="cat_walkes" ></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Tambah">
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="update_catatan_walkes" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" >Update Catatan Walikelas</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					
					<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="update">
					 {{ csrf_field() }}
						<div class="modal-body">
							<div class="form-group row kt-margin-t-20">
								<label class="col-form-label col-lg-3 col-sm-12">Siswa</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<!-- <select class="form-control kt-select2" id="siswa_cat_walkes" name="siswa">
									</select> -->
									 <input type="text" disabled="" class="form-control" id="smoy2" name="smoy2" value="">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Catatan Walikelas</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<textarea class="form-control" id="smoy1" name="cat_walkes" rows="10"></textarea>
								</div>
							</div>
							<input type="hidden" name="idcatwalkes" id="txt_userid" value="0">
						</div>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="delete_catatan_walkes" class="modal fade" role="dialog">
			<div class="modal-dialog ">
				<!-- Modal content-->
				<form id="deleteForm" method="post" action="delete">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title text-center">DELETE CONFIRMATION</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
						</div>
						<div class="modal-body">
							{{ csrf_field() }}
							<p class="text-center">Apakah Kamu Akan Menghapus Data Ini ?</p>
							<input type="hidden" name="idcatwalkes" id="idcat" value="0">
						</div>
						<div class="modal-footer">
							<center>
								<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-danger" >Yes, Delete</button>
							</center>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endsection

