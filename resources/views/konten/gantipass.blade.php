@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--begin::Portlet-->
<!-- 							<div class="row">
								<div class="col-lg-6"> -->

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Ganti password
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="form_users" method="POST" action="{{ url('/gantiPassword/proses') }}">
											{{ csrf_field() }}
											<div class="kt-portlet__body">
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Password baru *</label>
													<div class="col-lg-98 col-md-8 col-sm-12">
														<div class="input-group" id="show_hide_password">
														<input required minlength=8 type="password" class="form-control" name="passwordba1" id="passwordba1" placeholder="Enter new password">
															<span class="input-group-text"><a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a></span>	
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Password baru *</label>
													<div class="col-lg-8 col-md-8 col-sm-12">
														<div class="input-group" id="show_hide_password">
														<input required minlength=8 type="password" class="form-control" name="passwordba2" id="passwordba2" placeholder="Enter new password">
															<span class="input-group-text"><a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a></span>	
														</div> 	
											      <span id="error_password"></span></div>
											  
													</div>

												</div>
												
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" id="submit" class="btn btn-brand">Save</button>
															<a href="{{ url ('/') }}" class="btn btn-secondary">
															Cancel
															</a>
														</div>
													</div>
												</div>
											</div>
											
																		<div class="col-sm-9">
								<input hidden="true" value="{{Session::get('USERNAME')}}" name="username"   />
							</div>
							
										</form>

										<!--end::Form-->
									</div>

									<!--end::Portlet-->
<!-- 								</div>
							</div> -->
						</div>

						<!-- end:: Content -->
					</div>
					
					<script type="text/javascript">

		$(document).ready(function(){

				$('#passwordba2').keyup(function(){
					$('#submit').attr('disabled', false);
					var error_password = '';
					var password = $('#passwordba2').val();
					var password1 = $('#passwordba1').val();

					if(password != password1)
					{
						$('#error_password').html('<label class="text-danger">Password tidak sama</label>');
						$('#passwordb2').addClass('has-error');
						$('#submit').attr('disabled', 'disabled');
					}else{
						$('#error_password').html('<label class="text-success"></label>');
						$('#passwordba2').removeClass('has-error');
					}												
				});


		});
		
			$(document).ready(function(){

				$('#passwordba1').keyup(function(){
					$('#submit').attr('disabled', false);
					var error_password = '';
					var password = $('#passwordba2').val();
					var password1 = $('#passwordba1').val();

					if(password1 != password)
					{
						$('#error_password').html('<label class="text-danger">Password tidak sama</label>');
						$('#passwordb2').addClass('has-error');
						$('#submit').attr('disabled', 'disabled');
					}else{
						$('#error_password').html('<label class="text-success"></label>');
						$('#passwordba2').removeClass('has-error');
					}												
				});


		});
	</script>
@endsection