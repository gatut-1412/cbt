
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Subheader -->
		<!-- end:: Subheader -->

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Mapping Ekstrakulikuler
						</h3>
					</div>
				</div>
			</div>
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						@if($message = Session::get('success'))
						<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
							<div class="alert-text">{{ $message }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
						@endif
						@if($message = Session::get('error'))
						<div class="alert alert-solid-danger alert-bold" role="alert" style="margin-top: 20px">
							<div class="alert-text">{{ $message }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
						@endif
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="#add_mappingeskul" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									Tambah Data
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_mappingeskul">
						<thead>
							<tr>
								<th>no</th>
								<th>Nama Eskul</th>
								<th>Nama Siswa</th>
								<th>Pembina</th>
								<th>Keterangan</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>

		<!-- end:: Content -->
	</div>
	<!--begin::Modal-->
	<div class="modal fade" id="add_mappingeskul" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Tambah Mapping Ekstrakulikuler</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>
				<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="/mappingeskul/tambah">
					{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Ektrakulikuler</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2" id="addmappingeskul1" name="ideskul">
									<option value=""></option>
									@foreach ($eskul as $e)
									<option value="{{ $e -> id_eskul }} "> 
										{{ $e -> nama_eskul }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Siswa</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2" id="addmappingeskul2" name="idsiswa">
									<option value=""></option>
									@foreach ($siswa as $g)
									<option value="{{ $g -> id_kelas_siswa }} "> 
										{{ $g -> nis }} &ensp;|&ensp; {{ $g -> nama_siswa }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control"  name="keterangan" rows="6"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Tambah">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="update_mappingeskul" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Update Mapping Eskul</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>

				<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="/mappingeskul/update">
					{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Ektrakulikuler</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2 smoy1" id="editmappingeskul1" name="ideskul">
									<option value=""></option>
									@foreach ($eskul as $e)
									<option value="{{$e -> id_eskul}}"> 
										{{ $e -> nama_eskul }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Siswa</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2 smoy2" id="editmappingeskul2" name="idsiswa">
									<option value=""></option>
									@foreach ($siswa as $g)
									<option value="{{$g -> id_kelas_siswa}}"> 
										{{ $g -> nis }} &ensp;|&ensp; {{ $g -> nama_siswa }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control" id="smoy3" name="keterangan" rows="6"></textarea>
							</div>
						</div>
					</div>
					<input type="hidden" name="idmappingeskul" id="idmappingeskul" value="0">
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Update">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="delete_mappingeskul" class="modal fade" role="dialog">
		<div class="modal-dialog ">
			<!-- Modal content-->
			<form id="deleteForm" method="post" action="/mappingeskul/delete">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-center">DELETE CONFIRMATION</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					<div class="modal-body">
						{{ csrf_field() }}
						<p class="text-center">Apakah Kamu Akan Menghapus Data Ini ?</p>
						<input type="hidden" name="idmappingeskul" id="idmappingeskul1" value="0">
					</div>
					<div class="modal-footer">
						<center>
							<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-danger" >Yes, Delete</button>
						</center>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--end::Modal-->
	@endsection