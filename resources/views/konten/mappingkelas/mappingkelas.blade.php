<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Subheader -->
		<!-- end:: Subheader -->

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Mapping Kelas
						</h3>
					</div>
				</div>
			</div>
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						@if($message = Session::get('success'))
						<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
							<div class="alert-text">{{ $message }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
						@endif
						@if($message = Session::get('error'))
						<div class="alert alert-solid-danger alert-bold" role="alert" style="margin-top: 20px">
							<div class="alert-text">{{ $message }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
						@endif
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="#kt_select2_modal" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									Tambah Data
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_mapping_kelas">
						<thead>
							<tr>
								<th>no</th>
								<th>Tahun Ajaran</th>
								<th>Kelas</th>
								<th>Semester</th>
								<th>Wali Kelas</th>
								<th>Nama Guru</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>

		<!-- end:: Content -->
	</div>
	<!--begin::Modal-->
	<div class="modal fade" id="kt_select2_modal" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Tambah Data Mapping Kelas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>
				<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="/mappingkelas/tambah">
					{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Tahun Ajaran</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2" id="kt_select2_1_modal" name="tahun_ajaran">
									<option value=""></option>
									@foreach ($tahun_ajaran as $p)
									<option value="{{ $p -> id_tahun_ajaran }} "> 
										{{ $p -> tahun_ajaran }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Nama Kelas</label>
							<div class="input-group col-lg-9 col-md-9 col-sm-12">
								<div class="input-group-prepend">
									<select  class="form-control kt-select2" id="kt_select2_10" name="kelas" >
										<option value="X">X</option>
										<option value="XI">XI</option>
										<option value="XII">XII</option>

									</select>
								</div>
								<input type="text" class="form-control col-lg-6" name="namakelas" placeholder="Masukan Nama Kelas"></input>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Semester</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select  class="form-control kt-select2" id="semester" name="semester" >
									<option value="Semester 1">Semester 1</option>
									<option value="Semester 2">Semester 2</option>

								</select>
							</div>
						</div>
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Wali Kelas</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2" id="kt_select2_2_modal" name="walikelas">
									<option value=""></option>
									@foreach ($guru as $g)
									<option value="{{ $g -> id_guru }} "> 
										{{ $g -> nip }} &ensp;|&ensp; {{ $g -> nama_guru }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Tambah">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="update_mappingkelas" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Update Kelas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>

				<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="/mappingkelas/update">
					{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Tahun Ajaran</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2 smoyx1" id="edittahunajaran" name="idtahunajaran">
									@foreach ($tahun_ajaran as $p)
									<option value="{{$p -> id_tahun_ajaran}}"> 
										{{ $p -> tahun_ajaran }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Nama Kelas</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="smoy2" name="namakelas" value="">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Semester</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select  class="form-control kt-select2 smoyx3" id="editsemester" name="semester" >
									<option value="Semester 1">Semester 1</option>
									<option value="Semester 2">Semester 2</option>

								</select>
							</div>
						</div>
						<div class="form-group row kt-margin-b-20">
							<label class="col-form-label col-lg-3 col-sm-12">Wali Kelas</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2 smoyx4" id="editwalkes" name="idguru">
									@foreach ($guru as $g)
									<option value="{{ $g -> id_guru }}"> 
										{{ $g -> nip }} &ensp;|&ensp; {{ $g -> nama_guru }}  
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<input type="hidden" name="idmappingkelas" id="idmappingkelas" value="0">
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Update">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="delete_mappingkelas" class="modal fade" role="dialog">
		<div class="modal-dialog ">
			<!-- Modal content-->
			<form id="deleteForm" method="post" action="/mappingkelas/delete">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-center">DELETE CONFIRMATION</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					<div class="modal-body">
						{{ csrf_field() }}
						<p class="text-center">Apakah Kamu Akan Menghapus Data Ini ?</p>
						<input type="hidden" name="idmappingkelas" id="idmappingkelas1" value="0">
					</div>
					<div class="modal-footer">
						<center>
							<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-danger" >Yes, Delete</button>
						</center>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--end::Modal-->
	@endsection