
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

								<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Penilaian
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										@foreach($penilaian as $kl)
										<form class="kt-form kt-form--label-right" id="form_users" method="post" action="users/tambahuser/proses">
											{{ csrf_field() }}
											<input type="hidden" class="form-control form-control-sm" id="id_master_kelas3" value="{{ $kl->id_master_kelas}}" disabled >

											<div class="kt-portlet__body">
												<div class="form-group row" >
													<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
														<!-- <span class="form-text text-muted">Silahkan isi username anda.</span> -->
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm""  name="nama" value="{{ $kl->semester}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
													</div>
												</div>
											</div>
									<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="table_siswa_penilaian" >
										<thead>
											<tr>
												<th>No</th>
												<th>NIS</th>
												<th>Nama Siswa</th>
												<th>L/K</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>6</th>
												<th>7</th>
												<th>8</th>
												<th>9</th>
												<th>10</th>
												<th>Rata-Rata (RNPH)</th>
												<th>NPTS</th>
												<th>NPAS/NPAT</th>
												<th>NILAI AKHIR</th>
												
											</tr>
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand">Simpan</button>
															<button type="reset" class="btn btn-secondary">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									@endforeach
										<!--end::Form-->
									</div>

									<!--end::Portlet-->
						</div>

						<!-- end:: Content -->
					</div>
@endsection