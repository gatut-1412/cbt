
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
<!--begin::Modal-->
							<div class="modal fade" id="kt_select2_modal" role="dialog" aria-labelledby="" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" >Tambah Guru Mengajar</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" class="la la-remove"></span>
											</button>
										</div>
										<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="public/mappingkelas/tambah">
											{{ csrf_field() }}
											<div class="modal-body">
												<div class="form-group row kt-margin-t-20">
													<label class="col-form-label col-lg-3 col-sm-12"></label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<select class="form-control kt-select2" id="kt_select2_1_modal" name="nama_">
															<option value=""></option>
															@foreach ($guru as $g)
															<option value="{{ $g -> id_guru }} "> 
																{{ $g -> nama_guru }}  
															</option>
															@endforeach
														</select>
													</div>
												</div>
											
											
											</div>
											<div class="modal-footer">
												<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
												<input type="submit" class="btn btn-success" value="Tambah">
											</div>
										</form>
									</div>
								</div>
							</div>

							<!--end::Modal-->