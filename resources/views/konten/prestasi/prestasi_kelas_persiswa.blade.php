@extends('layout')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Data Prestasi Per Kelas
					</h3>
				</div>
				<!-- <div class="kt-portlet__head-label">
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" data-placement="left">
						<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<button type="button" class="btn btn-outline-brand btn-elevate btn-pill"><i class="la la-cog"></i> Option</button>
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
							
							<ul class="kt-nav">
								<li class="kt-nav__head">
									SIlahkan pilih option:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">

								</li>
								<li class="kt-nav__item">

									<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
									<span class="kt-nav__link-text">Hapus Soal CBT</span>
									
								</li>

								<li class="kt-nav__item">

								</li>


								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Update Soal</a>
								</li>
							</ul>
						</div>
					</div>
				</div> -->
		</div>

		<!--begin::Form-->
		@foreach($kelas as $so)
		<input type="hidden"  id="id_master_kelas_prestasi" value="{{ $so->id_master_kelas}}" disabled>
		<div class="kt-portlet__body">
			<div class="form-group row" >
				<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
				<div class="col-lg-4 ">
					<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $so->tahun_ajaran}}" disabled>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
				<div class="col-lg-4 ">
					<input type="text" class="form-control form-control-sm" name="password" value="{{ $so->semester}}" disabled>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-lg-2" style="text-align: left">Kelas</label>
				<div class="col-lg-4 ">
					<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->nama_kelas}} Menit" disabled>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-lg-2" style="text-align: left">Wali Kelas</label>
				<div class="col-lg-4 ">
					<input type="text" class="form-control form-control-sm" name="email" value="{{ $so->nama_guru}}" disabled>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="kt-portlet">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				@if($message = Session::get('success'))
				<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
					<div class="alert-text">{{ $message }}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="la la-close"></i></span>
						</button>
					</div>
				</div>
				@endif
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="la la-download"></i> Export
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
									<li class="kt-nav__section kt-nav__section--first">
										<span class="kt-nav__section-text">Choose an option</span>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link" id="export_print">
											<i class="kt-nav__link-icon la la-print"></i>
											<span class="kt-nav__link-text">Print</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link" id="export_copy">
											<i class="kt-nav__link-icon la la-copy"></i>
											<span class="kt-nav__link-text">Copy</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link" id="export_excel">
											<i class="kt-nav__link-icon la la-file-excel-o"></i>
											<span class="kt-nav__link-text">Excel</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link" id="export_csv">
											<i class="kt-nav__link-icon la la-file-text-o"></i>
											<span class="kt-nav__link-text">CSV</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link" id="export_pdf">
											<i class="kt-nav__link-icon la la-file-pdf-o"></i>
											<span class="kt-nav__link-text">PDF</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						&nbsp;
						<!-- <a href="#tambah_catatan_walkes" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							Tambah Data
						</a> -->
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="table_prestasi_kelas_persiswa" >
				<thead>
					<tr>
						<th>No</th>
						<th>NIS</th>
						<th>Nama Siswa</th>
						<th>Prestasi 1</th>
						<th>Keterangan 1</th>	
						<th>Prestasi 2</th>
						<th>Keterangan 2</th>
						<th>Prestasi 3</th>
						<th>Keterangan 3</th>	
						<th>Prestasi 4</th>
						<th>Keterangan 4</th>
						<th>Prestasi 5</th>
						<th>Keterangan 5</th>	
						<th>Action</th>										
					</tr>
				</thead>
			</table>
			<!--end: Datatable -->
		</div>
	</div>

	<!--end::Portlet-->
</div>

<!-- end:: Content -->
</div>
<!-- Tambah Modal HTML -->



@endsection