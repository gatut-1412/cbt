
@extends('layout')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

	<!-- begin:: Subheader -->
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		<div class="kt-portlet kt-portlet--mobile">

		</div>
		<div class="col-xl-4">

			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
						<h3 class="kt-portlet__head-title">Informasi Pengerjaan</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						Toolbar
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-portlet__content">
						@foreach ($detailpeng as $g)

						Dear <b>{{ $g -> nama_siswa }}</b> ,<br><br>

						Untuk mengikuti Online Test <b>{{ $g -> jenis_soal }}</b>, Saudara dapat mengunjungi halaman <b><a href="https://cbt.sma-muh1pramb.sch.id" target="blank">https://cbt.sma-muh1pramb.sch.id</a></b>.<br><br>

						Berikut username dan password yang dapat Saudara gunakan untuk login Online Test.<br><br>
						Username : <b>{{ $g -> nis }}</b><br>
						Password : <b>{{ $g -> nis }}</b><br><br>

						Adapun jadwal untuk Siswa melaksanakan Online Test adalah :<br><br>
						Tanggal : {{ $g -> tanggal }}<br>
						Jam : <b>{{ $g -> jam }} </b>- Selesai (WIB)<br>

						Siswa hanya dapat mengikuti Online Test pada waktu yang telah ditentukan tersebut. Tidak ada penjadwalan ulang untuk pelaksanaan Online Test. Apabila Saudara ada kendala dalam mengakses halaman web / ada pertanyaan boleh hubungi  Operator.<br><br>

						<!-- E-mail ini dikirimkan secara otomatis oleh sistem, mohon tidak me-reply e-mail ini. Jika ada pertanyaan lebih lanjut dapat mengirimkan email ke : info.recruit@inti.co.id. Terima kasih. --><br><br>



						Yogyakarta<br>
						<b>SMA MUHAMMADIYAH 1 PRAMBANAN	</b>
						@endforeach										
					</div>
				</div>
			</div>

			<!--end::Portlet-->
		</div>
	</div>

	<!-- end:: Content -->
</div>
<!--begin::Modal-->



@endsection
