
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

								<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Tambah/Edit Kelas Siswa
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										@foreach($kelassiswa as $kl)
										<form class="kt-form kt-form--label-right" id="form_users" method="post" action="updateKelasSiswa2">
											{{ csrf_field() }}

											<div class="kt-portlet__body">
												<div class="form-group row" >
													<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
														<!-- <span class="form-text text-muted">Silahkan isi username anda.</span> -->
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm""  name="nama" value="{{ $kl->semester}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>

													</div>
												</div>
											</div>
									<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="table_siswa2" >

										@if(count($errors) > 0)
										    <div class="alert alert-solid-danger alert-bold" role="alert" style="margin-top: 20px">
												<div class="alert-text">Gagal Upload !</div>
										    	<ul>
										      		@foreach($errors->all() as $error)
										      		<li>{{ $error }}</li>
										      		@endforeach
										     	</ul>
										    </div>
										@endif

										@if($message = Session::get('success'))
											<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
												<div class="alert-text">{{ $message }}
													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true"><i class="la la-close"></i></span>
													</button>
												</div>
											</div>
										@endif

										@if($message = Session::get('nochange'))
											<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
												<div class="alert-text">{{ $message }}
													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true"><i class="la la-close"></i></span>
													</button>
												</div>
											</div>
										@endif

										

										<input type="hidden" id="id_master_kelas" class="form-control form-control-sm" name="id_master_kelas" value="{{ $kl->id_master_kelas}}" disabled>


										<thead>
											<tr>
												<th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
												
												<th>No</th>
												<th>NIS</th>
												<th>Nama Siswa</th>
												<th>Jenis Kelamin</th>
												<th>Angkata</th>
												<th>Kelas Sekarang</th>
												<th hidden>id master kelas </th>
												
											</tr>
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand">Simpan</button>
															<button type="reset" class="btn btn-secondary">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									@endforeach
										<!--end::Form-->
									</div>

									<!--end::Portlet-->
						</div>

						<!-- end:: Content -->
					</div>
@endsection