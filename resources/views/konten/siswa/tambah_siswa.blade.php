
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); 
}
?>
@extends('layout')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

	<!-- begin:: Subheader -->
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

		<!--begin::Portlet-->
		<div class="row">
			<!--begin::Form-->
			<div class="col-md-6">
				<form class="kt-form kt-form--label-right" id="form_siswa" method="post" action="siswa/tambahsiswa/proses" enctype="multipart/form-data" autocomplete="off">
					{{ csrf_field() }}
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Tambah Siswa
								</h3>
							</div>
						</div>

						<div class="kt-portlet__body">
							<div class="form-group">
								<center>
									<div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
										<img id="pasphotoguru" class="kt-avatar__holder" src="{{asset('assets/images/pas_photo.jpg')}}" alt="your image" />
										<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
											<i class="fa fa-pen"></i>
											<input type="file" onchange="readURL(this);" accept=".png, .jpg, .jpeg, .PNG, .JPG, .JPEG, image/*" oninvalid="setCustomValidity('Format yang anda masukan salah')"  name="pasphotosiswa">
										</label>
										<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
											<i class="fa fa-times"></i>
										</div>
									</span>
								</center>
							</div>
							<div class="form-group">
								<label>NIS <label style="color: red">*</label></label>
								<input type="text" class="form-control {{ $errors->get('nis') ? 'is-invalid' : '' }} " name="nis" placeholder="Masukan Nomor Induk Siswa" value="{{old('nis')}}" required>
								@foreach($errors->get('nis') as $error)
								<div class="invalid-feedback">{{ $error }}</div> 
								<!-- <span class="help-block"></span> -->
								@endforeach
							</div>
							<div class="form-group">
								<label>Nama Siswa <label style="color: red">*</label></label>
								<input type="text" class="form-control"  name="namasiswa" value="{{old('namasiswa')}}" placeholder="Masukan Nama Siswa" required>
							</div>
							<div class="form-group">
								<label>Jenis Kelamin <label style="color: red">*</label></label>
								<select class="form-control" id="exampleSelect1" name="jk">
									<option value="L" <?php if  (old('jk') == "L") : echo"selected"; endif?> >Laki-Laki</option>
									<option value="P" <?php if  (old('jk') == "P") : echo"selected"; endif?>>Perempuan</option>
								</select>
							</div>
							<div class="form-group">
								<label>Tempat Lahir <label style="color: red">*</label></label>
								<input type="text" class="form-control" placeholder="Masukan tempat lahir" value="{{old('tmptlahir')}}" name="tmptlahir">
							</div>
							<div class="form-group">
								<label>Tanggal Lahir <label style="color: red">*</label> </label>
								<input type="text" class="form-control" name="tgllahir" value="{{old('tgllahir')}}" id="kt_datepickersiswa" readonly placeholder="Tanggal Lahir" />
							</div>
							<div class="form-group">
								<label>Email address</label>
								<input type="text" class="form-control" aria-describedby="emailHelp" name="emailsiswa" value="{{old('emailsiswa')}}" placeholder="Enter email" >
								<!-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> -->
							</div>
							<div class="form-group">
									<label>No Telepon Siswa</label>
									<input type="text" class="form-control"  name="notelp" value="{{old('notelp')}}" placeholder="Enter No telp" >
									<!-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> -->
							</div>
							<div class="form-group">
								<label>Agama</label>
								<select class="form-control" id="exampleSelect1" name="agama">
									<option value="Islam" <?php if  (old('agama') == "Islam") : echo"selected"; endif?> >Islam</option>
									<option value="Protestan" <?php if  (old('agama') == "Protestan") : echo"selected"; endif?>>Protestan</option>
									<option value="Katholik" <?php if  (old('agama') == "Katholik") : echo"selected"; endif?>>Katholik</option>
									<option value="Budha" <?php if  (old('agama') == "Budha") : echo"selected"; endif?>>Budha</option>
									<option value="Konghucu" <?php if  (old('agama') == "Konghucu") : echo"selected"; endif?>>Konghucu</option>
									<option value="Hindu" <?php if  (old('agama') == "Hindu") : echo"selected"; endif?>>Hindu</option>
									<option value="Kristen" <?php if  (old('agama') == "Kristen") : echo"selected"; endif?>>Kristen</option>
								</select>
							</div>
							<div class="form-group">
								<label>Status Keluarga</label>
								<input type="text" class="form-control" name="statuskel" value="{{old('statuskel')}}" placeholder="Masukan status keluarga">
							</div>
							<div class="form-group">
								<label>Anak Ke</label>
								<input type="text" class="form-control" name="anakke" value="{{old('anakke')}}" placeholder="Masukan anak ke">
							</div>

						</div>									
					</div>
					<!--end::Portlet-->
				</div>

				<div class="col-md-6">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
						</div>
						<div class="kt-portlet__body">
							<div class="form-group">
								<label>Alamat </label>
								<textarea class="form-control" id="exampleTextarea" rows="3" name="alamatsiswa">{{old('alamatsiswa')}}</textarea>
							</div>
							<div class="form-group">
								<label>Asal Sekolah</label>
								<input type="text" class="form-control" name="asalsekolah" value="{{old('asalsekolah')}}" placeholder="Masukan asal sekolah">
							</div>
							<div class="form-group">
								<label>Dikelas</label>
								<input type="text" class="form-control" name="dikelas" value="{{old('dikelas')}}" placeholder="Masukan dikelas">
							</div>
							<div class="form-group">
								<label>Tanggal Di Kelas</label>
								<input type="text" class="form-control" name="tgldikelas" value="{{old('tgldikelas')}}" id="kt_datepickersiswatgldikelas" readonly placeholder="Select date" />
							</div>
							<div class="form-group">
								<label>Nama Ayah</label>
								<input type="text" class="form-control" name="namaayah" value="{{old('namaayah')}}" placeholder="Masukan nama ayah">
							</div>
							<div class="form-group">
								<label>Nama Ibu</label>
								<input type="text" class="form-control" name="namaibu" value="{{old('namaibu')}}" placeholder="Masukan nama ibu">
							</div>
							<div class="form-group">
								<label>Alamat Orang Tua</label>
								<textarea class="form-control" id="exampleTextarea" rows="3" name="alamatortu" >{{old('alamatortu')}}</textarea>
							</div>
							<div class="form-group">
								<label>Pekerjaan Ayah</label>
								<input type="text" class="form-control" name="pekayah" value="{{old('pekayah')}}" placeholder="Masukan pekerjaan ayah">
							</div>
							<div class="form-group">
								<label>Pekerjaan Ibu</label>
								<input type="text" class="form-control" name="pekibu" value="{{old('pekibu')}}" placeholder="Masukan pekerjaan ibu">
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<button type="submit" class="btn btn-primary">Submit</button>
								<a href="back">
									<button type="button" class="btn btn-secondary">Cancel</button>
								</a>
							</div>
						</div>

					</div>
					<!--end::Portlet-->
				</div>

			</form>
			<!--end::Form-->
		</div>
	</div>

	<!-- end:: Content -->
</div>
@endsection