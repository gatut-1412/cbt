
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			@foreach($datatugas as $kertu)
			<div class="row">
				<div class="col-md-6">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Detail Tugas
								</h3>
							</div>
						</div>

						<div class="kt-portlet__body">
							<div class="form-group">
								<label>Mata Pelajaran :</label>
								<input type="email" class="form-control" value="{{ $kertu->nama_pelajaran}}" aria-describedby="emailHelp" disabled="">
							</div>
							<div class="form-group">
								<label>Nama Guru :</label>
								<input type="email" class="form-control" aria-describedby="emailHelp" value="{{ $kertu->nama_guru}}" disabled="">
							</div>
							<div class="form-group">
								<label>Judul tugas</label>
								<textarea class="form-control" id="exampleTextarea" rows="3" disabled="">{{ $kertu->judul_tugas}}</textarea>
							</div>
							<div class="form-group">
								<label>Detail Tugas : </label> <br>
								<label> {!! $kertu->detail_tugas !!}</label>

							</div>
						</div>
						<!--end::Form-->
					</div>
				</div>

				<div class="col-md-6">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__body">
							<div class="form-group">
								<label>Tanggal Mulai</label>
								<input type="email" class="form-control" aria-describedby="emailHelp" disabled="" value="{{ $kertu->start_date1}}">
							</div>
							<div class="form-group">
								<label>Tanggal Selesai</label>
								<input type="email" class="form-control" aria-describedby="emailHelp" disabled="" value="{{ $kertu->end_date1}}">
							</div>
							<?php
							$allowed = array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG','GIF','MP4','mp4');
							$filename1 = $kertu->gambartugasonline1;
							$filename2 = $kertu->gambartugasonline2;
							$filename3 = $kertu->gambartugasonline3;
							$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
							$ext2 = pathinfo($filename2, PATHINFO_EXTENSION);
							$ext3 = pathinfo($filename3, PATHINFO_EXTENSION);
							if (in_array($ext1, $allowed)) {
								?>
								<div class="form-group">
									<label>Attachment 1</label>
									<a href="{{asset('data_file/'.$kertu->gambartugasonline1)}}" class="fancybox">
										<img src="{{asset('data_file/'.$kertu->gambartugasonline1)}}" style="width:150px; height: 150px">
									</a>
								</div>
								<div class="form-group">
									<a href="{{asset('data_file/'.$kertu->gambartugasonline1)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
										Download Attachment 1
									</a>
								</div>
								<?php
							}
							if (!in_array($ext1, $allowed))
							{
								?>
								<?php
								if ($kertu->gambartugasonline1 != null)
								{
									?>
									<div class="form-group">
										<label>Nama File 1</label><br>
										<Labe> {{$kertu->gambartugasonline1}} </Labe>
									</div>
									<div class="form-group">
										<a href="{{asset('data_file/'.$kertu->gambartugasonline1)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
											Download Attachment 1
										</a>
									</div>
									<?php
								}
								else
								{
									
								}
							}
							if (in_array($ext2, $allowed)) {
								?>
								<div class="form-group">
									<label>Attachment 2</label>
									<a href="{{asset('data_file/'.$kertu->gambartugasonline2)}}" class="fancybox">
										<img src="{{asset('data_file/'.$kertu->gambartugasonline2)}}" style="width:150px; height: 150px">
									</a>
								</div>
								<div class="form-group">
									<a href="{{asset('data_file/'.$kertu->gambartugasonline2)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
										Download Attachment 2
									</a>
								</div>
								<?php
							}
							if (!in_array($ext2, $allowed)) 
							{
								?>
								<?php
								if ($kertu->gambartugasonline2 != null)
								{
									?>
									<div class="form-group">
										<label>Nama File 2</label><br>
										<Labe> {{$kertu->gambartugasonline2}} </Labe>
									</div>
									<div class="form-group">
										<a href="{{asset('data_file/'.$kertu->gambartugasonline2)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
											Download Attachment 2
										</a>
									</div>
									<?php
								}
								else
								{
									
								}
							}
							if (in_array($ext3, $allowed)) {
								?>
								<div class="form-group">
									<label>Attachment 3</label>
									<a href="{{asset('data_file/'.$kertu->gambartugasonline3)}}" class="fancybox">
										<img src="{{asset('data_file/'.$kertu->gambartugasonline3)}}" style="width:150px; height: 150px">
									</a>
								</div>
								<div class="form-group">
									<a href="{{asset('data_file/'.$kertu->gambartugasonline3)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
										Download Attachment 3
									</a>
								</div>
								<?php
							}
							if (!in_array($ext3, $allowed)) 
							{
								?>
								<?php
								if ($kertu->gambartugasonline3 != null)
								{
									?>
									<div class="form-group">
										<label>Nama File 3</label><br>
										<Labe> {{$kertu->gambartugasonline3}} </Labe>
									</div>
									<div class="form-group">
										<a href="{{asset('data_file/'.$kertu->gambartugasonline3)}}" download class="btn btn-brand btn-elevate btn-icon-sm">
											Download Attachment 3
										</a>
									</div>
									<?php
								}
								else
								{
									
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			<!--end::Portlet-->
		</div>

		<!-- end:: Content -->
	</div>
	@endsection