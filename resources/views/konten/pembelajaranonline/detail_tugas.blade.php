
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

								<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Detail Tugas
														@foreach($detailtugasguru as $kl)
														<input type="hidden" class="form-control form-control-sm" id="kelas4" value="{{ $kl->id_tugas_online}} ">
														
														@endforeach 
														<input type="hidden" class="form-control form-control-sm" id="kelas3" value="{{ $id_tgs}} ">
												</h3>
											</div>
										</div>

										<!--begin::Form-->
<!-- 										@foreach($detailtugasguru as $kl)
											<div class="kt-portlet__body">
												<div class="form-group row" >
													<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $kl->semester}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Mata Pelajaran</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
													</div>
												</div>
											</div>
											@endforeach -->

									<div class="kt-portlet__body">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="table_detailtugas" >
										<thead>
											<tr>
												<th>No</th>
												<th>NIS</th>
												<th>Nama Siswa</th>
												<!-- <th>J/P</th> -->
												<th>Status</th>
												<th>Progres</th>
												<th>Tanggal Upload</th>
												<th>File upload</th>									
											</tr>
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
									
										<!--end::Form-->
									</div>

									<!--end::Portlet-->
						</div>

						<!-- end:: Content -->
					</div>


					<!--begin::Modal-->
					<div class="modal fade" id="lihatfile" role="dialog" aria-labelledby="" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" >Tambah Guru Mengajar</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" class="la la-remove"></span>
									</button>
								</div>
								<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="public/matpelajaran/tambah">
									{{ csrf_field() }}
									<div class="modal-body">
										<div class="form-group row kt-margin-t-20">
											<label class="col-form-label col-lg-3 col-sm-12">Nama Mata Pelajaran</label>
											<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="namapel" required>
											</div>
										</div>											
																
									</div>
									<div class="modal-footer">
										<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
										<input type="submit" class="btn btn-success" value="Tambah">
									</div>
								</form>
							</div>
						</div>
					</div>
					<!--end::Modal-->


@endsection