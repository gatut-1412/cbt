<?php
$isuser = Session::get('level');
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Tambah Tugas
						</h3>
					</div>
				</div>

				<!--begin::Form-->
				@foreach($tambah_tugas as $kl)
				<input type="hidden" class="form-control form-control-sm" id="id_master_kelas1" value="{{ $kl->id_master_kelas}}" disabled >
				<div class="kt-portlet__body">
					<div class="form-group row" >
						<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $kl->semester}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
						</div>
					</div>
					<!-- <div class="form-group row">
					<label class="col-form-label col-lg-2" style="text-align: left">Mata Pelajaran</label>
					<div class="col-lg-4 ">
					<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
					</div>
				</div> -->

				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<a href="#addTugas" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
								<i class="la la-plus"></i>
								Tambah Tugas
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="kt-portlet__body">
				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-checkable" id="table_datatugas" >
					<thead>
						<tr>
							<th>No</th>
							<th>Subject Tugas</th>
							<th>Mata Pelajaran</th>
							<!-- <th>Data Uploas</th> -->
							<th>Tanggal</th>
							<th>Status Sekarang</th>
							<th>Action</th>
							<th>Detail</th>											
						</tr>
					</thead>
				</table>

				<!--end: Datatable -->
			</div>
			@endforeach
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
	<!-- end:: Content -->
	</div>

	<!-- Tambah Modal HTML -->
	<div id="addTugas" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form  method="post" action="tambah_tugas/proses" enctype="multipart/form-data"  autocomplete="off">
					{{ csrf_field() }}
					<div class="modal-header">						
						<h4 class="modal-title">Tambah Data Tugas Online</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Mata Pelajaran</label>
							<select class="form-control kt-select2" id="tambah_catatan_bk_1" name="mapel">
								@foreach ($gurmapel as $p)
								<option value="{{ $p -> id_pelajaran }} "> 
									{{ $p -> nama_pelajaran }}
								</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Judul Tugas</label>
							<input type="text" class="form-control" name="judul" required>
						</div>
							<!-- <div class="form-group">
							<label for="exampleSelects">Kategori Tugas</label>
							<select class="form-control form-control-sm" id="exampleSelects">
							<option>Upload File</option>
							<option>Esay</option>
							</select>
							</div> -->			
						<div class="form-group">
							<label>Detail Tugas</label>
							<textarea class="form-control" id="detailtgsonline" id="exampleTextarea" name="detail"></textarea>
						</div>
							<!-- <div class="form-group">
								<label>Soal Esay</label>
								<textarea id="addtugasonline" class="form-control" id="exampleTextarea" name="esay"></textarea>
						</div> -->
						<div class="form-group">
							<label>File 1</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="file">
								<label class="custom-file-label" for="customFile" >Choose file</label>
							</div>
						</div>
						<div class="form-group">
							<label>File 2</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile1" name="file1">
								<label class="custom-file-label" for="customFile" >Choose file</label>
							</div>
						</div>
						<div class="form-group">
							<label>File 3</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile2" name="file2">
								<label class="custom-file-label" for="customFile" >Choose file</label>
							</div>
						</div>
						<div class="form-group">
							<label>Tanggal Tugas</label>
							<div class="input-daterange input-group" >
								<input type="text" class="form-control" id="startdate" readonly data-z-index="1100" placeholder="Select start date & time" name="start" />
								<div class="input-group-append">
									<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
								</div>
								<input type="text" class="form-control" id="enddate" readonly data-z-index="1100" placeholder="Select end date & time" name="end" />
							</div>
						</div>
						@foreach($tambah_tugas as $as)
						<input type="hidden" class="form-control" name="idmasterkelas" value="{{ $as->id_master_kelas}}"  required>
						@endforeach
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Tambah">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endsection