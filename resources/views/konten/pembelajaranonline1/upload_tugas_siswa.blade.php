
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		@foreach($kerjakantugas as $kertu)
		<div class="row">
			<div class="col-md-6">
				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Lihat Tugas Siswa & Kerjakan
							</h3>
						</div>
					</div>
										
					<div class="kt-portlet__body">
						<div class="form-group">
							<label>Mata Pelajaran :</label>
							<input type="email" class="form-control" value="{{ $kertu->nama_pelajaran}}" aria-describedby="emailHelp" disabled="">
						</div>
						<div class="form-group">
							<label>Nama Guru :</label>
							<input type="email" class="form-control" aria-describedby="emailHelp" value="{{ $kertu->nama_guru}}" disabled="">
						</div>
						<div class="form-group">
							<label>Judul tugas</label>
							<textarea class="form-control" id="exampleTextarea" rows="3" disabled="">{{ $kertu->judul_tugas}}</textarea>
						</div>
						<div class="form-group">
							<label>Detail Tugas</label>
							<textarea class="form-control" id="exampleTextarea" rows="3" disabled="">{{ $kertu->detail_tugas}}</textarea>
						</div>
					</div>
					<!--end::Form-->
				</div>
			</div>

			<div class="col-md-6">
				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__body">
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="email" class="form-control" aria-describedby="emailHelp" disabled="" value="{{ $kertu->start_date}}">
						</div>
						<div class="form-group">
							<label>Tanggal Selesai</label>
							<input type="email" class="form-control" aria-describedby="emailHelp" disabled="" value="{{ $kertu->end_date}}">
						</div>
						<div class="form-group">
							<label>Attachment</label>
							<img src="{{asset('data_file/Shamber-01.jpg')}}" >
						</div>
						<div class="form-group">
							<a href="#kt_select2_modal" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
								Download Attachment
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Upload Tugas
							</h3>
						</div>
					</div>
					<form class="kt-form kt-form--label-right" enctype="multipart/form-data" id="form_users" method="post" action="uploadproses">
						{{ csrf_field() }}
					<div class="kt-portlet__body">
						<div class="form-group">
							<label>Catatan Siswa</label>
							<textarea class="form-control" id="exampleTextarea" name="catatansiswa" rows="3" ></textarea>
							<input type="hidden" name="iddto" value="{{ $kertu->id_detail_tugas_online}}">
						</div>
						<div class="form-group">
							<label>Upload File Tugas</label>
							<div></div>
							<div class="custom-file">

								<input type="file" class="custom-file-input" id="customFile" name="file">
								<label class="custom-file-label" for="customFile">Pilih file</label>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
										<button type="submit" class="btn btn-brand">Simpan</button>
										<button type="reset" class="btn btn-secondary">Cancel</button>
									
								</div>
							</div>
						</div>						
					</div>
					</form>
				</div>
			@endforeach	
		<!--end::Portlet-->
	</div>

	<!-- end:: Content -->
</div>
@endsection