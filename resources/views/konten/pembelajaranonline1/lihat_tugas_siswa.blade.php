
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

								<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Lihat Tugas Siswa
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										@foreach($lihattugass as $kl)
											<div class="kt-portlet__body">
												<div class="form-group row" >
													<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Nama Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Semester</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm""  name="nama" value="{{ $kl->semester}}" disabled>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-2" style="text-align: left">Guru Wali Kelas</label>
													<div class="col-lg-4 ">
														<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_guru}}" disabled>
													</div>
												</div>
											</div>
										@endforeach

									<div class="kt-portlet__body">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="table_lihat" >
										<thead>
											<tr>
												<th>No</th>
												<th>Mata Pelajaran</th>
												<th>Judul Tugas</th>
												<th>Tanggal Mulai</th>
												<th>Tanggal Selesai</th>
												<th>Status Tugas</th>
												<th>Detail</th>											
											</tr>
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
									
										<!--end::Form-->
									</div>

									<!--end::Portlet-->
						</div>

						<!-- end:: Content -->
					</div>
@endsection