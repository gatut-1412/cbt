
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Subheader -->
		<!-- end:: Subheader -->

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<!-- <div class="alert alert-light alert-elevate" role="alert">
				<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
				<div class="alert-text">
					DataTables has the ability to read data from virtually any JSON data source that can be obtained by Ajax. This can be done, in its most simple form, by setting the ajax option to the address of the JSON data source.
					See official documentation <a class="kt-link kt-font-bold" href="https://datatables.net/examples/data_sources/ajax.html" target="_blank">here</a>.
				</div>
			</div>  -->
			@if($message = Session::get('success'))
			<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
				<div class="alert-text">{{ $message }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"><i class="la la-close"></i></span>
					</button>
				</div>
			</div>
			@endif
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Data Users
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="{{ url ('/users/tambahuser') }}" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									New Record
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_users">
						<thead>
							<tr>
								<th>no</th>
								<th>Username</th>
								<th>Nama</th>
								<th>E-mail</th>
								<th>Level</th>
								<th>Created by</th>
								<th>Updated by</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>
		<!-- end:: Content -->
	</div>

	<div class="modal fade" id="users_update" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Update Pelajaran</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>

				<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="/users/update">
					{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Username</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="username" name="username" required>
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Password</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control"  name="password" value="">
								<span class="form-text text-muted">Kosongkan jika tidak ada perubahan password.</span>
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Nama Lengkap</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="nama" name="nama" value="" required>
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Email</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="email" name="email" value="">
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">Level</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<select class="form-control kt-select2 smoy2" id="level" name="level">
									<option>ADMIN</option>
									<option>GURU</option>
									<option>SISWA</option>
									
								</select>
							</div>
						</div>	
						<input type="hidden" name="idusers" id="idusers1" value="0">
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Update">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="users_nonactive" class="modal fade" role="dialog">
		<div class="modal-dialog ">
			<!-- Modal content-->
			<form id="deleteForm" method="post" action="/users/delete">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-center">DELETE CONFIRMATION</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					<div class="modal-body">
						{{ csrf_field() }}
						<p class="text-center">Apakah Kamu Akan Menghapus Data Ini ?</p>
						<input type="hidden" name="idusers" id="idusers2" value="0">
					</div>
					<div class="modal-footer">
						<center>
							<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-danger" >Yes, Delete</button>
						</center>
					</div>
				</div>
			</form>
		</div>
	</div>
	@endsection