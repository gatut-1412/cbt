
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--begin::Portlet-->
<!-- 							<div class="row">
								<div class="col-lg-6"> -->

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Tambah Users
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="form_users" method="post" action="users/tambahuser/proses">
											{{ csrf_field() }}
											<div class="kt-portlet__body">
<!-- 												<div class="form-group form-group-last kt-hide">
													<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
														<div class="alert-icon"><i class="flaticon-warning"></i></div>
														<div class="alert-text">
															Oh snap! Change a few things up and try submitting again.
														</div>
														<div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
												</div> -->
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Username *</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="username" placeholder="Enter your username">
														<!-- <span class="form-text text-muted">Silahkan isi username anda.</span> -->
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="password" placeholder="Enter your password">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Nama Lengkap *</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="nama" placeholder="Enter your full name">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="email" placeholder="Enter your email">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Level *</label>
													<div class="col-lg-9 col-md-9 col-sm-12 form-group-sub">
														<select class="form-control" name="level">
															<option value="">Select</option>
															<option>ADMIN</option>
															<option>GURU</option>
															<option>SISWA</option>
														</select>
													</div>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand">Simpan</button>
															<button type="reset" class="btn btn-secondary">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

									<!--end::Portlet-->
<!-- 								</div>
							</div> -->
						</div>

						<!-- end:: Content -->
					</div>
@endsection