@extends('layout')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Daftar Hasil Test 
					</h3>
				</div>
				<div class="kt-portlet__head-label">
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" data-placement="left">
						<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<button type="button" class="btn btn-outline-brand btn-elevate btn-pill"><i class="la la-cog"></i> Option</button>

							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									SIlahkan pilih option:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									
									<a href="" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Download Template Upload</span>
									</a>
									
								</li>
								<li class="kt-nav__item">
									<a href="#hapussoalcbt" class="kt-nav__link" data-toggle="modal">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Hapus Soal CBT</span>
									</a>
								</li>

								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Update Soal</a>
								</li>
							</ul>

							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			@foreach($cbt as $so)
			<input type="hidden" class="form-control form-control-sm" id="idmappingcbt" value="{{ $so->id_mapping_cbt}}" disabled >
		
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row" >
							<label class="col-form-label col-lg-3" style="text-align: left">Mata Pelajaran</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $so->judul_soal}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Nama Guru</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="password" value="{{ $so->nama_guru}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Jenis Ujian</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->jenis_soal}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Kelas</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->nama_kelas}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Semester</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->tahun_ajaran}} {{ $so->semester}}" disabled>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Waktu</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="email" value="{{ $so->waktu}} Menit" disabled>
							</div>
						</div>
						@foreach($cbt2 as $so2)
						<div class="form-group row" >
							<label class="col-form-label col-lg-3" style="text-align: left">Jumlah Soal</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $so2->jumlah}}" disabled>
							</div>
						</div>
						@endforeach
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Nilai Tertinggi</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="password" value="{{ $so->nilaimax}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Nilai Terendah</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->nilaimin}}" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="text-align: left">Nilai Rata-rata</label>
							<div class="col-lg-6 ">
								<input type="text" class="form-control form-control-sm" name="email" value="{{ $so->nilairatarata}}" disabled>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="kt-portlet">
							<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<!-- <a href="#tambah_soal_cbt" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									Tambah Data
								</a> -->
							</div>
						</div>
					</div>
				</div>
			<div class="kt-portlet__body">
				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-checkable" id="table_detailhasilujian" >
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Siswa</th>
							<th>Jumlah Benar</th>
							<th>Nilai</th>
							<th>Nilai Bobot</th>
<th>Mulai</th>
<th>Selesai</th>
							<th>Waktu Mengerjakan</th>
							<th>Action</th>										
						</tr>
					</thead>
				</table>
				<!--end: Datatable -->
			</div>
		</div>

		<!--end::Portlet-->
	</div>

	<!-- end:: Content -->
</div>

	@endsection