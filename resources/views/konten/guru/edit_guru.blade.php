
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); 
}
?>
@extends('layout')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!--begin::Form-->
	@foreach($guru as $sis)
	<form class="kt-form kt-form--label-left" id="form_guru" method="post" action="edit/proses" enctype="multipart/form-data"  autocomplete="off">
		{{ csrf_field() }}
		<div class="kt-subheader   kt-grid__item" id="kt_subheader">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Edit Guru
				</h3>
				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<div class="kt-subheader__group" id="kt_subheader_search">
					<span class="kt-subheader__desc" id="kt_subheader_total">
					</span>
				</div>
			</div>
			<div class="kt-subheader__toolbar">
				<a href="guru" class="btn btn-default btn-bold">
				Back </a>
				<div class="btn-group">
					<button type="submit" class="btn btn-brand btn-bold">
					Simpan </button>
				</div>
			</div>
		</div>
		<br><br>
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Personal Informasi
						</h3>
					</div>
				</div>		
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-xl-3 col-lg-3 col-form-label">Photo</label>
						<div class="col-lg-9 col-xl-6">
							<div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
								<img id="pasphotoguru" class="kt-avatar__holder" 
								<?php if  ($sis->pas_photo_guru == null) :  ?> src="{{asset('assets/images/pas_photo.jpg')}}"
								<?php 	else   : ?>  src="{{asset('assets/images/photo/guru/'.$sis->pas_photo_guru)}}"
								<?php endif ?> 
								alt="your image" />
								<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
									<i class="fa fa-pen"></i>
									<input type="file" onchange="readURL(this);" accept=".png, .jpg, .jpeg, .PNG, .JPG, .JPEG, image/*" oninvalid="setCustomValidity('Format yang anda masukan salah')"  name="pasphoto">
								</label>
								<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
									<i class="fa fa-times"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group row validated" >
						<label class="col-form-label col-lg-3 col-sm-12">NIP <label style="color: red">*</label></label>
						<div class="col-lg-9 col-md-9 col-sm-12" >
							<input type="text" class="form-control " name="nip" value="{{ $sis->nip }}"  readonly placeholder="Masukan NIP (Nomor Induk Pegawai)" required >
							<input type="hidden" class="form-control " name="idguru" value="{{ $sis->id_guru }}"  readonly>
							<!-- <span class="form-text text-muted">Silahkan isi username anda.</span> -->
							<!-- <div class="invalid-feedback">Shucks, check the formatting of that and try again.</div>  -->
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Nama Guru <label style="color: red">*</label></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" value="{{ $sis->nama_guru }}" name="namaguru" placeholder="Masukan Nama Guru" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Jenis Kelamin <label style="color: red">*</label></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<select class="form-control" name="jk" placeholder="Masukan Nama Guru">
								<option value="L" <?php if  ($sis->jenis_kelamin == "L") : echo"selected"; endif?> >Laki-Laki</option>
								<option value="P" <?php if  ($sis->jenis_kelamin == "P") : echo"selected"; endif?> >Perempuan</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Guru BK</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="kt-checkbox-list">
								<label class="kt-checkbox" >
									<input type="checkbox" name="isbk" value="Y" 
									<?php if  ($sis->is_bk == "Y") : echo"checked"; endif?>
									> Guru BP/BK
									<span></span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Tempat Lahir</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="tempatlahir" value="{{ $sis->tempat_lahir }}" placeholder="Masukan Tempat Lahir" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Tangal Lahir </label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group date">	
								<div class="input-group-append">
									<input type="text" class="form-control" value="{{ $sis->tanggal_lahir }}" name="tgllahir" id="kt_dategurutgllahir" readonly placeholder="Masukan Tanggal Lahir" >
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Agama</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" value="{{ $sis->agama }}" name="agama" placeholder="Masukan Agama Guru" >
						</div>
					</div>
				</div>

				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Kontak Data
						</h3>
					</div>
				</div>								
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">NIK</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="number" class="form-control" name="nik" value="{{ $sis->nik }}" placeholder="Masukan No Induk KTP" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">NO Telp</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group" >
								<input type="number" class="form-control" value="{{ $sis->notelp }}" name="notelp" placeholder="Masukan No Telp" >
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-phone"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Email</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group" >
								<input type="text" class="form-control" name="emailguru" value="{{ $sis->email_guru }}" placeholder="Masukan Email Guru" >
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-envelope"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Domisili</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="domisili" value="{{ $sis->domisili }}" placeholder="Masukan domisili" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Provinsi</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="provinsi" value="{{ $sis->provinsi }}" placeholder="Masukan nama provinsi" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Kabupaten</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="kabupaten" value="{{ $sis->kabupaten }}" placeholder="Masukan nama kabupaten" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Kecamatan</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="kecamatan"  value="{{ $sis->kecamatan }}" placeholder="Masukan nama kecamatan" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Alamat</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="alamat" value="{{ $sis->alamat }}" placeholder="Masukan Alamat" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Desa/Kelurahan</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="deskel" value="{{ $sis->desakelurahan }}" placeholder="Masukan desa / kelurahan" >
						</div>
					</div>
					<!--end::Form-->
				</div>

				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Data Keluarga
						</h3>
					</div>
				</div>								
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Status Menikah</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="statusnikah" value="{{ $sis->status_nikah }}" placeholder="Masukan status menikah" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Nama Ibu</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="namaibu" value="{{ $sis->nama_ibu }}" placeholder="Masukan Nama Ibu" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Nama Istri/Suami</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="namaissu" value="{{ $sis->nama_istrisuami }}" placeholder="Masukan Nama istri / suami" >
						</div>
					</div>
				</div>

				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Data Sekolah
						</h3>
					</div>
				</div>		

				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Status Kepegawaian</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="statuskepeg" value="{{ $sis->status_kepegawaian }}" placeholder="Masukan status kepegawain" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Jenis PTK</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="jenisptk" value="{{ $sis->jenis_ptk }}" placeholder="Masukan jenis PTK" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Lembaga Pengangkatan</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="lembagapeng" value="{{ $sis->lembaga_pengangkatan }}" placeholder="Masukan lembaga pengangkatan" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">NO SK Pengangkatan</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="nosk" value="{{ $sis->no_sk }}" placeholder="Masukan No SK Pengangkatan" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Tanggal Surat</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group date">
								<div class="input-group-append">
									<input type="text" class="form-control" name="tglsurat" value="{{ $sis->tgl_surat }}" id="kt_dategurutglsurat" placeholder="Masukan Tanggal Surat" >
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">NUPTK</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="nuptk" value="{{ $sis->nuptk }}" placeholder="Masukan NUPTK Guru" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">TMT Tugas</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="tmt" value="{{ $sis->tmt_tugas }}" placeholder="Masukan TMT Tugas" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Tugas Tambahan</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="tugastambahan" value="{{ $sis->tugas_tambahan }}" placeholder="Masukan Tugas Tambahan" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Mengajar</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="mengajar" value="{{ $sis->mengajar }}" placeholder="Masukan Guru Mengajar" >
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<button type="submit" class="btn btn-brand">Simpan</button>
								<a href="back">
									<button type="button" class="btn btn-secondary">Cancel</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	</form>
	@endforeach
	<!--end::Form-->
	<!-- end:: Content -->
</div>
@endsection