
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							@if(count($errors) > 0)
							    <div class="alert alert-solid-danger alert-bold" role="alert" style="margin-top: 20px">
									<div class="alert-text">Gagal Upload !</div>
								    	<ul>
							      		@foreach($errors->all() as $error)
								      		<li>{{ $error }}</li>
								      		@endforeach
								     	</ul>
								</div>
									@endif
									@if($message = Session::get('success'))
								<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
										<div class="alert-text">{{ $message }}
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
											</button>
										</div>
								</div>
								
							@endif
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Data Kelas
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-download"></i> Export
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="kt-nav">
															<li class="kt-nav__section kt-nav__section--first">
																<span class="kt-nav__section-text">Choose an option</span>
															</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" id="export_print">
																<i class="kt-nav__link-icon la la-print"></i>
																<span class="kt-nav__link-text">Print</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" id="export_copy">
																<i class="kt-nav__link-icon la la-copy"></i>
																<span class="kt-nav__link-text">Copy</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" id="export_excel">
																<i class="kt-nav__link-icon la la-file-excel-o"></i>
																<span class="kt-nav__link-text">Excel</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" id="export_csv">
																<i class="kt-nav__link-icon la la-file-text-o"></i>
																<span class="kt-nav__link-text">CSV</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" id="export_pdf">
																<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																<span class="kt-nav__link-text">PDF</span>
															</a>
														</li>
														</ul>
													</div>
												</div>
												&nbsp;
												<a href="#addEmployeeModal" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
													<i class="la la-plus"></i>
													Tambah Data
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="table_kelas">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Kelas</th>
												<th>Actions</th>
											</tr>
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>

					<!-- Edit Modal HTML -->
<div id="editEmployeeModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form>
				<div class="modal-header">						
					<h4 class="modal-title">Edit Employee</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Address</label>
						<textarea class="form-control" required></textarea>
					</div>
					<div class="form-group">
						<label>Phone</label>
						<input type="text" class="form-control" required>
					</div>					
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-info" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Delete Modal HTML -->
<div id="deleteEmployeeModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form>
				<div class="modal-header">						
					<h4 class="modal-title">Hapus Data Kelas</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</div>
				<div class="modal-body">					
					<p>Apakah yakin akan menghapus kelas ini?</p>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-danger" value="Delete">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Tambah Modal HTML -->
<div id="addEmployeeModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  method="post" action="public/kelas/tambahkelas">
				{{ csrf_field() }}
				<div class="modal-header">						
					<h4 class="modal-title">Tambah Data Kelas</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label>Name Kelas</label>
						<input type="text" class="form-control" name="namakelas" required>
					</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-success" value="Tambah">
				</div>
			</form>
		</div>
	</div>
</div>
@endsection