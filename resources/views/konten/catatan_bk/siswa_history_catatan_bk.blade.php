<?php
$isuser = Session::get('level');
if ($isuser == "GURU"){
	$url =url ('/404_Not_Found');
	header('Location: '.$url); /* Redirect browser */
	die(); }
	?>
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							History Catatan BK
						</h3>
					</div>
				</div>

				@foreach($datasiswa as $kl)
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">NIS Siswa</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nis}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Nama Siswa</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="email" value="{{ $kl->nama_siswa}}" disabled>
						</div>
					</div>
					<div class="form-group row" >
						<label class="col-form-label col-lg-2" style="text-align: left">Tahun Ajaran / Semester</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $kl->tahun_ajaran}} / {{ $kl->semester}}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2" style="text-align: left">Kelas</label>
						<div class="col-lg-4 ">
							<input type="text" class="form-control form-control-sm" name="password" value="{{ $kl->nama_kelas}}" disabled>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="kt-portlet">
				<!-- <div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_print">
													<i class="kt-nav__link-icon la la-print"></i>
													<span class="kt-nav__link-text">Print</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_copy">
													<i class="kt-nav__link-icon la la-copy"></i>
													<span class="kt-nav__link-text">Copy</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_excel">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_csv">
													<i class="kt-nav__link-icon la la-file-text-o"></i>
													<span class="kt-nav__link-text">CSV</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link" id="export_pdf">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="#tambah_soal_cbt" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									Tambah Data
								</a>
							</div>
						</div>
					</div>
				</div> -->
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_history_absensi" >
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Catatan BK</th>
								<!-- <th>Action</th> -->
							</tr>
						</thead>
						<tbody>
							<?php $n=1 ?>
							@foreach($catatanbk as $catbk)
							<tr>
								<td>{{ $n++ }}</td>
								<td>{{ $catbk->tanggal}}</td>
								<td>{{ $catbk->catatan}}</td>
								<!-- <td>Action</td> -->
							</tr>
							@endforeach
						</tbody>
					</table>

					<!--end: Datatable -->
				</div>

				<!--end::Form-->
			</div>

			<!--end::Portlet-->
		</div>

		<!-- end:: Content -->
	</div>
	@endsection