@extends('layout')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit Detail Tugas Pilihan Ganda
					</h3>
				</div>
			</div>
		</div>
		



	@foreach($datadetailsoalcbt as $so)
		<div class="modal-content">
			<form method="post" action="editdetailsoalcbt/prosespg" enctype="multipart/form-data"  autocomplete="off">
				{{ csrf_field() }}
				
				<div class="modal-body">
					<div class="form-group">
						<label>Soal</label>
						
						<textarea class="form-control" id="exampleTextarea"  name="soal" rows="3">{{ $so->soal_pg}} </textarea>
							<input type="hidden" name="idsoal" readonly value="{{$so->id_soal_cbt}}" />
							<input type="hidden" name="id_detail_soal_cbt"  value="{{ $so->id_detail_soal_cbt}}" />
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Gambar</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="custom-file">
								<?php if ($so->gambar_soal) { ?>
				
								<input type="file" class="custom-file-input" id="customFile" name="file" value="{{$so->gambar_soal}}"><br>
								<label class="custom-file-label" for="customFile" >Choose file</label>
								<div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
														<div class="kt-avatar__holder" style="background-image: url(&quot;{{ url('/soal_cbt/pg/soal/' . $so->gambar_soal)}}&quot;);"></div>
														<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
															<i class="fa fa-times"></i>
														</span>
													</div>
							<?php	}else{  ?>
								<input type="file" class="custom-file-input" id="customFile" name="file">
								<label class="custom-file-label" for="customFile" >Choose file</label>
							<?php } ?>
								
							</div>
						</div>
					</div>	


						<?php
$date =  substr($so->pilihan_a,0,10);
$format = 'd-m-Y' ;
$d = DateTime::createFromFormat($format, $date);

$isdate = $d && $d->format($format) === $date;
// var_dump($isdate);

?>

					<div class="kt-portlet__body">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link"   data-toggle="tab" href="#" data-target="#kt_tabs_1_1">Text</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2">Gambar</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane " id="kt_tabs_1_1" role="tabpanel">
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan A</label>
									<div class="col-lg-9 col-md-9 col-sm-12">

										<input type="text" class="form-control" value="{{$so->pilihan_a}}" name="pil_a" >
									</div>							
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan B</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" value="{{$so->pilihan_b}}" name="pil_b" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan C</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" value="{{$so->pilihan_c}}" name="pil_c" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan D</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" value="{{$so->pilihan_d}}" name="pil_d" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan E</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" value="{{$so->pilihan_e}}" name="pil_e" >
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
								<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan A</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_a">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>							
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan B</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_b">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan C</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_c">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan D</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_d">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan E</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_e">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>


						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Kunci Jawaban</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="col-9">
									<div class="kt-radio-inline">
										<label class="kt-radio">
										<?php if($so->kunci_jawaban == "a" || $so->kunci_jawaban == "A") { ?>
											<input type="radio" checked="checked" name="kunci" value="A"> A
										<?php }else{?> 
											<input type="radio" name="kunci" value="A"> A
										<?php } ?>
											<span></span>
										</label>
										<label class="kt-radio">
										<?php if($so->kunci_jawaban == "b" || $so->kunci_jawaban == "B") { ?>
											<input type="radio" checked="checked" name="kunci" value="B"> B
										<?php }else{?> 
											<input type="radio" name="kunci" value="B"> B
										<?php } ?>
											<span></span>
										</label>
										<label class="kt-radio">
										<?php if($so->kunci_jawaban == "C" || $so->kunci_jawaban == "c") { ?>
											<input type="radio" checked="checked" name="kunci" value="C"> C
										<?php }else{?> 
											<input type="radio" name="kunci" value="C"> C
										<?php } ?>
											<span></span>
										</label>
										<label class="kt-radio">
											<?php if($so->kunci_jawaban == "D" || $so->kunci_jawaban == "d") { ?>
											<input type="radio" checked="checked" name="kunci" value="D"> D
										<?php }else{?> 
											<input type="radio" name="kunci" value="D"> D
										<?php } ?>
											<span></span>
										</label>
										<label class="kt-radio">
											<?php if($so->kunci_jawaban == "E" || $so->kunci_jawaban == "e") { ?>
											<input type="radio" checked="checked" name="kunci" value="E"> E
										<?php }else{?> 
											<input type="radio" name="kunci" value="E"> E
										<?php } ?>
											<span></span>
										</label>
									</div>
									<span class="form-text text-muted">Pilih salah satu untuk jawaban yang benar</span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Bobot Nilai</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" value="{{$so->bobot}}" name="bobot" required>
							</div>
						</div>					
						<input type="hidden" class="form-control" name="idmasterkelas" value=""  required>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Update">
						</div>
					</div>
				</div>
			</form>
		</div>


@endforeach

	@endsection