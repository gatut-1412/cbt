@extends('layout')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Content -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Tambah Detail Tugas 
					</h3>
				</div>
				<div class="kt-portlet__head-label">
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" data-placement="left">
						<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<button type="button" class="btn btn-outline-brand btn-elevate btn-pill"><i class="la la-cog"></i> Option</button>

							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									SIlahkan pilih option:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									@foreach($template as $tl)
									<a href="{{url('data_file/template_upload/'.$tl->file_upload)}}" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Download Template Upload</span>
									</a>
									@endforeach
								</li>
								<li class="kt-nav__item">
									@foreach($soalcbt as $so)	

									<a onclick="return confirm('Apakah data akan dihapus ?');" 
									href="tambahdetailsoalcbt/hapussemua/{{ $so->id_soal_cbt}}" class="kt-nav__link">
									@endforeach								
									<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
									<span class="kt-nav__link-text">Hapus Soal CBT</span>
								</a>
							</li>

							<!-- <li class="kt-nav__item">
								@foreach($soalcbt as $so) -->
								<?php // $a =  $so -> id_soal_cbt  ?>
								<?php //echo"<a href ='../print_pdf/index2.php?id=".$a."' class='kt-nav__link' target='_blank'>"; ?>
								<!-- <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
								<span class="kt-nav__link-text">Download Soal</span>
								@endforeach
							</a>
						</li> -->

						<li class="kt-nav__item">
							@foreach($soalcbt as $so1)
							<a target="_blank" href="laporan_soal/{{ $so1->id_soal_cbt}}" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
								<span class="kt-nav__link-text">Download Soal</span>
							</a>
							@endforeach	
						</li>


						<li class="kt-nav__separator"></li>
						<li class="kt-nav__foot">
							<a class="btn btn-label-brand btn-bold btn-sm" href="#">Update Soal</a>
						</li>
					</ul>

					<!--end::Nav-->
				</div>
			</div>
		</div>
	</div>

	<!--begin::Form-->
	@foreach($soalcbt as $so)
	<input type="hidden" class="form-control form-control-sm" id="idsoalcbt" value="{{ $so->id_soal_cbt}}" disabled >
	<input type="hidden" class="form-control form-control-sm" id="idsoalcbtesay" value="{{ $so->id_soal_cbt}}" disabled >
	<div class="kt-portlet__body">
		<div class="form-group row" >
			<label class="col-form-label col-lg-2" style="text-align: left">Judul Soal</label>
			<div class="col-lg-4 ">
				<input type="text" class="form-control form-control-sm" name="tahunajaran" value="{{ $so->judul_soal}}" disabled>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-form-label col-lg-2" style="text-align: left">Jenis Soal</label>
			<div class="col-lg-4 ">
				<input type="text" class="form-control form-control-sm" name="password" value="{{ $so->jenis_soal}}" disabled>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-form-label col-lg-2" style="text-align: left">Waktu</label>
			<div class="col-lg-4 ">
				<input type="text" class="form-control form-control-sm"  name="nama" value="{{ $so->waktu}}" disabled>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-form-label col-lg-2" style="text-align: left">Token</label>
			<div class="col-lg-4 ">
				<input type="text" class="form-control form-control-sm" name="email" value="{{ $so->token}}" disabled>
			</div>
		</div>
	</div>
	@endforeach
</div>
<div class="kt-portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<a href="#addsoalpgcbt" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
				<i class="la la-plus"></i>
				Tambah Soal PG
			</a>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<form method="post" enctype="multipart/form-data" action="{{ url('/soalcbt/tambahdetailsoalcbt/import') }}">
						{{ csrf_field() }}
						<table style="float:right">
							<tr>
								<td align="right"><label>Select File for Upload</label></td>
								<td>
									<input type="file" name="select_file" />

								</td>
								<td width="s100px" align="left">
									<input type="submit" name="upload" class="btn btn-primary" value="Upload Soal PG">
									@foreach($soalcbt as $so)
									<input type="hidden" name="idsoal" readonly value="{{ $so->id_soal_cbt}}" />
									@endforeach
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="table_detailsoalcbt" >
			<thead>
				<tr>
					<th>No</th>
					<th>Soal</th>
					<th>Pilihan A</th>
					<th>Pilihan B</th>
					<th>Pilihan C</th>
					<th>Pilihan D</th>	
					<th>Pilihan E</th>
					<th>Jawaban</th>
					<th>Bobot</th>
					<th>Action</th>										
				</tr>
			</thead>
		</table>
		<!--end: Datatable -->
	</div>
</div>

	<!-- 	<div class="kt-portlet">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<a href="#addsoalesaycbt" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Tambah Soal Essay
					</a>
				</div>
			</div>
			<div class="kt-portlet__body">
				
				<table class="table table-striped- table-bordered table-hover table-checkable" id="table_detailsoalcbtesay" >
					<thead>
						<tr>
							<th>No</th>
							<th>Soal Esay</th>
							<th>Bobot</th>
							<th>Action</th>										
						</tr>
					</thead>
				</table>
			
			</div>
			
		</div>
	-->
	<!--end::Portlet-->
</div>

<!-- end:: Content -->
</div>
<!-- Tambah Modal HTML -->





<div class="modal fade" id="addsoalpgcbt" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form method="post" action="tambahdetailsoalcbt/prosespg" enctype="multipart/form-data"  autocomplete="off">
				{{ csrf_field() }}
				<div class="modal-header">						
					<h4 class="modal-title">Tambah Soal PG CBT</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Soal</label>
						<textarea class="form-control" id="exampleTextarea" name="soal" rows="3"></textarea>
						@foreach($soalcbt as $so)
						<input type="hidden" name="idsoal" readonly value="{{ $so->id_soal_cbt}}" />
						@endforeach
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">Gambar</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="file">
								<label class="custom-file-label" for="customFile" >Choose file</label>
							</div>
						</div>
					</div>	
					<div class="kt-portlet__body">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#" data-target="#kt_tabs_1_1">Text</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2">Gambar</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane " id="kt_tabs_1_1" role="tabpanel">
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan A</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<input type="text" class="form-control" name="pil_a"  >
									</div>							
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan B</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" name="pil_b" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan C</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" name="pil_c" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan D</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" name="pil_d" >
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Pilihan E</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" name="pil_e" >
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
								<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan A</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_a">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>							
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan B</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_b">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan C</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_c">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan D</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_d">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pilihan E</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" name="pil_e">
												<label class="custom-file-label" for="customFile" >Choose file</label>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>


						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Kunci Jawaban</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="col-9">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="kunci" checked="checked" value="A"> A
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="kunci" value="B"> B
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="kunci" value="C"> C
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="kunci" value="D"> D
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="kunci" value="E"> E
											<span></span>
										</label>
									</div>
									<span class="form-text text-muted">Pilih salah satu untuk jawaban yang benar</span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Bobot Nilai</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="bobot" required>
							</div>
						</div>					
						<input type="hidden" class="form-control" name="idmasterkelas" value=""  required>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Tambah">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>






<div class="modal fade" id="addsoalesaycbt" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form  method="post" action="tambahdetailsoalcbt/prosesesay" enctype="multipart/form-data"  autocomplete="off">
				{{ csrf_field() }}
				<div class="modal-header">						
					<h4 class="modal-title">Tambah Soal CBT Essay</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Soal Essay</label>
							<textarea class="form-control" id="exampleTextarea1" name="soalesay" rows="3"></textarea>
							@foreach($soalcbt as $so)
							<input type="hidden" name="idsoal" readonly value="{{ $so->id_soal_cbt}}" />
							@endforeach
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Gambar</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="customFile1" name="file">
									<label class="custom-file-label" for="customFile" >Choose file</label>
								</div>
							</div>
						</div>			
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12">Bobot Nilai</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="bobot" required>
							</div>
						</div>							
						<input type="hidden" class="form-control" name="idmasterkelas" value=""  required>

						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
							<input type="submit" class="btn btn-success" value="Tambah">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--end::Modal-->


</div>

@endsection