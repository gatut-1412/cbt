@extends('layout')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Mapping Soal
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <a href="#tambahcbtperkelas" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
                        <i class="la la-plus"></i>
                        Bagikan Soal
                    </a>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="table_mappingcbt" >
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kelas</th>
                            <th>Mata Pelajaran</th>
                            <th>Soal</th>
                            <th>Type Soal</th>
                            <th>Tanggal & Jam</th>
                            <th>Status</th>
                            <th>Action Status</th>  
                            <th>Action</th>                                     
                        </tr>
                    </thead>
                </table>
                <!--end: Datatable -->
            </div>
        </div>

        <!--end::Portlet-->
    </div>

    <!-- end:: Content -->
</div>
<!-- Tambah Modal HTML -->


<!--begin::Modal-->
<div class="modal fade" id="tambahcbtperkelas" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Bagikan Soal ke Kelas yang dituju</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="kt-form kt-form--fit kt-form--label-right" method="post" action="mapingan_cbt_perkelas/tambah">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group row kt-margin-t-20">
                        <label class="col-form-label col-lg-3 col-sm-12">Kelas</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control kt-select2" id="kelascbt" name="kelascbt1">
                                <option value=""></option>
                                @foreach ($mapelkelas as $g)
                                <option value="{{ $g -> id_guru_mapel }} "> 
                                    {{ $g -> nama_kelas }}  || {{ $g -> nama_pelajaran }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>                                          
                    <div class="form-group row kt-margin-t-20">
                        <label class="col-form-label col-lg-3 col-sm-12">Pilih Soal</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control kt-select2" id="soalcbt" name="soalcbt1">
                                <option value=""></option>
                                @foreach ($datasoalcbt as $m)
                                <option value="{{ $m -> id_soal_cbt}} "> 
                                    {{ $m -> judul_soal }}  || {{ $m -> jenis_soal }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>  
                    <div class="form-group row kt-margin-t-20">
                        <label class="col-form-label col-lg-3 col-sm-12">Tanggal</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="datecbt"  data-z-index="1100" placeholder="Select date & time" name='tglcbt'/>
                        </div>
                    </div>  
                    <div class="form-group row kt-margin-t-20">
                        <label class="col-form-label col-lg-3 col-sm-12">Catatan</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" id="exampleTextarea" name="catatan" rows="3"></textarea>
                        </div>
                    </div>
            <!--        <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Waktu</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" name="waktu" placeholder="Masukan waktu dalam satuan menit">
                                <div class="input-group-append"><span class="input-group-text">Menit</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Token</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <div class="input-group">   
                                <input type="text" class="form-control" name="token" value="{{  $filename }}" readonly>
                            </div>
                        </div>
                    </div>                                      
                </div> -->
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success" value="Tambah">
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<div id="hapussoalcbt" class="modal fade text-danger" role="dialog">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <form action="" id="deleteForm" method="post">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">DELETE CONFIRMATION</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <p class="text-center">Are You Sure Want To Delete ?</p>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection