
<?php
$isuser = Session::get('level');
// var_dump($isuser);
// exit();
if ($isuser == "SISWA"){
      $url =url ('/404_Not_Found');
    header('Location: '.$url); /* Redirect browser */
    die(); }
    ?>
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Subheader -->
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Format Upload
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable">
										<thead>
											<?php
											$no =1;
											?>
											<tr>
												<th>No</th>
												<th>Data Upload</th>
												<th>File Upload</th>												
												<th>Actions</th>
											</tr>
											@foreach($templateupload as $tmpup )
											<tr>
												<td>{{$no++}}</td>
												<td>{{ $tmpup-> menu_upload }}</td>
												<td>{{ $tmpup-> file_upload }}</td>
												<td>Ubah</td>
											</tr>
											@endforeach
										</thead>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
						</div>
						<!-- end:: Content -->
					</div>
					<!--begin::Modal-->
					<div class="modal fade" id="addampel" role="dialog" aria-labelledby="" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" >Tambah Guru Mengajar</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" class="la la-remove"></span>
									</button>
								</div>
								<form class="kt-form kt-form--fit kt-form--label-right" method="post" action="public/matpelajaran/tambah">
									{{ csrf_field() }}
									<div class="modal-body">
										<div class="form-group row kt-margin-t-20">
											<label class="col-form-label col-lg-3 col-sm-12">Nama Mata Pelajaran</label>
											<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="namapel" required>
											</div>
										</div>											
										<div class="form-group row kt-margin-t-20">
											<label class="col-form-label col-lg-3 col-sm-12">Kelompok Pelajaran</label>
											<div class="col-lg-9 col-md-9 col-sm-12">
												<select class="form-control kt-select2" id="kt_select2_1_modal" name="idkel">
													<option value=""></option>
												</select>
											</div>
										</div>																	
									</div>
									<div class="modal-footer">
										<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
										<input type="submit" class="btn btn-success" value="Tambah">
									</div>
								</form>
							</div>
						</div>
					</div>
					<!--end::Modal-->
@endsection