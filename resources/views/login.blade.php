<html lang="en">

    <!-- begin::Head -->
    <head>
    @include('partials.head')
    </head>

	
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<style>
#formContent {
  -webkit-border-radius: 10px 10px 10px 10px;
  border-radius: 10px 10px 10px 10px;
  background: #fff;
  padding: 30px;
  width: 90%;
  max-width: 450px;
  position: relative;
  padding: 0px;
  -webkit-box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  text-align: center;
}
 </style>
		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login" style="background-image: url({{asset('assets/media//bg/logo2.png')}})">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						

 <div id="formContent">

													
							<div class="kt-login__signin" style="padding:40px;">

								<div class="kt-login__head" style="padding:20px;">
									<h3 class="kt-login__title">SMA MUHAMMADIYAH 1 PRAMBANAN</h3>

								</div>
											@if(\Session::has('alert'))
											<div class="alert alert-danger">
												<div>{{Session::get('alert')}}</div>
											</div>
											@endif
											@if(\Session::has('alert-success'))
											<div class="alert alert-success">
												<div>{{Session::get('alert-success')}}</div>
											</div>
											@endif
								
								<form class="kt-form" action="{{ url('/loginPost') }}" method="post">
								{!! csrf_field() !!}
									<div class="input-group" style="padding:10px;">
										<input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
									</div>
									<div class="input-group" id="show_hide_password" style="padding:10px;">
										<input class="form-control" type="password" placeholder="Password" name="password" >
									
									</div>
									
									<div class="kt-login__actions">
										<button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>
									</div>
								</form>
								
							</div>
							

						
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
									
</html>