<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         // the call method
        $schedule->call(function () {
           $publish = 'PUBLISH';
           $expired = 'EXPIRED';
           $draft = 'DRAFT';
           date_default_timezone_set("Asia/Bangkok");
            DB::table('tb_tugas_online')
              ->where('start_date','<=',date('Y-m-d H:i:s'))
              ->where('status_tugas',$draft)
              ->update(['status_tugas' => $publish]);
        })->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
