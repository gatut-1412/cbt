<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class matpelajaranController extends Controller
{
    //
    public function tampilMatPelajaran()
    {
        $kelmapel =  DB::table('tb_kelompok_pelajaran')->where('status',"active")->get();
        $matpelajaran = null;
        return view('konten.matpelajaran.matpelajaran', compact(['matpelajaran'],['kelmapel']));
    }
    public function dataMatPelajaran()
    {
        $mapel = DB::table('tb_pelajaran')
        ->join('tb_kelompok_pelajaran', 'tb_pelajaran.id_kel_pelajaran', '=', 'tb_kelompok_pelajaran.id_kel_pelajaran')
        ->where('tb_pelajaran.status',"active")
        ->select('tb_pelajaran.id_pelajaran','tb_pelajaran.nama_pelajaran','tb_kelompok_pelajaran.nama_kel_pelajaran','tb_pelajaran.status')
        ->get();

        $result = array(
            'data' => $mapel 
        );
        $mapel = json_encode($result);
        return $mapel;
    }

    public function hapusPelajaran(Request $request)
    {

       DB::table('tb_pelajaran')
       ->where('id_pelajaran',$request->id)->delete();
       return back()->with('success', 'Berhasil Update Data.');
   }

   public function tambahMatPelajaran(Request $request)
   {
        DB::table('tb_pelajaran')->insert([
            'nama_pelajaran' => $request->namapel,
            'id_kel_pelajaran' => $request->idkel,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function fetchdata($id)
    {
        $matpelajaran = DB::table('tb_pelajaran')
        ->join('tb_kelompok_pelajaran', 'tb_pelajaran.id_kel_pelajaran', '=', 'tb_kelompok_pelajaran.id_kel_pelajaran')
        ->where('tb_pelajaran.id_pelajaran',$id)
        ->get();

        return response()->json([
          'data' => $matpelajaran
      ]);
    }

    public function updateMatpel(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_pelajaran')
        ->where('id_pelajaran', $request->idmatpel)
        ->update([
            'nama_pelajaran' => $request->namapel,
            'id_kel_pelajaran' => $request->idkel,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteMatpel(Request $request)
    {
        try{
            DB::table('tb_pelajaran')
            ->where('id_pelajaran',$request->idmatpel)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu mappingan data.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
   }
}
