<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    //
    public function tampilCategory()
    { 
        $category  = null;
        return view('konten.category.category', compact(['category']));
    }
    public function dataCategory()
    {
        $category = DB::table('mst_categories AS tb1')
                    ->join('mst_categories AS tb2','tb1.category_id','=','tb2.sub_category_id')
                    ->select('tb1.category_id as id_cat','tb2.category_id as id_sub', 'tb1.category_name as cat_name', 'tb2.category_name as sub_cat_name')
                    ->where('tb1.status',"active")
                    ->get();
        $result = array(
        'data' => $category 
        );
        $category = json_encode($result);
        return $category;
    }
    public function tambahCategory()
    {
        return view('konten.category.tambah_category', compact(['tambah']));
    }

    public function prosesTambahCategory(Request $request)
    {
    	// var_dump($request->nama);
    	// exit();
            DB::table('mst_users')->insert([
            'username' => $request->username,
            'password' => md5($request->password),
            'nama' => $request->nama,
            'email' => $request->email,
            'level' => $request->level,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return redirect('/category');
    }

    public function editCategory()
    {

        return redirect('/category');
    }
}
