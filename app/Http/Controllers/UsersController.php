<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class UsersController extends Controller
{
    //
    public function tampilUsers1()
    {
        return view('konten.users.users1');
    }
    public function tampilUsers()
    {
        return view('konten.users.users');
    }
    public function dataUsers()
    {
        $users = DB::table('mst_users')->where('status',"active")->get();
        $result = array(
            'data' => $users 
        );
        $users = json_encode($result);
        return $users;
    }
    public function dataUsersServer()
    {
        $users = DB::table('mst_users')->where('status',"active")->get();
        $users = json_encode($users);
        return $users;
    }
    public function tambahUsers()
    {
        return view('konten.users.tambah_user');
    }

    public function prosesTambahUsers(Request $request)
    {
    	// var_dump($request->nama);
    	// exit();
        DB::table('mst_users')->insert([
            'username' => $request->username,
            'password' => md5($request->password),
            'nama' => $request->nama,
            'email' => $request->email,
            'level' => $request->level,
            'created_by' => "SYSTEM",
            'status' => "active"
        ]);
        return redirect('/users')->with('success', 'Data berhasil ditambahkan.');
    }

    public function prosesEditUsers(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $pwd = $request->password;

        if ($pwd == null)
        {
            DB::table('mst_users')
            ->where('id_users', $request->idusers)
            ->update([
                'username' => $request->username,
                'nama' => $request->nama,
                'email' => $request->email,
                'level' => $request->level,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }
        else
        {
            DB::table('mst_users')
            ->where('id_users', $request->idusers)
            ->update([
                'username' => $request->username,
                'nama' => $request->nama,
                'email' => $request->email,
                'password' => md5($request->password),
                'level' => $request->level,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function fetchdata($id)
    {
        $users = DB::table('mst_users')
        ->where('id_users',$id)
        ->get();

        return response()->json([
          'data' => $users
      ]);
    }
}
