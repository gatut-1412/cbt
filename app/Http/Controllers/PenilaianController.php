<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class penilaianController extends Controller
{
    //
    public function tampilPenilaian()
    {
        $penilaian = null;
        return view('konten.penilaian.penilaian', compact(['penilaian']));
    }
    public function dataKelasSiswa()
    {
        $kelassiswa = DB::table('tb_kelas_siswa')
        ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
        ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
        ->get();
        $result = array(
        'data' => $kelassiswa 
        );
        $kelassiswa = json_encode($result);
        return $kelassiswa;
    }

    public function dataSiswaPenilaian()
    {
          $id_mk =  request()->segment(count(request()->segments()));    
        $id_mk = (int)$id_mk;
        $siswa = DB::table('tb_siswa')
        ->leftjoin('tb_kelas_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->leftjoin('tb_master_kelas', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
        ->select('tb_siswa.id_siswa', 'nama_siswa', 'nis', 'jenis_kelamin')
         ->where('tb_master_kelas.id_master_kelas',$id_mk)
        ->where('tb_siswa.status',"active")->get();
        $result = array(
        'data' => $siswa 
        );
        $siswa = json_encode($result);
        return $siswa;
    }

    public function tambahMappingKelasSiswa()
    {
        return view('konten.mappingkelas.tambah_mappingkelas');
    }
    public function proses(Request $request)
    {
            DB::table('tb_kelas')->insert([
            'nama_kelas' => $request->namakelas,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
        //return redirect('/kelas');
    }

    public function editPenilaian($id)
    {
        $penilaian = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
        return view('konten.penilaian.update_penilaian',compact('penilaian'));
    }
}
