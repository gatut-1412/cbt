<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class kelasController extends Controller
{
    //
    public function tampilKelas()
    {
        $kelas = null;
        return view('konten.kelas.kelas', compact(['kelas']));
    }
    public function datakelas()
    {
        $kelas = DB::table('tb_kelas')->where('status',"active")->get();
        $result = array(
        'data' => $kelas 
        );
        $kelas = json_encode($result);
        return $kelas;
    }
    public function tambahKelas(Request $request)
    {
            DB::table('tb_kelas')->insert([
            'nama_kelas' => $request->namakelas,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
        //return redirect('/kelas');
    }

    public function editkelas()
    {

        return redirect('/kelas');
    }
}
