<?php

namespace App\Http\Controllers;

use Session;
use App\TahunAjaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $TA = TahunAjaran::all();
        $tahunajaran = null;
        $tahunajaran = DB::table('tb_tahun_ajaran')->where('status',"active")->get();
        $result = array(
        'data' => $tahunajaran 
        );
        $tahunajaran = json_encode($result);
        return view('konten.tahun_ajaran.tahun_ajaran', compact(['tahunajaran']))->with([
            'TA' => $TA,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::table('tb_tahun_ajaran')->insert([
            'tahun_ajaran' => $request->tahunajaran,
            'created_by' => "admin",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tahunajaran = DB::table('tb_tahun_ajaran')
        ->where('id_tahun_ajaran',$id)
        ->get();

        return response()->json([
          'data' => $tahunajaran
      ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_tahun_ajaran')
        ->where('id_tahun_ajaran', $request->$id)
        ->update([
            'tahun_ajaran' => $request->tahunajaran,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            DB::table('tb_tahun_ajaran')
            ->where('id_tahun_ajaran',$request->$id)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu pada mappingan.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
    }
}
