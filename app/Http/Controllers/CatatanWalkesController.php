<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Datatables;

class catatanWalkesController extends Controller
{
    //
    public function tampilCatatanWalkes()
    {
        $catatan_walkes = null;
        return view('konten.catatan_walkes.catatan_walkes', compact(['catatan_walkes']));
    }
    public function dataCatatanWalkesPerkelas()
    {
        $user = Session::get('username');
        $level = Session::get('level');
        if($level== "ADMIN")
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
                'data' => $kelassiswa 
            );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
        if($level == "GURU")
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->where('tb_guru.nip',$user)
            ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
                'data' => $kelassiswa 
            );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
    }

    public function dataCatatanWalkes()
    {
        $id_mk =  request()->segment(count(request()->segments()));    
        $id_mk = (int)$id_mk;

        $siswa = DB::table('tb_catatan_walkes')
        ->rightjoin('tb_kelas_siswa', 'tb_catatan_walkes.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->rightjoin('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_master_kelas.id_master_kelas',$id_mk)
        ->select('id_catatan_walkes as id','tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','jenis_kelamin',DB::raw('IFNULL(catatan, "") as catatan') )
        ->groupBy('nis')
        ->get();
        $result = array(
            'data' => $siswa 
        );
        $siswa = json_encode($result);
        return $siswa;
    }

    //smoy berubah jadi satuan
    // public function updateCatatanWalkes(Request $request)
    // {


    //     for ($i = 0; $i < count($request->idks); $i++) {
    //     $answers[] = [
    //         'id_kelas_siswa' => $request->idks[$i],
    //         'catatan' => $request->catatan[$i]
    //         ];

    //     }

    //     DB::table('tb_catatan_walkes')->insert($answers);

    //     return back()->with('success', 'Berhasil Tambah Data.');
    //     //return redirect('/kelas');
    // }

    public function detailCatatanWalkes($id)
    {
        $user = Session::get('username');
        $catatan_walkes = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();

        $siswa = DB::table('tb_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_guru.id_guru', '=', 'tb_master_kelas.id_guru')
        ->leftjoin('tb_catatan_walkes', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_catatan_walkes.id_kelas_siswa')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_guru.nip',$user)
        ->where('tb_master_kelas.id_master_kelas',$id)
        // ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas')
        ->orderby('nis')
        ->get();

        $siswa2 = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_guru.id_guru', '=', 'tb_master_kelas.id_guru')
        ->rightjoin('tb_catatan_walkes', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_catatan_walkes.id_kelas_siswa')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_guru.nip',$user)
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('tb_catatan_walkes.id_kelas_siswa')
        ->groupBy('tb_catatan_walkes.id_kelas_siswa')
        ->get();

        return view('konten.catatan_walkes.update_catatan_walkes',compact('catatan_walkes','siswa','siswa2'));
    }
    public function prosesTambah(Request $request)
    {
        $user = Session::get('username');
        DB::table('tb_catatan_walkes')->insert
        ([
            'id_kelas_siswa' => $request->siswa,
            'catatan' => $request->cat_walkes,
            'created_by' => $user
        ]);

        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function updateCatatanWalkes(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");

        DB::table('tb_catatan_walkes')
        ->where('id_catatan_walkes', $request->idcatwalkes)
        ->update([
            'catatan' => $request->cat_walkes,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function fetchdata($id)
    {
        $siswa = DB::table('tb_catatan_walkes')
        ->rightjoin('tb_kelas_siswa', 'tb_catatan_walkes.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->rightjoin('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_catatan_walkes.id_catatan_walkes',$id)
        ->select('id_catatan_walkes as id','tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','jenis_kelamin',DB::raw('IFNULL(catatan, "") as catatan') )
        ->groupBy('nis')
        ->get();

         return response()->json([
          'data' => $siswa
        ]);
    }

    public function deleteCatatanWalkes(Request $request)
    {
        DB::table('tb_catatan_walkes')
       ->where('id_catatan_walkes',$request->idcatwalkes)->delete();

        return back()->with('success', 'Data berhasil Dihapus.');
    }


    //START
    //-------------------------------------SISWA CATATAN WALKES-------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function siswaCatatanWalkes()
    {
        return view('konten.catatan_walkes.siswa_catatan_walkes');
    }
    public function dataSiswaCatatanWalkes()
    {
        $user = Session::get('username');   
        $kelassiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_siswa.nis',$user)
        ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas','tb_kelas_siswa.id_kelas_siswa')
        ->groupBy('tahun_ajaran','nama_kelas')
        ->get();
        $result = array(
          'data' => $kelassiswa 
      );
        $kelassiswa = json_encode($result);
        return $kelassiswa;
    }
    public function siswaCatatanWalkesdetail($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester','nama_guru')
        ->get();

        $catatanwalkes = DB::table('tb_catatan_walkes')
        ->where('id_kelas_siswa',$id)
        ->select('catatan')
        ->get();
        return view('konten.catatan_walkes.siswa_history_catatan_walkes',compact(['datasiswa'],('catatanwalkes')));
    }
    //END
    //----------------------------------- SISWA CATATAN WALKES -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
}
