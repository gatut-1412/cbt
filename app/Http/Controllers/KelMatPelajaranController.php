<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class kelmatpelajaranController extends Controller
{
    //
    public function tampilKelMatPelajaran()
    {
        $kelmatpelajaran = null;
        return view('konten.kelmatpelajaran.kelmatpelajaran', compact(['kelmatpelajaran']));
    }
    public function dataKelMatPelajaran()
    {
        $mapel = DB::table('tb_kelompok_pelajaran')
        ->where('status',"active")
        ->get();

        $result = array(
            'data' => $mapel 
        );
        $mapel = json_encode($result);
        return $mapel;
    }
    public function tambaKelhMatPelajaran(Request $request)
    {
        DB::table('tb_kelompok_pelajaran')->insert([
            'nama_kel_pelajaran' => $request->namakelmapel,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Data berhasil Disimpan.');
        // return redirect('/kelmatpelajaran');
    }

    public function fetchdata($id)
    {
        $kelmatpelajaran = DB::table('tb_kelompok_pelajaran')
        ->where('id_kel_pelajaran',$id)
        ->get();

        return response()->json([
          'data' => $kelmatpelajaran
      ]);
    }

    public function updateKelMatpel(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_kelompok_pelajaran')
        ->where('id_kel_pelajaran', $request->idkelmatpel)
        ->update([
            'nama_kel_pelajaran' => $request->namakelpel,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteKelMatpel(Request $request)
    {
        try{
            DB::table('tb_kelompok_pelajaran')
            ->where('id_kel_pelajaran',$request->idkelmatpel)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu pada Mata Pelajaran.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
   }
}
