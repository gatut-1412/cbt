<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
    //
    public function tampilServices()
    {
        $services = null;
        return view('konten.services.services', compact(['services']));
    }

    public function dataServices()
    {
        $services = DB::table('mst_services')
                    ->join('mst_categories', 'mst_services.service_cat_id', '=', 'mst_categories.category_id')
                    ->join('mst_members', 'mst_services.member_id', '=', 'mst_members.member_id')
                    ->join('mst_territories', 'mst_services.service_loc_id', '=', 'mst_territories.kode_wilayah')
                    ->where('mst_services.status','!=',"inactive")
                    ->select('mst_services.*', 'mst_members.username', 'mst_categories.category_name', 'mst_territories.nama')
                    ->get();
        $result = array(
        'data' => $services 
        );
        $services = json_encode($result);
        return $services;
    }

    public function approveServices(Request $request)
    {
    DB::table('mst_services')->where('service_id',$request->id)->update([
        'status' => "ACTIVE"

        ]);
        return redirect('/services');
    }

    public function rejectServices(Request $request)
    {
    DB::table('mst_services')->where('service_id',$request->id)->update([
        'status' => "REJECT"

        ]);
        return redirect('/services');
    }

    public function hapusServices(Request $request)
    {
    DB::table('mst_services')->where('service_id',$request->id)->update([
        'status' => "INACTIVE"

        ]);
        return redirect('/services');
    }

}