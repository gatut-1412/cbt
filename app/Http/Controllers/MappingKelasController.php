<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class mappingkelasController extends Controller
{
    //
    public function tampilMappingKelas()
    {
        
        $tahun_ajaran = DB::table('tb_tahun_ajaran')->where('status',"active")->get();
        $guru = DB::table('tb_guru')->where('status',"active")->get();
        return view('konten.mappingkelas.mappingkelas', compact(['tahun_ajaran'],['guru']));
    }
    public function dataMappingKelas()
    {
        $mappingkelas = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->select('tb_master_kelas.*', 'tb_guru.nama_guru', 'tb_tahun_ajaran.tahun_ajaran')
        ->get();
        $result = array(
        'data' => $mappingkelas 
        );
        $mappingkelas = json_encode($result);
        return $mappingkelas;
    }

    public function tambahMappingKelas(Request $request)
    {
            DB::table('tb_master_kelas')->insert([
            'nama_kelas' => $request->input('kelas') . " " . $request->input('namakelas') ,
            'id_tahun_ajaran' => $request->tahun_ajaran,
            'semester' => $request->semester,
            'id_guru' => $request->walikelas,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }
    public function proses(Request $request)
    {
            DB::table('tb_kelas')->insert([
            'nama_kelas' => $request->namakelas,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function fetchdata($id)
    {
        $mappingkelas = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('tb_master_kelas.*', 'tb_guru.nama_guru', 'tb_tahun_ajaran.tahun_ajaran')
        ->get();

        return response()->json([
          'data' => $mappingkelas
      ]);
    }

    public function updateMappingKelas(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_master_kelas')
        ->where('id_master_kelas', $request->idmappingkelas)
        ->update([
            'nama_kelas' => $request->namakelas,
            'id_tahun_ajaran' => $request->idtahunajaran,
            'semester' => $request->semester,
            'id_guru' => $request->idguru,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteMappingKelas(Request $request)
    {
        try{
            DB::table('tb_master_kelas')
            ->where('id_master_kelas',$request->idmappingkelas)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu mappingan data.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
   }
}
