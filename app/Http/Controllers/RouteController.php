<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    //
    public function tampilUtama()
    {

 $user = Session::get('username');
        $siswa = DB::table('tb_siswa')
        ->where('status',"active")
        ->select(DB::raw('count(tb_siswa.id_siswa) as jumlah_siswa'))
        ->get();

        $guru = DB::table('tb_guru')
        ->where('status',"active")
            ->select(DB::raw('count(tb_guru.id_guru) as jumlah_guru'))
            ->get();

        $soal = DB::table('tb_detail_soal_cbt')
            ->select(DB::raw('count(tb_detail_soal_cbt.id_detail_soal_cbt) as jumlah_soal'))
            ->get();

 


        // mengirim data user ke view index
    return view('konten.home',compact(['siswa'],['guru'],['soal']));
    }

     public function tampilSiswa()
    {
        // mengirim data user ke view index
        return view('konten.homesiswa');
    }

      public function tampilGuru()
    {
        // mengirim data user ke view index
        return view('konten.homeguru');
    }
}
