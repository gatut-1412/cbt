<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class PembelajaranOnlineController extends Controller
{
    //
    public function tampilDafttarTugas()
    {
        $daftar_tugas = null;
        return view('konten.pembelajaranonline.daftar_pembelajaran_perkelas', compact(['daftar_tugas']));
    }
    public function dataPembelajaranPerkelas()
    {
        $nip = Session::get('username');
        $level = Session::get('level');
        if($level== "ADMIN")
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru as a', 'tb_master_kelas.id_guru', '=', 'a.id_guru')
            ->join('tb_guru_mapel', 'tb_guru_mapel.id_master_kelas','=','tb_master_kelas.id_master_kelas')
            ->join('tb_guru as b', 'tb_guru_mapel.id_guru', '=', 'b.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->select('a.nama_guru as nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
        }
        if($level == "GURU")
        {  
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru as a', 'tb_master_kelas.id_guru', '=', 'a.id_guru')
            ->join('tb_guru_mapel', 'tb_guru_mapel.id_master_kelas','=','tb_master_kelas.id_master_kelas')
            ->join('tb_guru as b', 'tb_guru_mapel.id_guru', '=', 'b.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->where('b.nip',$nip)
            ->select('a.nama_guru as nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
        }
        $result = array(
            'data' => $kelassiswa 
        );
        $kelassiswa = json_encode($result);
        return $kelassiswa;
    }

    public function tambahTugas($id)
    {
        $nip = Session::get('username');
        $gurmapel = DB::table('tb_guru_mapel as gm')
        ->join('tb_pelajaran as pel', 'gm.id_pelajaran','=','pel.id_pelajaran')
        ->join('tb_guru as guru', 'guru.id_guru','=','gm.id_guru')
        ->where('guru.nip',$nip)
        ->where('gm.id_master_kelas',$id)
        ->groupBy('pel.id_pelajaran')
        ->get();
        $tambah_tugas = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
        return view('konten.pembelajaranonline.tambah_tugas',compact(['tambah_tugas'],['gurmapel']));
    }

    public function dataTugasOnline()
    {
        $nip = Session::get('username');
        $id_mk =  request()->segment(count(request()->segments()));
        $level = Session::get('level');
        if($level== "ADMIN")
        {
            $tugasonline = DB::table('tb_tugas_online')
            ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran','=','tb_tugas_online.id_pelajaran')
            ->join('tb_guru_mapel', 'tb_pelajaran.id_pelajaran','=','tb_guru_mapel.id_pelajaran')
            ->join('tb_guru', 'tb_guru_mapel.id_guru','=','tb_guru.id_guru')
            ->where('tb_tugas_online.id_master_kelas',$id_mk)
            ->select('id_tugas_online','tb_tugas_online.id_master_kelas','judul_tugas', 'nama_pelajaran', 'tugas_upload1','tugas_upload2','tugas_upload3','soal_esay','tanggal_tugas','status_tugas')
            ->groupBy('tb_tugas_online.id_tugas_online')
            ->get();
        }
        if($level == "GURU")
        {    
            $tugasonline = DB::table('tb_tugas_online')
            ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran','=','tb_tugas_online.id_pelajaran')
            ->join('tb_guru_mapel', 'tb_pelajaran.id_pelajaran','=','tb_guru_mapel.id_pelajaran')
            ->join('tb_guru', 'tb_guru_mapel.id_guru','=','tb_guru.id_guru')
            ->where('tb_tugas_online.id_master_kelas',$id_mk)
            ->where('nip',$nip)
            ->select('id_tugas_online','tb_tugas_online.id_master_kelas','judul_tugas', 'nama_pelajaran', 'tugas_upload1','tugas_upload2','tugas_upload3','soal_esay','tanggal_tugas','status_tugas')
            ->groupBy('tb_tugas_online.id_tugas_online')
            ->get();
        }
        $result = array(
            'data' => $tugasonline 
        );
        $tugasonline = json_encode($result);
        return $tugasonline;
    }

    public function dataTugaslihatSiswa()
    {
        $nis = Session::get('username');
        $tugasonlinesiswa = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto','to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as s','ks.id_siswa','=','s.id_siswa')
        ->join('tb_pelajaran as pel','pel.id_pelajaran','=','to.id_pelajaran')
        ->where('nis',$nis)
        ->whereIn('status_tugas', array('PUBLISH','EXPIRED'))
        ->select('dto.id_tugas_online','dto.id_detail_tugas_online','to.id_master_kelas','judul_tugas','nama_pelajaran', 'soal_esay','tanggal_tugas',DB::raw('DATE_FORMAT(start_date, "%d %M, %Y %H:%i") as start_date1'),DB::raw('DATE_FORMAT(end_date, "%d %M, %Y %H:%i") as end_date1'),'dto.status as statusdto','dto.file_upload1 as filesiswa','status_tugas' )
        ->get();
        $result = array(
            'data' => $tugasonlinesiswa 
        );
        $tugasonlinesiswa = json_encode($result);
        return $tugasonlinesiswa;
    }

    public function uploadTugasSiswa($id3)
    {
        $user = Session::get('username'); 
        $kerjakantugas = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto', 'to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as sis','sis.id_siswa','=','ks.id_siswa')
        ->join('tb_pelajaran as pel','pel.id_pelajaran','=','to.id_pelajaran')
        ->join('tb_guru_mapel as gm','pel.id_pelajaran','=','gm.id_pelajaran')
        ->join('tb_guru as g','g.nip','=','to.created_by')
        ->select('nama_pelajaran','nama_guru','judul_tugas','detail_tugas',DB::raw('DATE_FORMAT(start_date, "%d %M, %Y %H:%i") as start_date1'),DB::raw('DATE_FORMAT(end_date, "%d %M, %Y %H:%i") as end_date1'),'id_detail_tugas_online', 'to.tugas_upload1 as gambartugasonline1', 'to.tugas_upload2 as gambartugasonline2', 'to.tugas_upload3 as gambartugasonline3')
        ->where('to.id_tugas_online',$id3)
        ->where('sis.nis',$user)
        ->groupBy('to.id_tugas_online')
        ->get();
        return view('konten.pembelajaranonline.upload_tugas_siswa',compact('kerjakantugas'));
    }

    //smoy
    public function tambahTugasProses(Request $request)
    {
        $user = Session::get('username'); 
        // $this->validate($request, [
        //     'file' => 'required|file|max:6000',
        // ]);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');
        $file1 = $request->file('file1');
        $file2 = $request->file('file2');
        $tujuan_upload = 'data_file';

        if ($file == null )
        {
            $nama_file = null;
        }
        if ($file1 == null )
        {
            $nama_file1 = null;
        }
        if ($file2 == null )
        {
            $nama_file2 = null;
        }
        if ($file != null )
        {
            $nama_file = date('d-m-Y His')."_1_".$file->getClientOriginalName();
            $file->move($tujuan_upload,$nama_file);
        }
        if ($file1 != null )
        {
            $nama_file1 = date('d-m-Y His')."_2_".$file1->getClientOriginalName();
            $file1->move($tujuan_upload,$nama_file1);
        }
        if ($file2 != null )
        {
            $nama_file2 = date('d-m-Y His')."_3_".$file2->getClientOriginalName();
            $file2->move($tujuan_upload,$nama_file2);
        }

         //bin
        $countsiswa = DB::table('tb_kelas_siswa')->where('id_master_kelas', $request->idmasterkelas)->count();
        $kelassiswa = DB::table('tb_kelas_siswa')->where('id_master_kelas', $request->idmasterkelas)->get();
        $result = array(
          'data' => $kelassiswa,
      );

        $kelassiswa = json_encode($result,true);
        $data2 = json_decode($kelassiswa, true);

        $statement = DB::select("SHOW TABLE STATUS LIKE 'tb_tugas_online'");
        $lastId = $statement[0]->Auto_increment;

        //bin
        for ($i=0; $i <$countsiswa ; $i++) { 

            DB::table('tb_detail_tugas_online')->insert([   
                'id_kelas_siswa' => $data2['data'][$i]['id_kelas_siswa'],
                'id_tugas_online' => $lastId,
                'status' => 'BELUM DIKERJAKAN',
                'created_by' => $user
            ]);
        }

        //smoy    
        $stardate = Date('Y-m-d H:i:s', strtotime($request->start));
        $enddate = Date('Y-m-d H:i:s', strtotime($request->end));
        DB::table('tb_tugas_online')->insert([   
            'id_master_kelas' => $request->idmasterkelas,
            'judul_tugas' => $request->judul,
            'tugas_upload1' => $nama_file,
            'tugas_upload2' => $nama_file1,
            'tugas_upload3' => $nama_file2,
            'detail_tugas' => $request->detail,
            'id_pelajaran' => $request->mapel,
            'soal_esay' => $request->esay,
            'start_date' => $stardate,
            'end_date' => $enddate,
            'status_tugas' => 'DRAFT',
            'status' => 'active',
            'created_by' => $user
        ]);

        return back()->with('success', 'Berhasil Tambah Data.');
    }


    public function lihatDetailTugasGuru($id2)
    {

        $id_tgs =  request()->segment(count(request()->segments()));

        $detailtugasguru = DB::table('tb_tugas_online')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas','=','tb_tugas_online.id_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id2)
        ->select('tb_tugas_online.id_tugas_online','nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
        return view('konten.pembelajaranonline.detail_tugas',compact('detailtugasguru','id_tgs'));
    }

    public function lihatTugasSiswa()
    {
        $nis = Session::get('username');
        $lihattugass = DB::table('tb_siswa as s')
        ->join('tb_kelas_siswa as ks','ks.id_siswa','=','s.id_siswa')
        ->join('tb_master_kelas as mk','ks.id_master_kelas','=','mk.id_master_kelas')
        ->join('tb_tahun_ajaran as ta', 'mk.id_tahun_ajaran', '=', 'ta.id_tahun_ajaran')
        ->join('tb_guru as g', 'mk.id_guru', '=', 'g.id_guru')
        ->where('s.nis',$nis)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','mk.id_master_kelas')
        ->get();
        return view('konten.pembelajaranonline.lihat_tugas_siswa',compact('lihattugass'));
    }

    public function dataDetailSiswaTugas()
    {
        $id_tgs =  request()->segment(count(request()->segments()));

        $detailtugas = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto', 'to.id_tugas_online', '=', 'dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks', 'ks.id_kelas_siswa', '=', 'dto.id_kelas_siswa')
        ->join('tb_siswa as s', 's.id_siswa' , '=', 'ks.id_siswa')
        ->where('to.id_tugas_online',$id_tgs)
        ->select('s.id_siswa','nis','nama_siswa','jenis_kelamin', 'status_tugas','dto.status as detailstatus','dto.file_upload1 as uploadsiswa', DB::raw('DATE_FORMAT(tanggal_upload, "%d %M %Y, Pukul %H:%i") as tanggal_upload'),'id_detail_tugas_online','to.id_tugas_online as id_to')
        ->get();
        $result = array(
            'data' => $detailtugas 
        );
        $detailtugas = json_encode($result);
        return $detailtugas;
    }


    public function updateUploadTugas(Request $request)
    {

        $nis = Session::get('username');
        $tujuan_upload = 'data_file/upload_tugas';

        $file1 = $request->file('file1');
        $file2 = $request->file('file2');
        $file3 = $request->file('file3');
        $file4 = $request->file('file4');
        $file5 = $request->file('file5');  

        if ($file1==null )
        {
            $nama_file1 = $file1;
        }
        if ($file2==null)
        {
            $nama_file2 = $file2;
        }
        if ( $file3==null)
        {
            $nama_file3 = $file3;
        }
        if ( $file4==null  )
        {
            $nama_file4 = $file4;
        }
        if ($file5==null )
        {
            $nama_file5 = $file5;
        }
        if ($file1)
        {
            $nama_file1 = "1"."_".date('d-m-Y His')."_".$file1->getClientOriginalName();
            $file1->move($tujuan_upload,$nama_file1);
        }
        if ($file2)
        {
            $nama_file2 = "2"."_".date('d-m-Y His')."_".$file2->getClientOriginalName();
            $file2->move($tujuan_upload,$nama_file2);
        }
        if ($file3)
        {
            $nama_file3 = "3"."_".date('d-m-Y His')."_".$file3->getClientOriginalName();
            $file3->move($tujuan_upload,$nama_file3);
        }
        if ($file4)
        {
            $nama_file4 = "4"."_".date('d-m-Y His')."_".$file4->getClientOriginalName();
            $file4->move($tujuan_upload,$nama_file4);
        }
        if ($file5)
        {
            $nama_file5 = "5"."_".date('d-m-Y His')."_".$file5->getClientOriginalName();
            $file5->move($tujuan_upload,$nama_file5);
        }

        date_default_timezone_set("Asia/Bangkok");
        DB::table('tb_detail_tugas_online')
        ->where('id_detail_tugas_online',$request->iddto)
        ->update([
            'file_upload1' => $nama_file1,
            'file_upload2' => $nama_file2,
            'file_upload3' => $nama_file3,
            'file_upload4' => $nama_file4,
            'file_upload5' => $nama_file5,
            'jawaban_siswa' => $request->jawabansiswa,
            'status' => 'SUDAH UPLOAD',
            'tanggal_upload' => date('Y-m-d H:i:s'),
            'updated_by' => $nis,
            'updated_timestamp' => date('Y-m-d H:i:s'),

        ]);
        return redirect('/lihat_tugas')->with('success', 'Berhasil tambah data.');
    }

    public function lihatPengerjaan($idto,$iddet)
    {

        $user = Session::get('username'); 
        $datatugas = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto', 'to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as sis','sis.id_siswa','=','ks.id_siswa')
        ->join('tb_pelajaran as pel','pel.id_pelajaran','=','to.id_pelajaran')
        ->join('tb_guru_mapel as gm','pel.id_pelajaran','=','gm.id_pelajaran')
        ->join('tb_guru as g','g.nip','=','to.created_by')
        ->select('nama_pelajaran','nama_guru','judul_tugas','detail_tugas',DB::raw('DATE_FORMAT(start_date, "%d %M, %Y %H:%i") as start_date1'),DB::raw('DATE_FORMAT(end_date, "%d %M, %Y %H:%i") as end_date1'),'id_detail_tugas_online','to.tugas_upload1 as gambartugasonline1', 'to.tugas_upload2 as gambartugasonline2', 'to.tugas_upload3 as gambartugasonline3')
        ->where('to.id_tugas_online',$idto)
        ->where('sis.nis',$user)
        ->groupBy('to.id_tugas_online')
        ->get();

        $detailtugas = DB::table('tb_detail_tugas_online')
        ->where('id_detail_tugas_online',$iddet)
        ->get();
        return view('konten.pembelajaranonline.lihat_pengerjaan_siswa',compact('datatugas','detailtugas'));
    }

    public function lihatPengerjaanSiswa($idto,$iddet)
    {
        $user = Session::get('username'); 
        $datatugas = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto', 'to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as sis','sis.id_siswa','=','ks.id_siswa')
        ->join('tb_pelajaran as pel','pel.id_pelajaran','=','to.id_pelajaran')
        ->join('tb_guru_mapel as gm','pel.id_pelajaran','=','gm.id_pelajaran')
        ->join('tb_guru as g','g.nip','=','to.created_by')
        ->select('nama_pelajaran','nama_guru','judul_tugas','detail_tugas',DB::raw('DATE_FORMAT(start_date, "%d %M, %Y %H:%i") as start_date1'),DB::raw('DATE_FORMAT(end_date, "%d %M, %Y %H:%i") as end_date1'),'id_detail_tugas_online','to.tugas_upload1 as gambartugasonline1', 'to.tugas_upload2 as gambartugasonline2', 'to.tugas_upload3 as gambartugasonline3')
        ->where('to.id_tugas_online',$idto)
        //->where('sis.nis',$user)
        ->groupBy('to.id_tugas_online')
        ->get();

        $detailtugas = DB::table('tb_detail_tugas_online as dto')
        ->join('tb_tugas_online as to', 'to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as sis','sis.id_siswa','=','ks.id_siswa')
        ->join('tb_master_kelas as kel','kel.id_master_kelas','=','to.id_master_kelas')
        ->where('id_detail_tugas_online',$iddet)
        ->get();
        return view('konten.pembelajaranonline.lihat_pengerjaan_siswa_byguru',compact('datatugas','detailtugas'));
    }

    public function detailTugasGuru ($iddto2)
    {
        $user = Session::get('username'); 
        $datatugas = DB::table('tb_tugas_online as to')
        ->join('tb_detail_tugas_online as dto', 'to.id_tugas_online','=','dto.id_tugas_online')
        ->join('tb_kelas_siswa as ks','ks.id_kelas_siswa','=','dto.id_kelas_siswa')
        ->join('tb_siswa as sis','sis.id_siswa','=','ks.id_siswa')
        ->join('tb_pelajaran as pel','pel.id_pelajaran','=','to.id_pelajaran')
        ->join('tb_guru_mapel as gm','pel.id_pelajaran','=','gm.id_pelajaran')
        ->join('tb_guru as g','g.nip','=','to.created_by')
        ->select('nama_pelajaran','nama_guru','judul_tugas','detail_tugas',DB::raw('DATE_FORMAT(start_date, "%d %M, %Y %H:%i") as start_date1'),DB::raw('DATE_FORMAT(end_date, "%d %M, %Y %H:%i") as end_date1'),'id_detail_tugas_online','to.tugas_upload1 as gambartugasonline1', 'to.tugas_upload2 as gambartugasonline2', 'to.tugas_upload3 as gambartugasonline3')
        ->where('to.id_tugas_online',$iddto2)
        //->where('sis.nis',$user)
        ->groupBy('to.id_tugas_online')
        ->get();
        return view('konten.pembelajaranonline.lihat_detail_tugas_guru',compact('datatugas'));
    }

    public function updateKePublish($id)
    {
        $user = Session::get('username'); 
        DB::table('tb_tugas_online')->where('id_tugas_online',$id)->update([
            'status_tugas' => "PUBLISH",
            'updated_by' => $user,
            'updated_timestamp' => now()
        ]);
        return back()->with('success', 'Berhasil Update Data.');
    }

    public function updateKeSelesai($id2)
    {
        $user = Session::get('username'); 
        DB::table('tb_tugas_online')->where('id_tugas_online',$id2)->update([
            'status_tugas' => "EXPIRED",
            'updated_by' => $user,
            'updated_timestamp' => now()
        ]);
        return back()->with('success', 'Berhasil Update Data.');
    }

    public function hapusTugas(Request $request)
    {
        DB::table('tb_tugas_online')
        ->where('id_tugas_online',$request->id)->delete();

        DB::table('tb_detail_tugas_online')
        ->where('id_tugas_online',$request->id)->delete();

        return back()->with('success', 'Berhasil Update Data.');

    }
}
