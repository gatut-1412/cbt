<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuruMapelController extends Controller
{
    //
    public function tampilGuruMapel()
    {
        $guru =  DB::table('tb_guru')->where('status',"active")->get();
        $matpel = DB::table('tb_pelajaran')->where('status',"active")->get();
        $kelas = DB::table('tb_master_kelas')->where('status',"active")->get();

        return view('konten.gurumapel.gurumapel',  compact(['matpel'],['guru'],['kelas']));

    }
    public function dataGuruMapel()
    {
        $gurumapel = DB::table('tb_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->select('id_guru_mapel','nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester')
        ->get();
        $result = array(
            'data' => $gurumapel 
        );
        $gurumapel = json_encode($result);
        return $gurumapel;
    }
    public function tambahGuruMapel(Request $request)
    {
           DB::table('tb_guru_mapel')->insert([
            'id_master_kelas' => $request->kelas ,
            'id_guru' => $request->guru,
            'id_pelajaran' => $request->mapel,
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function editkelas()
    {

        return redirect('/kelas');
    }


   public function hapusGuruMapel(Request $request)
    {

   DB::table('tb_guru_mapel')
     ->where('id_guru_mapel',$request->id)->delete();
    return back()->with('success', 'Berhasil Update Data.');

}

}
