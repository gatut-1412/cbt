<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class absensiController extends Controller
{
    //
    public function tampilAbsensi()
    {
        $absensi = null;
        $siswa = DB::table('tb_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->where('tb_master_kelas.status',"active")
        ->select('tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas','jenis_kelamin')
        ->get();
        return view('konten.absensi.tambah_absensi', compact(['absensi'],['siswa']));
    }

    public function dataAbsensi()
    {
        $absen = DB::table('tb_absensi')
        ->join('tb_kelas_siswa', 'tb_absensi.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->where('tb_absensi.status',"ACTIVE")
        ->select('nama_siswa','nis','nama_kelas','type_absen','semester','tb_master_kelas.id_master_kelas','id_absensi','tanggal_absen','keterangan')
        ->orderby('id_absensi','DESC')
        ->get();
        $result = array(
            'data' => $absen 
        );
        $absen = json_encode($result);
        return $absen;
    }

    public function tampilAbsensiPerkelas()
    {
        $absensi = null;
        return view('konten.absensi.absensi', compact(['absensi']));
    }
    public function dataKelasSiswa()
    {
        $kelassiswa = DB::table('tb_kelas_siswa')
        ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
        ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
        ->get();
        $result = array(
            'data' => $kelassiswa 
        );
        $kelassiswa = json_encode($result);
        return $kelassiswa;
    }

    public function dataSiswaAbsensi()
    {
        $id_mk =  request()->segment(count(request()->segments()));    
        $id_mk = (int)$id_mk;
        $siswa = DB::table('tb_absensi')
        ->rightjoin('tb_kelas_siswa', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_absensi.id_kelas_siswa')
        ->rightjoin('tb_master_kelas', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
        ->rightjoin('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->select('tb_siswa.id_siswa', 'nama_siswa', 'nis', 'jenis_kelamin','tb_kelas_siswa.id_kelas_siswa',
            DB::raw('sum(case when type_absen like "IJIN"  then 1 else 0 end) as ijin1'),
            DB::raw('sum(case when type_absen like "SAKIT" then 1 else 0 end) as sakit1'),
            DB::raw('sum(case when type_absen like "ALPA" then 1 else 0 end) as alpa1'),
            DB::raw('(sum(case when type_absen like "IJIN"  then 1 else 0 end))+(sum(case when type_absen like "SAKIT" then 1 else 0 end))+(sum(case when type_absen like "ALPA" then 1 else 0 end)) as jumlah'))
        ->groupBy('tb_kelas_siswa.id_kelas_siswa')
        ->where('tb_master_kelas.id_master_kelas',$id_mk)
        ->where('tb_siswa.status',"active")->get();
        $result = array(
            'data' => $siswa 
        );
        $siswa = json_encode($result);
        return $siswa;
    }

    public function proses(Request $request)
    {
        $dataTanggal = Date('Y-m-d', strtotime($request->tgl_absen));
        DB::table('tb_absensi')->insert([
            'id_kelas_siswa' => $request->siswa,
            'type_absen' => $request->type,
            'tanggal_absen' => $dataTanggal,
            'keterangan' => $request->keterangan,
            'created_by' => "smoy",
            'status' => "ACTIVE"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
        //return redirect('/kelas');
    }

    public function detailAbsensi($id)
    {
        $absensi = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
        return view('konten.absensi.update_absensi',compact('absensi'));
    }

    public function historyAbsensi($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester')
        ->get();

        $absensi = DB::table('tb_absensi')
        ->where('id_kelas_siswa',$id)
        ->select('type_absen',DB::raw('DATE_FORMAT(tanggal_absen, "%d %M %Y ") as tanggal_absen1'),'keterangan')
        ->get();
        return view('konten.absensi.history_absensi',compact(['datasiswa'],('absensi')));
    }

    public function fetchdata($id)
    {
        $siswa = DB::table('tb_absensi')
        ->rightjoin('tb_kelas_siswa', 'tb_absensi.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->rightjoin('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->where('tb_absensi.id_absensi',$id)
        ->select('id_absensi as id','tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','jenis_kelamin','tanggal_absen', 'keterangan', 'type_absen')
        ->groupBy('nis')
        ->get();

         return response()->json([
          'data' => $siswa
        ]);
    }

    public function updateAbsensi(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_absensi')
        ->where('id_absensi', $request->idabsen)
        ->update([
            'type_absen' => $request->type,
            'tanggal_absen' => $dataTanggal,
            'keterangan' => $request->ket,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteAbsensi(Request $request)
    {
        DB::table('tb_absensi')
       ->where('id_absensi',$request->idabsen)->delete();

        return back()->with('success', 'Data berhasil Dihapus.');
    }

    //START
    //-----------------------------------------SISWA ABSENSI----------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function siswaAbsensi()
    {
        return view('konten.absensi.siswa_absensi');
    }
    public function dataSiswaAbsensiSiswa()
    {
        $user = Session::get('username');   
        $absen = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_siswa.nis',$user)
        ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas','tb_kelas_siswa.id_kelas_siswa')
        ->groupBy('tahun_ajaran','nama_kelas')
        ->get();
        $result = array(
          'data' => $absen 
      );
        $absen = json_encode($result);
        return $absen;
    }
    public function siswaAbsensiDetail($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester','nama_guru')
        ->get();

        $absensi = DB::table('tb_absensi')
        ->where('id_kelas_siswa',$id)
        ->select('type_absen',DB::raw('DATE_FORMAT(tanggal_absen, "%d %M %Y ") as tanggal_absen1'),'keterangan')
        ->orderby('id_absensi')
        ->get();
        return view('konten.absensi.siswa_history_absensi',compact(['datasiswa'],('absensi')));
    }
    //END
    //----------------------------------- SISWA CATATAN WALKES -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
}
