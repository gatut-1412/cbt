<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class PengumumanCbtController extends Controller
{
    //
    public function tampilPengumumanCbt()
    {

        return view('konten.pengumuman_cbt.pengumuman_cbt');

    }
    public function dataPengumumanCbt()
    {
        $user = Session::get('username');
        $mappcbt = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
        ->join('tb_kelas_siswa','tb_kelas_siswa.id_master_kelas','=','tb_master_kelas.id_master_kelas')
        ->join('tb_siswa','tb_siswa.id_siswa','=','tb_kelas_siswa.id_siswa')
        ->where('tb_siswa.nis',$user)
        ->whereIn('tb_mapping_cbt.status_mapping', array('DRAFT','PUBLISH'))
        //->select('nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester')
        ->get();
        $result = array(
            'data' => $mappcbt 
        );
        $mappcbt = json_encode($result);
        return $mappcbt;
    }

    public function detailPengumumanCbt($id)
    {
        $user = Session::get('username');
        $detailpeng = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
        ->join('tb_kelas_siswa','tb_kelas_siswa.id_master_kelas','=','tb_master_kelas.id_master_kelas')
        ->join('tb_siswa','tb_siswa.id_siswa','=','tb_kelas_siswa.id_siswa')
        ->where('tb_siswa.nis',$user)
        ->where('tb_soal_cbt.id_soal_cbt',$id)
        ->whereIn('tb_mapping_cbt.status_mapping', array('DRAFT','PUBLISH'))
        ->select('nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester','jenis_soal','nis','nama_siswa',DB::raw('DATE_FORMAT(tanggal, "%d %M, %Y") as tanggal'),DB::raw('DATE_FORMAT(tanggal, "%H:%i") as jam'))
        ->get();
        return view('konten.pengumuman_cbt.detailpengumumancbt',compact(['detailpeng']));

    }
}
