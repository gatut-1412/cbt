<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembersController extends Controller
{
    //
    public function tampilMembers()
    {
        return view('konten.members.members', compact(['members']));
    }

    public function dataMembers()
    {
        $members = DB::table('mst_members')
                    ->where('status','!=',"inactive")
                    ->get();
        $result = array(
        'data' => $members 
        );
        $members = json_encode($result);
        return $members;
    }

    public function tambahMembers()
    {
        return view('konten.members.tambah_members', compact(['tambah']));
    }

    public function prosesTambahMembers(Request $request)
    {
    	// var_dump($request->nama);
    	// exit();
            DB::table('mst_users')->insert([
            'username' => $request->username,
            'password' => md5($request->password),
            'nama' => $request->nama,
            'email' => $request->email,
            'level' => $request->level,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return redirect('/members');
    }

    public function editMembers()
    {

        return redirect('/members');
    }
}
