<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class PrintController extends Controller

{
	public function generatePDF($idsoal)
	{
		// $detailsoal = DB::table('tb_soal_cbt')
		// ->join('tb_mapping_cbt', 'tb_mapping_cbt.id_soal_cbt', '=', 'tb_soal_cbt.id_soal_cbt')
		// ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
		// ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
		// ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
		// ->select('tb_soal_cbt.id_soal_cbt','nama_kelas','nama_pelajaran','jenis_soal','waktu')
		// ->where('tb_soal_cbt.id_soal_cbt',$idsoal)
		// ->Groupby('tb_soal_cbt.id_soal_cbt')
		// ->get();

		$detailsoal = DB::table('tb_soal_cbt')
		->join('tb_detail_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_detail_soal_cbt.id_soal_cbt')
		->select('tb_soal_cbt.id_soal_cbt','judul_soal','jenis_soal','waktu',DB::raw('count(tb_detail_soal_cbt.id_detail_soal_cbt) as jumlah'))
		->where('tb_soal_cbt.id_soal_cbt',$idsoal)
		->Groupby('tb_soal_cbt.id_soal_cbt')
		->get();

		$soal = DB::table('tb_soal_cbt')
		->join('tb_detail_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_detail_soal_cbt.id_soal_cbt')
		->where('tb_soal_cbt.id_soal_cbt',$idsoal)->get();
        // $data = ['title' => 'Welcome to belajarphp.net'];

		$pdf = PDF::loadView('print.print_soal', compact(['soal'],['detailsoal']));
        // return $pdf->download('laporan-pdf.pdf');
		return $pdf->stream();
	}
	public function generatePDFJawabanSiswa($idujian)
	{
		$query = DB::table('tb_mapping_cbt')
		->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
		->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
		->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
		->join('tb_kelas_siswa', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
		->join('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
		->join('tb_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_mapping_cbt.id_soal_cbt')
		->join('tr_ikut_ujian as aa', 'aa.id_tes', '=', 'tb_mapping_cbt.id_mapping_cbt')
		->join('tr_ikut_ujian', 'tr_ikut_ujian.id_user', '=', 'tb_siswa.id_siswa')
		->select('nama_siswa','tr_ikut_ujian.list_jawaban', 'tr_ikut_ujian.list_soal','nama_kelas','waktu','jenis_soal','nama_pelajaran')
		->where('tr_ikut_ujian.id',$idujian)
		->Groupby('nama_siswa')->get();

		foreach($query as $data)
		{
			$jawabann = [];
			$a = [];
			// var_dump($query);
			// exit();
			$listjawaban = $data->list_jawaban;
			$arai2 = explode(",", $listjawaban);
			for($i = 0; $i<count($arai2); $i++){
				$int = $arai2[$i];
				$arai3 = explode(":", $int);
				for($s = 0; $s<2; $s++){
					if ($s % 2 == 0) {
						$soal1[] = $arai3[$s];

					}else{
						$jawaban[] = $arai3[$s];
					}

				}
			}

			$query1 = DB::table('tb_detail_soal_cbt')
			->select('id_detail_soal_cbt','soal_pg','gambar_soal')
			->whereIn('id_detail_soal_cbt',$soal1)->get();

			$query2 = DB::table('tb_detail_soal_cbt')
			->select('pilihan_a', 'pilihan_b', 'pilihan_c', 'pilihan_d', 'pilihan_e')
			->whereIn('id_detail_soal_cbt',$soal1)->get();
			$result = array(
				'data' => $query2 
			);
			$query2 = json_encode($result,true);
			$query2 = json_decode($query2,true);

			for ($k=0; $k <count($jawaban) ; $k++) { 
				if($jawaban[$k] == "A")    {
					$jawabann[] = $query2['data'][$k]['pilihan_a'];
					$a[] = "A";
				}else if($jawaban[$k] == "B")    {
					$jawabann[] = $query2['data'][$k]['pilihan_b'];
					$a[] = "B";
				}else  if($jawaban[$k] == "C")    {
					$jawabann[] = $query2['data'][$k]['pilihan_c'];
					$a[] = "C";
				}else  if($jawaban[$k] == "D")    {
					$jawabann[] = $query2['data'][$k]['pilihan_d'];
					$a[] = "D";
				}else  if($jawaban[$k] == "E")    {
					$jawabann[] = $query2['data'][$k]['pilihan_e'];
					$a[] = "E";
				}else{
					$jawabann[] =" ";
					$a[] = " ";

				}
			}
		}
		// var_dump($jawabann,$a);
		// exit();

		$pdf = PDF::loadView('print.print_jawaban_siswa', compact(['query'],['query1'],['jawabann'],['a']));
        // return $pdf->download('laporan-pdf.pdf');
		return $pdf->stream();
	}

	public function generatePDFSiswa($id)
	{
		$siswa = DB::table('tb_siswa')
		->where('id_siswa',$id)
		->get();

		$pdf = PDF::loadView('print.print_profil_siswa', compact(['siswa']));
        // return $pdf->download('laporan-pdf.pdf');
		return $pdf->stream();
	}

	public function generatePDFGuru($id)
	{
		$guru = DB::table('tb_guru')
		->where('id_guru',$id)
		->get();

		$pdf = PDF::loadView('print.print_profil_guru', compact(['guru']));
        // return $pdf->download('laporan-pdf.pdf');
		return $pdf->stream();
	}
}