<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class ProfilController extends Controller
{

	public function profilGuru()
	{
		$user = Session::get('username');
		$level = Session::get('level');
		$guru = DB::table('tb_guru')->where('nip',$user)->get();
		return view('konten.guru.view_data_guru',compact(['guru']));
	}
	public function profilSiswa()
	{
		$user = Session::get('username');
		$level = Session::get('level');
		$siswa = DB::table('tb_siswa')->where('nis',$user)->get();
		return view('konten.siswa.view_data_siswa',compact(['siswa']));
	}

	public function updateProfilSiswa(Request $request)
	{
		date_default_timezone_set("Asia/Bangkok");

		$user = Session::get('username');
		$level = Session::get('level');

		$file = $request->file('pasphotosiswa');
		if ($file == null )
		{
			$nama_file = null;
			DB::table('tb_siswa')
			->where('tb_siswa.nis', $user)
			->update([
				'nama_siswa' => $request->namasiswa,
				'jenis_kelamin' => $request->jeniskelamin,
				'tempat_lahir' => $request->tempatlahir,
				'tanggal_lahir' => $request->tanggallahir,
				'agama' => $request->agama,
				'alamat' => $request->alamat,
				'dikelas' => $request->dikelas,
				'asal_sekolah' => $request->asalsekolah,
				'tanggal_dikelas' => $request->tanggaldikelas,
				'status_keluarga' => $request->statuskeluarga,
				'anak_ke' => $request->anakke,
				'nama_ayah' => $request->namaayah,
				'nama_ibu' => $request->namaibu,
				'alamat_orangtua' => $request->alamatorangtua,
				'pekerjaan_ayah' => $request->pekerjaanayah,
				'pekerjaan_ibu' => $request->pekerjaanibu,
				'updated_by' => $user,
				'updated_timestamp' => date('Y-m-d H:i:s')

			]);
		}
		if ($file != null )
		{
			$nama_file = $request->nis."_".$request->namasiswa.".".$file->getClientOriginalExtension();
			$file->move('assets/images/photo/siswa/',$nama_file);
			DB::table('tb_siswa')
			->where('tb_siswa.nis', $user)
			->update([
				'nama_siswa' => $request->namasiswa,
				'jenis_kelamin' => $request->jeniskelamin,
				'tempat_lahir' => $request->tempatlahir,
				'tanggal_lahir' => $request->tanggallahir,
				'agama' => $request->agama,
				'alamat' => $request->alamat,
				'pas_photo_siswa'  => $nama_file,
				'dikelas' => $request->dikelas,
				'asal_sekolah' => $request->asalsekolah,
				'tanggal_dikelas' => $request->tanggaldikelas,
				'status_keluarga' => $request->statuskeluarga,
				'anak_ke' => $request->anakke,
				'nama_ayah' => $request->namaayah,
				'nama_ibu' => $request->namaibu,
				'alamat_orangtua' => $request->alamatorangtua,
				'pekerjaan_ayah' => $request->pekerjaanayah,
				'pekerjaan_ibu' => $request->pekerjaanibu,
				'updated_by' => $user,
				'updated_timestamp' => date('Y-m-d H:i:s')

			]);
		}

		return back()->with('success', 'Data berhasil disimpan.');
	}
	

	public function updateProfilGuru(Request $request)
	{
		date_default_timezone_set("Asia/Bangkok");

		$user = Session::get('username');
		$file = $request->file('pasphotoguru');
        $tgllahir = Date('Y-m-d', strtotime($request->tgllahir));

        if ($file == null )
        {
            $nama_file = null;
            DB::table('tb_guru')
            ->where('nip', $request->nip)
            ->update([
                'tempat_lahir' => $request->tempatlahir,
                'tanggal_lahir' => $tgllahir,
                'nik' => $request->nik,
                'notelp' => $request->notelp,
                'email_guru' => $request->emailguru,
                'agama' => $request->agama,
                'domisili' => $request->domisili,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'alamat' => $request->alamat,
                'desakelurahan' => $request->deskel,
                'status_nikah' => $request->statusnikah,
                'nama_ibu' => $request->namaibu,
                'nama_istrisuami' => $request->namaissu,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }
        if ($file != null )
        {
            $nama_file = $request->nip."_".$request->namaguru.".".$file->getClientOriginalExtension();
            $file->move('assets/images/photo/guru/',$nama_file);
            DB::table('tb_guru')
            ->where('nip', $request->nip)
            ->update([
                'tempat_lahir' => $request->tempatlahir,
                'tanggal_lahir' => $tgllahir,
                'pas_photo_guru' => $nama_file,
                'nik' => $request->nik,
                'notelp' => $request->notelp,
                'email_guru' => $request->emailguru,
                'agama' => $request->agama,
                'domisili' => $request->domisili,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'alamat' => $request->alamat,
                'desakelurahan' => $request->deskel,
                'status_nikah' => $request->statusnikah,
                'nama_ibu' => $request->namaibu,
                'nama_istrisuami' => $request->namaissu,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }

		return back()->with('success', 'Data berhasil disimpan.');
	}


}
