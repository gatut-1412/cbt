<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;


class catatanBKController extends Controller
{
    //START
    //--------------------------------------- GURU CATATAN BK  -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function tampilCatatanBK1()
    {
      $guru = DB::table('tb_guru')->where('status',"active")->get();  
        $siswa = DB::table('tb_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->where('tb_master_kelas.status',"active")
        ->select('tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas','jenis_kelamin')
        ->get();
        return view('konten.catatan_bk.catatan_bk', compact(['siswa'],['guru']));
    }

    public function dataCatatan()
    {
        $databk = DB::table('tb_catatan_bk')
        ->join('tb_kelas_siswa', 'tb_catatan_bk.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->select('id_catatan_bk as id','tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas','jenis_kelamin','catatan', DB::raw('DATE_FORMAT(tanggal, "%d %M, %Y") as tanggal'))
        ->where('tb_catatan_bk.status',"active")->get();

        $result = array(
        'data' => $databk 
        );
        $databk = json_encode($result);
        return $databk;
    }
    //
    public function lihatCatBKPerkelas()
    {
        return view('konten.catatan_bk.catatan_bk_perkelas');
    }

    public function dataCatatanBKPerKelas()
    {
        $user = Session::get('username');   
        $level = Session::get('level');
        $walkes = Session::get('walikelas');
        $bk = Session::get('is_bk');
        // var_dump($level, $walkes, $bk);
        // exit();
        if($level== "GURU" && $walkes=="Y" && $bk=="N" )
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->where('tb_guru.nip',$user)
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
        else
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
    }

    public function lihatCatBKPerkelasSiswa($id)
    {
        $catbk = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
        $this->dataCatatanBKPerKelasSiswa($id);
        return view('konten.catatan_bk.catatan_bk_perkelas_siswa',compact('catbk'));
    }

    public function dataCatatanBKPerKelasSiswa($id)
    {
        $data = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->leftjoin('tb_catatan_bk', 'tb_catatan_bk.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->select('nama_siswa','jenis_kelamin','nama_kelas','nis', 'tb_kelas_siswa.id_kelas_siswa','tanggal','catatan',
            DB::raw('count(tb_catatan_bk.id_kelas_siswa) as jumlah'))
        ->groupBy('tb_kelas_siswa.id_kelas_siswa')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->orderby('nis')
        ->groupBy('tb_catatan_bk.id_kelas_siswa')
        ->get();
        $result = array(
          'data' => $data 
      );
        $data = json_encode($result);
        return $data;
    }

    public function historyCatatanBK($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester')
        ->get();

        $catatanbk = DB::table('tb_catatan_bk')
        ->where('id_kelas_siswa',$id)
        ->select(DB::raw('DATE_FORMAT(tanggal, "%d %M %Y ") as tanggal'),'catatan')
        ->get();
        return view('konten.catatan_bk.history_catatan_bk',compact(['datasiswa'],('catatanbk')));
    }
 
    public function tambahCatatanBk(Request $request)
    {
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
            DB::table('tb_catatan_bk')->insert([
            'id_kelas_siswa' => $request->input('kelas'),
            'catatan' => $request->catatan,
            'tanggal' => $dataTanggal,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function fetchdata($id)
    {
        $siswa = DB::table('tb_catatan_bk')
        ->rightjoin('tb_kelas_siswa', 'tb_catatan_bk.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->rightjoin('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->where('tb_catatan_bk.id_catatan_bk',$id)
        ->select('id_catatan_bk as id','tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','jenis_kelamin','tanggal', 'catatan')
        ->groupBy('nis')
        ->get();

         return response()->json([
          'data' => $siswa
        ]);
    }

    public function updateCatatanBK(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_catatan_bk')
        ->where('id_catatan_bk', $request->idcatbk)
        ->update([
            'tanggal' => $dataTanggal,
            'catatan' => $request->cat_bk,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteCatatanBK(Request $request)
    {
        DB::table('tb_catatan_bk')
       ->where('id_catatan_bk',$request->idcatbk)->delete();

        return back()->with('success', 'Data berhasil Dihapus.');
    }
    //END
    //--------------------------------------- GURU CATATAN BK  -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//

    //START
    //-------------------------------------  SISWA CATATAN BK  -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function siswaCatatanBK()
    {
        return view('konten.catatan_bk.siswa_catatan_bk');
    }
    public function dataSiswaCatatanBK()
    {
        $user = Session::get('username');   
        $kelassiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_siswa.nis',$user)
        ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas','tb_kelas_siswa.id_kelas_siswa')
        ->groupBy('tahun_ajaran','nama_kelas')
        ->get();
        $result = array(
          'data' => $kelassiswa 
      );
        $kelassiswa = json_encode($result);
        return $kelassiswa;
    }
    public function siswaCatatanBKdetail($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester')
        ->get();

        $catatanbk = DB::table('tb_catatan_bk')
        ->where('id_kelas_siswa',$id)
        ->select(DB::raw('DATE_FORMAT(tanggal, "%d %M %Y ") as tanggal'),'catatan')
        ->get();
        return view('konten.catatan_bk.siswa_history_catatan_bk',compact(['datasiswa'],('catatanbk')));
    }
    //END
    //-------------------------------------  SISWA CATATAN BK  -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
}
