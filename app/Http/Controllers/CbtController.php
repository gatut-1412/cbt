<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Session;
use Excel;


class cbtController extends Controller
{
    //smoy
    public function tampilSoalCbt()
    {
        $user = Session::get('username');   
        $level = Session::get('level');
        if($level== "GURU")
        {
            $mapel = DB::table('tb_guru_mapel')
            ->join('tb_guru', 'tb_guru.id_guru', '=', 'tb_guru_mapel.id_guru') 	
            ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
            ->where('tb_guru.nip',$user)
            ->groupby('tb_pelajaran.id_pelajaran')
            ->get(); 
        }
        else 
        {
            $mapel = DB::table('tb_pelajaran')->where('status','active')->get();
        }
        $filename = Str::random(8);
        return view('konten.cbt.soalcbt', compact(['filename'],['mapel']));
    }
    //smoy
    public function dataSoalCbt()
    {
        $user = Session::get('username');
        $level = Session::get('level');
        if($level== "ADMIN"){
            $datasoalcbt = DB::table('tb_soal_cbt')
            ->join('mst_users', 'mst_users.username', '=', 'tb_soal_cbt.created_by')
            ->where('tb_soal_cbt.status',"active")->get(); 
        }
        if($level == "GURU")
        {
           $datasoalcbt = DB::table('tb_soal_cbt')
           ->join('tb_guru', 'tb_guru.nip', '=', 'tb_soal_cbt.created_by')
           ->join('tb_guru_mapel', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
           ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
           ->where('tb_soal_cbt.created_by',$user)->where('tb_soal_cbt.status',"active")->groupby('tb_soal_cbt.id_soal_cbt')->get(); 
        }

        $result = array(
        'data' => $datasoalcbt 
        );
        $datasoalcbt = json_encode($result);
        return $datasoalcbt;
    }
    
    //smoy
    public function tambahSoalCbt(Request $request)
    {

        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        $user = Session::get('username');
        $que = DB::table('tb_soal_cbt')->insertGetId([
            'judul_soal' => $request->input('judulsoal'),
            'jenis_soal' => $request->jenissoal,
            'tanggal_dibuat' => $dataTanggal,
            'waktu' => $request->waktu,
            'token' => $request->token,
            'created_by' => $user,
            'status' => "ACTIVE"
        ]);        

        $this->tambahDetailSoalCbt($que);
        return redirect('/soalcbt/tambahdetailsoalcbt/'.$que);
        //return back()->with('success', 'Berhasil Tambah Data.');
    }
    //smoy
    public function tambahDetailSoalCbt($que)
    {
        $template =  DB::table('tb_format_upload')->where('menu_upload',"data_soal_cbt_pg")->get();
        $soalcbt = DB::table('tb_soal_cbt')
        // ->where('status',"active")
        ->where('id_soal_cbt',$que)
        ->get(); 
        $this->dataDetailSoalCbt($que);
        $this->dataDetailSoalCbtEsay($que);
        return view('konten.cbt.tambah_detail_soal_cbt', compact(['soalcbt'],['template']));
    }


    public function editsoalpgcbt($id)
    {
        $template =  DB::table('tb_format_upload')->where('menu_upload',"data_soal_cbt_pg")->get();

        $datadetailsoalcbt = DB::table('tb_soal_cbt')
        ->join('tb_detail_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_detail_soal_cbt.id_soal_cbt')
        ->where('tb_soal_cbt.status',"active")
        ->where('tb_detail_soal_cbt.id_detail_soal_cbt',$id)
        ->select('tb_detail_soal_cbt.id_detail_soal_cbt','tb_detail_soal_cbt.id_soal_cbt','gambar_soal','soal_pg','pilihan_a','pilihan_b','pilihan_c','pilihan_d', 'pilihan_e', 'kunci_jawaban','bobot')
        ->get();

        $soalcbt = DB::table('tb_soal_cbt')
        ->where('status',"active")
        ->where('id_soal_cbt',$id)
        ->get(); 
        $this->dataDetailSoalCbtEsay($id);
        // var_dump($datadetailsoalcbt);
        // exit();
        return view('konten.cbt.edit_detail_soal_cbt', compact(['soalcbt'],['datadetailsoalcbt'],['template']));
    }

    //smoy
    public function dataDetailSoalCbt($que)
    {
        $datadetailsoalcbt = DB::table('tb_soal_cbt')
        ->join('tb_detail_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_detail_soal_cbt.id_soal_cbt')
        ->where('tb_soal_cbt.status',"active")
        ->where('tb_detail_soal_cbt.id_soal_cbt',$que)
        ->select('tb_detail_soal_cbt.id_detail_soal_cbt','tb_detail_soal_cbt.id_soal_cbt','soal_pg','gambar_soal','pilihan_a','pilihan_b','pilihan_c','pilihan_d', 'pilihan_e', 'kunci_jawaban','bobot')
        ->get();

        $result = array(
            'data' => $datadetailsoalcbt 
        );
        $datadetailsoalcbt = json_encode($result);
        return $datadetailsoalcbt;
    }
    //smoy
    public function dataDetailSoalCbtEsay($que)
    {
        $datadetailsoalcbtesay = DB::table('tb_soal_cbt')
        ->join('tb_detail_soal_cbt_esay', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_detail_soal_cbt_esay.id_soal_cbt')
        ->where('tb_soal_cbt.status',"active")
        ->where('tb_detail_soal_cbt_esay.id_soal_cbt',$que)
        ->get();

        $result = array(
            'data' => $datadetailsoalcbtesay 
        );
        $datadetailsoalcbtesay = json_encode($result);
        return $datadetailsoalcbtesay;
    }
    //smoy
    public function tambahSoalCbtDetailPg(Request $request)
    {
        $filei = $request->file('pil_a');
        $texti = $request->input('pil_a');
        if ($texti == !Null && $filei == Null){
            if (!empty($request->file('file'))) {
                $filesoal = $request->file('file');
                $nama_filesoal = date('d-m-Y His')."_".$filesoal->getClientOriginalName();
                $tujuan_uploadsoal = 'soal_cbt/pg/soal';
                $filesoal->move($tujuan_uploadsoal,$nama_filesoal);

            }
            else {
             $nama_filesoal = null;
            }

            $a = (int)$request->idsoal;
            $user = Session::get('username');
            DB::table('tb_detail_soal_cbt')->insert([
                'soal_pg' => $request->input('soal'),
                'gambar_soal' => $nama_filesoal,
                'id_soal_cbt' => $a,
                'pilihan_a' => $request->pil_a,
                'pilihan_b' => $request->pil_b,
                'pilihan_c' => $request->pil_c,
                'pilihan_d' => $request->pil_d,
                'pilihan_e' => $request->pil_e,
                'kunci_jawaban' => $request->kunci,
                'bobot' => $request->bobot,
                'created_by' => $user,
            ]);        

            return back()->with('success', 'Berhasil Tambah Data.');
        }

        else if (($texti == Null && $filei == !Null)|| ($texti == !Null && $filei == !Null))  {

            $tujuan_upload = 'soal_cbt/pg/option';

            $file = $request->file('pil_a');
            $file1 = $request->file('pil_b');
            $file2 = $request->file('pil_c');
            $file3 = $request->file('pil_d');
            $file4 = $request->file('pil_e');

            $nama_file = date('d-m-Y His')."_".$file->getClientOriginalName();
            $nama_file1 = date('d-m-Y His')."_".$file1->getClientOriginalName();
            $nama_file2 = date('d-m-Y His')."_".$file2->getClientOriginalName();
            $nama_file3 = date('d-m-Y His')."_".$file3->getClientOriginalName();
            $nama_file4 = date('d-m-Y His')."_".$file4->getClientOriginalName();

            $file->move($tujuan_upload,$nama_file);
            $file1->move($tujuan_upload,$nama_file1);
            $file2->move($tujuan_upload,$nama_file2);
            $file3->move($tujuan_upload,$nama_file3);
            $file4->move($tujuan_upload,$nama_file4);

            $a = (int)$request->idsoal;
            $user = Session::get('username');
            DB::table('tb_detail_soal_cbt')->insert([
                'soal_pg'       => $request->input('soal'),
                'id_soal_cbt'   => $a,
                'pilihan_a'     => $nama_file,
                'pilihan_b'     => $nama_file1,
                'pilihan_c'     => $nama_file2,
                'pilihan_d'     => $nama_file3,
                'pilihan_e'     => $nama_file4,
                'kunci_jawaban' => $request->kunci,
                'bobot'         => $request->bobot,
                'created_by'    => $user,
            ]);        

            return back()->with('success', 'Berhasil Tambah Data.');
        }
        else if ($texti == Null && $filei == Null){
            return back()->with('gagal', 'Option Pilihan Ganda Tidak Boleh Kosong.');
        }

    }

    //bin
    public function hapusSemuaSoalPg(Request $request)
    {
       DB::table('tb_detail_soal_cbt')
       ->where('id_soal_cbt',$request->id)->delete();
       DB::table('tb_soal_cbt')
       ->where('id_soal_cbt',$request->id)->delete();

       $mapel = DB::table('tb_pelajaran')->where('status',"active")->get(); 
       $filename = Str::random(8);
       return redirect('/soalcbt')->with('success', 'Data Berhasil dihapus.');
    }

   public function hapusSoalCbtDetailPg(Request $request)
   {

       DB::table('tb_detail_soal_cbt')
       ->where('id_detail_soal_cbt',$request->id_detail)->delete();
       return back()->with('success', 'Berhasil Update Data.');
   }

   public function editSoalCbtDetailPg(Request $request)
   {
    $filei = $request->file('pil_a');
    $texti = $request->input('pil_a');

    if ($texti == !Null && $filei == Null){

        if (!empty($request->file('file'))) {
            $filesoal = $request->file('file');
            $nama_filesoal = date('d-m-Y His')."_".$filesoal->getClientOriginalName();
            $tujuan_uploadsoal = 'soal_cbt/pg/soal';
            $filesoal->move($tujuan_uploadsoal,$nama_filesoal);

        } 
        else {
           $nama_filesoal = null;
        }


        $a = (int)$request->idsoal;
        $user = Session::get('username');

        DB::table('tb_detail_soal_cbt')
        ->where('tb_detail_soal_cbt.id_detail_soal_cbt', $request->id_detail_soal_cbt)
        ->update([
            'soal_pg' => $request->input('soal'),
            'gambar_soal' => $nama_filesoal,
            'id_soal_cbt' => $a,
            'pilihan_a' => $request->pil_a,
            'pilihan_b' => $request->pil_b,
            'pilihan_c' => $request->pil_c,
            'pilihan_d' => $request->pil_d,
            'pilihan_e' => $request->pil_e,
            'kunci_jawaban' => $request->kunci,
            'bobot' => $request->bobot,
            'created_by' => $user,
        ]);

        return back()->with('success', 'Data berhasil disimpan.');
    }
    else if (($texti == Null && $filei == !Null)|| ($texti == !Null && $filei == !Null))  {

        $tujuan_upload = 'soal_cbt/pg/option';

        $file = $request->file('pil_a');
        $file1 = $request->file('pil_b');
        $file2 = $request->file('pil_c');
        $file3 = $request->file('pil_d');
        $file4 = $request->file('pil_e');

        $nama_file = date('d-m-Y His')."_".$file->getClientOriginalName();
        $nama_file1 = date('d-m-Y His')."_".$file1->getClientOriginalName();
        $nama_file2 = date('d-m-Y His')."_".$file2->getClientOriginalName();
        $nama_file3 = date('d-m-Y His')."_".$file3->getClientOriginalName();
        $nama_file4 = date('d-m-Y His')."_".$file4->getClientOriginalName();

        $file->move($tujuan_upload,$nama_file);
        $file1->move($tujuan_upload,$nama_file1);
        $file2->move($tujuan_upload,$nama_file2);
        $file3->move($tujuan_upload,$nama_file3);
        $file4->move($tujuan_upload,$nama_file4);

        $a = (int)$request->idsoal;
        $user = Session::get('username');

        DB::table('tb_detail_soal_cbt')
        ->where('tb_detail_soal_cbt.id_detail_soal_cbt', $request->id_detail_soal_cbt)
        ->update([
           'soal_pg'       => $request->input('soal'),
           'id_soal_cbt'   => $a,
           'pilihan_a'     => $nama_file,
           'pilihan_b'     => $nama_file1,
           'pilihan_c'     => $nama_file2,
           'pilihan_d'     => $nama_file3,
           'pilihan_e'     => $nama_file4,
           'kunci_jawaban' => $request->kunci,
           'bobot'         => $request->bobot,
           'created_by'    => $user,
        ]);      

        return back()->with('success', 'Berhasil Simpan  Data.');
        }
        else if ($texti == Null && $filei == Null){
            return back()->with('gagal', 'Option Pilihan Ganda Tidak Boleh Kosong.');
        }

    }

    //bin

    //smoy
    public function tambahSoalCbtDetailEsay(Request $request)
    {
        $filesoal = $request->file('file');
        $nama_filesoal = date('d-m-Y His')."_".$filesoal->getClientOriginalName();
        $tujuan_uploadsoal = 'soal_cbt/esay/soal';
        $filesoal->move($tujuan_uploadsoal,$nama_filesoal);

        $user = Session::get('username');
        $b = (int)$request->idsoal;
        DB::table('tb_detail_soal_cbt_esay')->insert([
            'soal_esay' => $request->input('soalesay'),
            'id_soal_cbt' => $b,
            'gambar' => $nama_filesoal,
            'bobot' => $request->bobot,
            'created_by' => $user,
        ]); 

        return back()->with('success', 'Berhasil Tambah Data.');
    }
    //smoy
    Public function importSoalCbt(Request $request)
    {
        $c = (int)$request->idsoal;
        $user = Session::get('username');
        $this->validate($request, ['select_file'  => 'required|mimes:xls,xlsx']);
        $path = $request->file('select_file')->getRealPath();
        $data = Excel::load($path)->get();
        if($data->count() > 0)
        {
            foreach($data->toArray() as $key => $value)
            {
                foreach($value as $row)
                {
                    $insert_data[] = array(
                     'soal_pg'        => $row['soal'],
                     'id_soal_cbt'    => $c,
                     'pilihan_a'      => $row['pilihan_a'],
                     'pilihan_b'      => $row['pilihan_b'],
                     'pilihan_c'      => $row['pilihan_c'],
                     'pilihan_d'      => $row['pilihan_d'],
                     'pilihan_e'      => $row['pilihan_e'],
                     'kunci_jawaban'  => $row['kunci_jawaban'],
                     'bobot'          => $row['bobot'],
                     'created_by'     => $user
                 );
                }
            }

            if(!empty($insert_data))
            {
                DB::table('tb_detail_soal_cbt')->insert($insert_data);
            }
        }
        return back()->with('success', 'Berhasil import data excel.');
    }
    //smoy
    public function tampilMapinganCbtPerkelas()
    {
        $filename = Str::random(8);
        $datasoalcbt = DB::table('tb_soal_cbt')->where('status',"active")->get(); 
        $mapelkelas = DB::table('tb_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->select('id_guru_mapel','nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester')
        ->get();

        return view('konten.cbt.cbt_perkelas', compact(['datasoalcbt'],['mapelkelas'],['filename']));
    }

}
