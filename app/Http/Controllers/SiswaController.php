<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;
use Validator;
use Session;

class siswaController extends Controller
{
    //
	public function tampilSiswa()
	{
        //return view('konten.siswa.siswa', compact(['siswa']));
		$templates =  DB::table('tb_format_upload')->where('menu_upload',"data_siswa")->get();
		return view('konten.siswa.siswa', compact(['templates']));
	}

	public function datasiswa()
	{
		$siswa = DB::table('tb_siswa')->where('status',"active")->get();
		$result = array(
			'data' => $siswa 
		);
		$siswa = json_encode($result);
		return $siswa;
	}

	public function tambahSiswa()
	{
		return view('konten.siswa.tambah_siswa');
	}

	public function prosesTambahsiswa(Request $request)
	{
		$messages = [
			'unique'    => 'NIS sudah digunakan Oleh Siswa lain'
		];

		$rule = array(
			'nis'=>'required|max:50|unique:tb_siswa,nis'
		);

		$validator = Validator::make($request->all(), $rule, $messages);

		if($validator->fails()) {
			return redirect()->back()->withInput($request->all())->withErrors($validator);
		}

		$file = $request->file('pasphotosiswa');
		if ($file == null )
		{
			$nama_file = null;
		}
		if ($file != null )
		{
			$nama_file = $request->nis."_".$request->namasiswa.".".$file->getClientOriginalExtension();
			$file->move('assets/images/photo/siswa/',$nama_file);
		}

		$tgllahir = Date('Y-m-d', strtotime($request->tgllahir));
		$tgldikelas = Date('Y-m-d', strtotime($request->tgldikelas));
		DB::table('tb_siswa')->insert([
			'nis'  => $request->nis,
			'nama_siswa'   => $request->namasiswa,
			'jenis_kelamin'   => $request->jk,
			'tempat_lahir'  => $request->tmptlahir,
			'tanggal_lahir'   => $tgllahir,
			'agama'  => $request->agama,
			'email_siswa'  => $request->emailsiswa,
			'no_telp_siswa'  => $request->notelp,
			'pas_photo_siswa'  => $nama_file,
			'status_keluarga'   => $request->statuskel,
			'anak_ke'  => $request->anakke,
			'alamat'   => $request->alamatsiswa,
			'asal_sekolah'  => $request->asalsekolah,
			'dikelas'   => $request->dikelas,
			'tanggal_dikelas'  => $tgldikelas,
			'nama_ayah'   => $request->namaayah,
			'nama_ibu'  => $request->namaibu,
			'alamat_orangtua'   => $request->alamatortu,
			'pekerjaan_ayah'  => $request->pekayah,
			'pekerjaan_ibu'   => $request->pekibu,
		]);

		DB::table('mst_users')->insert([
			'username' => $request->nis,
			'password' => md5($request->nis),
			'nama' => $request->namasiswa,
			'email' => $request->emailsiswa,
			'level' => "SISWA",
			'created_by' => "SYSTEM",
			'status' => "ACTIVE"
		]);
		return redirect('/siswa')->with('success', 'Berhasil Tambah Data Siswa.');
	}

	public function back()
	{
		return redirect('/');
	}

	public function backedit()
	{
		return redirect('/siswa');
	}

	Public function import(Request $request)
	{
		// $this->validate($request, ['select_file'  => 'required|mimes:xls,xlsx']);
		// $path = $request->file('select_file')->getRealPath();
		// $data = Excel::import($path);
		// if($data->count() > 0)
		// {
		// 	foreach($data->toArray() as $key => $value)
		// 	{
		// 		foreach($value as $row)
		// 		{
		// 			if ($row['nis']==!null){
		// 				$insert_data[] = array(
		// 					'nis'  => $row['nis'],
		// 					'nama_siswa'   => $row['nama_siswa'],
		// 					'jenis_kelamin'   => $row['jenis_kelamin'],
		// 					'tempat_lahir'  => $row['tempat_lahir'],
		// 					'tanggal_lahir'   => $row['tanggal_lahir'],
		// 					'agama'  => $row['agama'],
		// 					'status_keluarga'   => $row['status_keluarga'],
		// 					'anak_ke'  => $row['anak_ke'],
		// 					'alamat'   => $row['alamat'],
		// 					'asal_sekolah'  => $row['asal_sekolah'],
		// 					'dikelas'   => $row['dikelas'],
		// 					'tanggal_dikelas'  => $row['tanggal_dikelas'],
		// 					'nama_ayah'   => $row['nama_ayah'],
		// 					'nama_ibu'  => $row['nama_ibu'],
		// 					'alamat_orangtua'   => $row['alamat_orangtua'],
		// 					'pekerjaan_ayah'  => $row['pekerjaan_ayah'],
		// 					'pekerjaan_ibu'   => $row['pekerjaan_ibu'],
		// 				);

		// 				DB::table('mst_users')->insert([
		// 					'username' => $row['nis'],
		// 					'password' => md5($row['nis']),
		// 					'nama' => $row['nama_siswa'],
		// 					'email' => "",
		// 					'level' => "SISWA",
		// 					'created_by' => "SYSTEM",
		// 					'status' => "ACTIVE"
		// 				]);
		// 			}
		// 			else
		// 			{
		// 				var_dump('Ada Field yang null');
		// 				exit();
		// 			}
		// 		}
		// 	}

		// 	if(!empty($insert_data))
		// 	{
		// 		DB::table('tb_siswa')->insert($insert_data);
		// 	}
		// }
		// return back()->with('success', 'Berhasil import data excel.');

		//Cara 2//

		$this->validate($request, [
			'select_file'  => 'required|mimes:xls,xlsx'
		   ]);
	  
		   $path = $request->file('select_file')->getRealPath();
	  
		   $data = Excel::import($path);

		   if($data->count() > 0)
		   {
			foreach($data->toArray() as $key => $value)
			{
			 foreach($value as $row)
			 {
				if ($row['nis']==!null){
					$insert_data[] = array(
						'nis'  => $row['nis'],
						'nama_siswa'   => $row['nama_siswa'],
						'jenis_kelamin'   => $row['jenis_kelamin'],
						'tempat_lahir'  => $row['tempat_lahir'],
						'tanggal_lahir'   => $row['tanggal_lahir'],
						'agama'  => $row['agama'],
						'status_keluarga'   => $row['status_keluarga'],
						'anak_ke'  => $row['anak_ke'],
						'alamat'   => $row['alamat'],
						'asal_sekolah'  => $row['asal_sekolah'],
						'dikelas'   => $row['dikelas'],
						'tanggal_dikelas'  => $row['tanggal_dikelas'],
						'nama_ayah'   => $row['nama_ayah'],
						'nama_ibu'  => $row['nama_ibu'],
						'alamat_orangtua'   => $row['alamat_orangtua'],
						'pekerjaan_ayah'  => $row['pekerjaan_ayah'],
						'pekerjaan_ibu'   => $row['pekerjaan_ibu'],
					);

					DB::table('mst_users')->insert([
						'username' => $row['nis'],
						'password' => md5($row['nis']),
						'nama' => $row['nama_siswa'],
						'email' => "",
						'level' => "SISWA",
						'created_by' => "SYSTEM",
						'status' => "ACTIVE"
					]);
				}
				else
				{
					var_dump('Ada Field yang null');
					exit();
				}
			 }
			}
	  
			if(!empty($insert_data))
			{
			 DB::table('tb_siswa')->insert($insert_data);
			}
		   }
		   return back()->with('success', 'Berhasil import data excel.');
	}

	public function editSiswa($id)
	{ 
		$siswa =  DB::table('tb_siswa')->where('id_siswa',$id)->get();
		return view('konten.siswa.edit_siswa', compact(['siswa']));
	}

	public function editSiswaProses(Request $request)
	{ 
		$user = Session::get('username');
		date_default_timezone_set("Asia/Bangkok");
		$tgllahir = Date('Y-m-d', strtotime($request->tgllahir));
		$tgldikelas = Date('Y-m-d', strtotime($request->tgldikelas));

		$file = $request->file('pasphotosiswa');
		// var_dump($file);
		// exit();
		if ($file == null )
		{
			$nama_file = null;
			DB::table('tb_siswa')
			->where('id_siswa', $request->idsiswa)
			->update([
				'nama_siswa'   => $request->namasiswa,
				'jenis_kelamin'   => $request->jk,
				'tempat_lahir'  => $request->tmptlahir,
				'tanggal_lahir'   => $tgllahir,
				'agama'  => $request->agama,
				'email_siswa'  => $request->emailsiswa,
				'no_telp_siswa'  => $request->notelp,
				'status_keluarga'   => $request->statuskel,
				'anak_ke'  => $request->anakke,
				'alamat'   => $request->alamatsiswa,
				'asal_sekolah'  => $request->asalsekolah,
				'dikelas'   => $request->dikelas,
				'tanggal_dikelas'  => $tgldikelas,
				'nama_ayah'   => $request->namaayah,
				'nama_ibu'  => $request->namaibu,
				'alamat_orangtua'   => $request->alamatortu,
				'pekerjaan_ayah'  => $request->pekayah,
				'pekerjaan_ibu'   => $request->pekibu,
				'updated_by' => $user,
				'updated_timestamp' => date('Y-m-d H:i:s')
			]);
		}
		if ($file != null )
		{
			$nama_file = $request->nis."_".$request->namasiswa.".".$file->getClientOriginalExtension();
			$file->move('assets/images/photo/siswa/',$nama_file);
			DB::table('tb_siswa')
			->where('id_siswa', $request->idsiswa)
			->update([
				'nama_siswa'   => $request->namasiswa,
				'jenis_kelamin'   => $request->jk,
				'tempat_lahir'  => $request->tmptlahir,
				'tanggal_lahir'   => $tgllahir,
				'agama'  => $request->agama,
				'email_siswa'  => $request->emailsiswa,
				'no_telp_siswa'  => $request->notelp,
				'pas_photo_siswa'  => $nama_file,
				'status_keluarga'   => $request->statuskel,
				'anak_ke'  => $request->anakke,
				'alamat'   => $request->alamatsiswa,
				'asal_sekolah'  => $request->asalsekolah,
				'dikelas'   => $request->dikelas,
				'tanggal_dikelas'  => $tgldikelas,
				'nama_ayah'   => $request->namaayah,
				'nama_ibu'  => $request->namaibu,
				'alamat_orangtua'   => $request->alamatortu,
				'pekerjaan_ayah'  => $request->pekayah,
				'pekerjaan_ibu'   => $request->pekibu,
				'updated_by' => $user,
				'updated_timestamp' => date('Y-m-d H:i:s')
			]);
		}

		return redirect('/siswa')->with('success', 'Berhasil Update Data Siswa.');
	}

	public function nonactiveSiswa(Request $request)
	{ 
		$user = Session::get('username');
		date_default_timezone_set("Asia/Bangkok");

		DB::table('tb_siswa')
		->where('id_siswa', $request->idsiswa)
		->update([
			'status'   => 'nonactive',
			'updated_by' => $user,
			'updated_timestamp' => date('Y-m-d H:i:s')
		]);

		return redirect('/siswa')->with('success', 'Berhasil Update Data Siswa Menjadi Nonactive.');
	}

	public function lulusSiswa(Request $request)
	{ 
		$user = Session::get('username');
		date_default_timezone_set("Asia/Bangkok");

		DB::table('tb_siswa')
		->where('id_siswa', $request->idsiswa)
		->update([
			'status'   => 'lulus',
			'updated_by' => $user,
			'updated_timestamp' => date('Y-m-d H:i:s')
		]);

		return redirect('/siswa')->with('success', 'Berhasil Update Data Siswa Menjadi Lulus.');
	}
}
