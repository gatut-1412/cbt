<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class eskulController extends Controller
{
    //START
    //------------------------------------- DATA MASTER ESKUL --------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//

    public function tampileskul()
    {       
        $guru = DB::table('tb_guru')->where('status',"active")->get();
        return view('konten.eskul.eskul', compact(['guru']));
    }
    public function dataeskul()
    {
        $eskul = DB::table('tb_eskul')
        ->leftjoin('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_eskul.status',"ACTIVE")
        ->select('tb_eskul.*', 'tb_guru.nama_guru')
        ->get();
        $result = array(
            'data' => $eskul 
        );
        $eskul = json_encode($result);
        return $eskul;
    }

    public function tambahEskul(Request $request)
    {
        $user = Session::get('username');
        DB::table('tb_eskul')->insert([
            'nama_eskul' => $request->input('namaeskul') ,
            'id_guru' => $request->idguru,
            'nip' => $request->nip,
            'created_by' => $user,
            'status' => "ACTIVE"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function fetchdata($id)
    {
        $eskul = DB::table('tb_eskul')
        ->join('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_eskul.id_eskul',$id)
        ->select('tb_eskul.*', 'tb_guru.nama_guru', 'tb_guru.nip')
        ->get();

        return response()->json([
          'data' => $eskul
      ]);
    }

    public function updateEskul(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        DB::table('tb_eskul')
        ->where('id_eskul', $request->ideskul)
        ->update([
            'nama_eskul' => $request->namaeskul ,
            'id_guru' => $request->idguru,
            'nip' => $request->nip,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteEskul(Request $request)
    {
        try{
            DB::table('tb_eskul')
            ->where('id_eskul',$request->ideskul)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu mappingan data.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
    }
    //END
    //------------------------------------- DATA MASTER ESKUL --------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//

    //START
    //------------------------------------- MAPPING DATA ESKUL -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function tampilMappingEskul()
    {       
        $siswa = DB::table('tb_siswa')
        ->join('tb_kelas_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->select('nis','nama_siswa','tb_siswa.id_siswa','id_kelas_siswa')
        ->where('status',"active")
        ->get();
        $eskul = DB::table('tb_eskul')->where('status',"active")->get();
        return view('konten.eskul.mappingeskul', compact(['siswa'],['eskul']));
    }
    public function dataMappingEskul()
    {
        $eskul = DB::table('tb_mapping_eskul')
        ->join('tb_kelas_siswa', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_mapping_eskul.id_kelas_siswa')
        ->join('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->join('tb_eskul', 'tb_eskul.id_eskul', '=', 'tb_mapping_eskul.id_eskul')
        ->leftjoin('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_mapping_eskul.status',"ACTIVE")
        ->select('nis','nama_siswa','nama_eskul','tb_guru.nama_guru','id_mapping_eskul','keterangan')
        ->get();
        $result = array(
            'data' => $eskul 
        );
        $eskul = json_encode($result);
        return $eskul;
    }
    public function tambahMappingEskul(Request $request)
    {
        $user = Session::get('username');
        DB::table('tb_mapping_eskul')->insert([
            'id_eskul' => $request->input('ideskul') ,
            'id_kelas_siswa' => $request->idsiswa,
            'keterangan' => $request->keterangan,
            'created_by' => $user,
            'status' => "ACTIVE"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }
    public function fetchdata2($id)
    {
        $eskul = DB::table('tb_mapping_eskul')
        ->join('tb_kelas_siswa', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_mapping_eskul.id_kelas_siswa')
        ->join('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->join('tb_eskul', 'tb_eskul.id_eskul', '=', 'tb_mapping_eskul.id_eskul')
        ->leftjoin('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_mapping_eskul.id_mapping_eskul',$id)
        ->select('nis','nama_siswa','nama_eskul','tb_guru.nama_guru','id_mapping_eskul','keterangan','tb_mapping_eskul.id_eskul','tb_mapping_eskul.id_kelas_siswa')
        ->get();

        return response()->json([
          'data' => $eskul
      ]);
    }

    public function updateMappingEskul(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        DB::table('tb_mapping_eskul')
        ->where('id_mapping_eskul', $request->idmappingeskul)
        ->update([
            'id_eskul' => $request->ideskul ,
            'id_kelas_siswa' => $request->idsiswa,
            'keterangan' => $request->keterangan,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteMappingEskul(Request $request)
    {
        try{
            DB::table('tb_mapping_eskul')
            ->where('id_mapping_eskul',$request->idmappingeskul)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu mappingan data.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
    }

    public function tampilMappingEskulPerkelas()
    {       
        return view('konten.eskul.mappingeskul_perkelas');
    }

    public function dataMappingEskulPerkelas()
    {
        $user = Session::get('username');   
        $level = Session::get('level');
        $walkes = Session::get('walikelas');
        $bk = Session::get('is_bk');
        if($level== "GURU" && $walkes=="Y" && $bk=="N" )
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->where('tb_guru.nip',$user)
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
        else
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
    }   

    public function tampilMappingEskulPerkelasSiswa($id)
    {       
        $mapeskul = DB::table('tb_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
        ->get();
         $this->dataMappingEskulPerkelasSiswa($id);
        return view('konten.eskul.mappingeskul_perkelas_siswa',compact('mapeskul'));
    }

    public function dataMappingEskulPerkelasSiswa($id)
    {
        $data = DB::table('tb_mapping_eskul')
        ->rightjoin('tb_kelas_siswa', 'tb_kelas_siswa.id_kelas_siswa', '=', 'tb_mapping_eskul.id_kelas_siswa')
        ->join('tb_master_kelas', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
        ->join('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
        ->select('tb_siswa.id_siswa', 'nama_siswa', 'nis', 'jenis_kelamin','tb_kelas_siswa.id_kelas_siswa',
            DB::raw('count(tb_mapping_eskul.id_kelas_siswa) as jumlah'))
        ->groupBy('nis')
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->where('tb_siswa.status',"active")->get();
        $result = array(
            'data' => $data 
        );
        $data = json_encode($result);
        return $data;
    }

    public function historyEskul($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester')
        ->get();

        $mappeskul = DB::table('tb_mapping_eskul')
        ->join('tb_eskul', 'tb_eskul.id_eskul', '=', 'tb_mapping_eskul.id_eskul')
        ->leftjoin('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('id_kelas_siswa',$id)
        ->select('nama_eskul','nama_guru','keterangan')
        ->get();
        return view('konten.eskul.history_eskul',compact(['datasiswa'],('mappeskul')));
    }

    //END
    //------------------------------------- MAPPING DATA ESKUL -------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//

    //START
    //----------------------------------------- SISWA ESKUL ----------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function siswaEskul()
    {
        return view('konten.eskul.siswa_eskul');
    }
    public function dataSiswaEskul()
    {
        $user = Session::get('username');   
        $eskul = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_siswa.nis',$user)
        ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas','tb_kelas_siswa.id_kelas_siswa')
        ->groupBy('tahun_ajaran','nama_kelas')
        ->get();
        $result = array(
          'data' => $eskul 
      );
        $eskul = json_encode($result);
        return $eskul;
    }
    public function siswaEskulDetail($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester','nama_guru')
        ->get();

        $mappeskul = DB::table('tb_mapping_eskul')
        ->join('tb_eskul', 'tb_eskul.id_eskul', '=', 'tb_mapping_eskul.id_eskul')
        ->leftjoin('tb_guru', 'tb_eskul.id_guru', '=', 'tb_guru.id_guru')
        ->where('id_kelas_siswa',$id)
        ->select('nama_eskul','nama_guru','keterangan')
        ->get();
        return view('konten.eskul.siswa_history_eskul',compact(['datasiswa'],('mappeskul')));
    }
    //END
    //----------------------------------------- SISWA ESKUL ----------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
}
