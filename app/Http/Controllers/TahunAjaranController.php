<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class tahunAjaranController extends Controller
{
    //
    public function tampilTahunAjaran()
    {
        $tahunajaran = null;
        return view('konten.tahun_ajaran.tahun_ajaran', compact(['tahunajaran']));
    }
    public function datatahunajaran()
    {
        $tahunajaran = DB::table('tb_tahun_ajaran')->where('status',"active")->get();
        $result = array(
        'data' => $tahunajaran 
        );
        $tahunajaran = json_encode($result);
        return $tahunajaran;
    }
    public function tambahTahunAjaran(Request $request)
    {
            DB::table('tb_tahun_ajaran')->insert([
            'tahun_ajaran' => $request->tahunajaran,
            'created_by' => "admin",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function fetchdata($id)
    {
        $tahunajaran = DB::table('tb_tahun_ajaran')
        ->where('id_tahun_ajaran',$id)
        ->get();

        return response()->json([
          'data' => $tahunajaran
      ]);
    }

    public function updateTahunAjaran(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        DB::table('tb_tahun_ajaran')
        ->where('id_tahun_ajaran', $request->idtahunajaran)
        ->update([
            'tahun_ajaran' => $request->tahunajaran,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deleteTahunAjaran(Request $request)
    {
        try{
            DB::table('tb_tahun_ajaran')
            ->where('id_tahun_ajaran',$request->idtahunajaran)->delete();
        }catch(\Exception $e){
           return back()->with('error', 'Data gagal dihapus, Silahkan hilangkan dulu pada mappingan.');
       }
       return back()->with('success', 'Data berhasil Dihapus.');
   }
}
