<?php

 
namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Session;

 

class UtilsController extends Controller

{

    public function scheduler(){

       date_default_timezone_set("Asia/Bangkok");

       $datenow = date('Y-m-d H:i:s');

       DB::table('tb_tugas_online')

       ->where('status_tugas','DRAFT')

       ->where('start_date','<=',$datenow)

       ->update([

        'status_tugas' => 'PUBLISH'

    ]);
    
     DB::table('tb_tugas_online')

       ->where('status_tugas','PUBLISH')

       ->where('end_date','<=',$datenow)

       ->update([

        'status_tugas' => 'EXPIRED'

    ]);
    
     DB::table('tb_mapping_cbt')

       ->where('status_mapping','DRAFT')

       ->where('tanggal','<=',$datenow)

       ->update([

        'status_mapping' => 'PUBLISH'

    ]);

 

   }
   
   

 

}