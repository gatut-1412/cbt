<?php

namespace App\Http\Controllers;
use App\Http\ModelLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
	public function login()
	{
		return view('login');
	}

	public function loginPost(Request $request) 
	{
		$username = $request->username;
		$password = $request->password;     
    	// $data = ModelUser::where('email',$email)->first();
		$data = ModelLogin::where('username',$username)->where('status','ACTIVE')->first();
		if($data) 
		{ 
			$hashed = md5($password);             
			if ($data->password == $hashed) 
			{
				Session::put('username',$data->username); 
				Session::put('nama',$data->nama);
				Session::put('level',$data->level);                
				Session::put('login',TRUE);

	          // if($data->level == "ADMIN" || $data->level == "SUPER ADMIN"){
	          //   return redirect('/')->with('data', $data);
	          // }else if($data->level == "SISWA"){
	          //   return redirect('/dashboardSiswa')->with('data', $data);
	          // }else if($data->level == "GURU"){
	          //   return redirect('/dashboardGuru')->with('data', $data);
	          // }

				if ($data->level == 'GURU') {
					$data2 = DB::table('tb_guru')->where('nip',$username)->where('status','active')->first();

					$data3 = DB::table('tb_master_kelas')
					->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
					->where('nip',$username)
					->where('tb_master_kelas.status','active')
					->get();
					if ($data3 -> isEmpty())
					{
						Session::put('walikelas','N');
					}
					else
					{
						Session::put('walikelas','Y');
					}
					Session::put('is_bk',$data2->is_bk);
				}
				else if ($data->level == 'ADMIN')
				{
					Session::put('is_bk','Y');
				}
				else
				{
					Session::put('is_bk','N');
				}

				return redirect('/')->with('data', $data);
			}
			else
			{
				return redirect('login')->with('alert','Password atau Username, Salah !');
			}
		}
		else
		{
			return redirect('login')->with('alert',' Username tidak terdaftar / Inactive!');
		}
	}

	public function logout(){
		Session::flush();
		return redirect('login')->with('alert','Anda sudah logout');
	}

	public function gantiPassword(){
		return view('konten.gantipass');
	}


	public function updatePassword(Request $request)
	{

  	// update password
		DB::table('mst_users')->where('USERNAME',$request->username)->update([
			'password' => md5($request->passwordba2)
		]);
        // alihkan halaman ke halaman user
		Session::flush();
		return redirect('login')->with('alert','Password berhasil diganti. Silahkan login kembali');
	}


 	//smoy
	public function profilGuru()
	{
		return view('konten.guru.view_data_guru');
	}
	public function profilSiswa()
	{
		$user = Session::get('username');
		$level = Session::get('level');
		$siswa = DB::table('tb_siswa')->where('nis',$user)->get();
		return view('konten.siswa.view_data_siswa',compact(['siswa']));
	}


}
