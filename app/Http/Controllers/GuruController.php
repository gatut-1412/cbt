<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use Session;
use Validator;

class guruController extends Controller
{
    //smoy
    public function tampilGuru()
    {
        $guru = null;
        $template =  DB::table('tb_format_upload')->where('menu_upload',"data_guru")->get();
        return view('konten.guru.guru', compact(['guru'],['template']));
    }
    public function dataGuru()
    {
        $guru = DB::table('tb_guru')->where('status',"active")->get();
        $result = array(
            'data' => $guru 
        );
        $guru = json_encode($result);
        return $guru;
    }
    public function tambahGuru()
    {
        return view('konten.guru.tambah_guru');
    }

    public function prosesTambahGuru(Request $request)
    {
        // $this->validate($request, [
        //     'nip'=>'required|max:50|unique:tb_guru,nip',
        // ]);
        $messages = [
            'unique'    => 'NIP sudah digunakan Oleh Guru lain'
        ];

        $rule = array(
            'nip'=>'required|max:50|unique:tb_guru,nip'
        );

        $validator = Validator::make($request->all(), $rule, $messages);

        if($validator->fails()) {
             return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $file = $request->file('pasphoto');
        if ($file == null )
        {
            $nama_file = null;
        }
        if ($file != null )
        {
            $nama_file = $request->nip."_".$request->namaguru.".".$file->getClientOriginalExtension();
            $file->move('assets/images/photo/guru/',$nama_file);
        }
        // var_dump($nama_file, $file);
        // exit();
        $user = Session::get('username');
        $a = $request->isbk;
        If ($a == NULL) {
            $a ='N';
        }

        $tgllahir = Date('Y-m-d', strtotime($request->tgllahir));
        $tglsurat = Date('Y-m-d', strtotime($request->tglsurat));
        DB::table('tb_guru')->insert([
            'nip' => $request->nip,
            'nama_guru' => $request->namaguru,
            'jenis_kelamin' => $request->jk,
            'is_bk' => $a,
            'tempat_lahir' => $request->tempatlahir,
            'tanggal_lahir' => $tgllahir,
            'nik' => $request->nik,
            'pas_photo_guru' => $nama_file,
            'notelp' => $request->notelp,
            'email_guru' => $request->emailguru,
            'agama' => $request->agama,
            'domisili' => $request->domisili,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->kecamatan,
            'alamat' => $request->alamat,
            'desakelurahan' => $request->deskel,
            'status_nikah' => $request->statusnikah,
            'nama_ibu' => $request->namaibu,
            'nama_istrisuami' => $request->namaissu,
            'status_kepegawaian' => $request->statuskepeg,
            'jenis_ptk' => $request->jenisptk,
            'lembaga_pengangkatan' => $request->lembagapeng,
            'no_sk' => $request->nosk,
            'tgl_surat' => $tglsurat,
            'nuptk' => $request->nuptk,
            'tmt_tugas' => $request->tmt,
            'tugas_tambahan' => $request->tugastambahan,
            'mengajar' => $request->mengajar,
            'created_by' => $user,
            'status' => "active"
        ]);

        DB::table('mst_users')->insert([
            'username' => $request->nip,
            'password' => md5($request->nip),
            'nama' => $request->namaguru,
            'email' => $request->emailguru,
            'level' => "GURU",
            'created_by' => "SYSTEM",
            'status' => "ACTIVE"
        ]);
        return redirect('/guru')->with('success', 'Berhasil tambah data.');
    }

    Public function import(Request $request)
    {
        $user = Session::get('username');
        $this->validate($request, ['select_file'  => 'required|mimes:xls,xlsx']);
        $path = $request->file('select_file')->getRealPath();
        $data = Excel::load($path)->get();
        if($data->count() > 0)
        {
            foreach($data->toArray() as $key => $value)
            {
                foreach($value as $row)
                {
                    $insert_data[] = array(
                     'nip'  => $row['nip'],
                     'nama_guru'   => $row['nama_guru'],
                     'jenis_kelamin'   => $row['jenis_kelamin'],
                     'is_bk'   => $row['guru_bk'],
                     'tempat_lahir' => $row['tempat_lahir'],
                     'tanggal_lahir' => $row['tanggal_lahir'],
                     'nik' => $row['nik'],
                     'notelp' => $row['no_telp'],
                     'email_guru' => $row['email'],
                     'agama' => $row['agama'],
                     'domisili' => $row['domisili'],
                     'provinsi' => $row['provinsi'],
                     'kabupaten' => $row['kabupaten'],
                     'kecamatan' => $row['kecamatan'],
                     'alamat' => $row['alamat'],
                     'desakelurahan' => $row['desa_kelurahan'],
                     'status_nikah' => $row['status_nikah'],
                     'nama_ibu' => $row['nama_ibu'],
                     'nama_istrisuami' => $row['nama_istrisuami'],
                     'status_kepegawaian' => $row['status_kepegawaian'],
                     'jenis_ptk' => $row['jenis_ptk'],
                     'lembaga_pengangkatan' => $row['lembaga_pengangkatan'],
                     'no_sk' => $row['no_sk'],
                     'tgl_surat' => $row['tgl_surat'],
                     'nuptk' => $row['nuptk'],
                     'tmt_tugas' => $row['tmt_tugas'],
                     'tugas_tambahan' => $row['tugas_tambahan'],
                     'mengajar' => $row['mengajar'],
                     'created_by' => $user,
                     'status' => "active"

                 );

                    DB::table('mst_users')->insert([
                        'username' => $row['nip'],
                        'password' => md5($row['nip']),
                        'nama' => $row['nama_guru'],
                        'email' => $row['email'],
                        'level' => "GURU",
                        'created_by' => "SYSTEM",
                        'status' => "ACTIVE"
                    ]);
                }
            }

            if(!empty($insert_data))
            {
                DB::table('tb_guru')->insert($insert_data);
            }
        }
        return back()->with('success', 'Berhasil import data excel.');
    }

    public function editGuru($id)
    { 
        $guru =  DB::table('tb_guru')->where('id_guru',$id)->get();
        return view('konten.guru.edit_guru', compact(['guru']));
    }

    public function editGuruProses(Request $request)
    { 
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");
        $a = $request->isbk;
        If ($a == NULL) {
            $a ='N';
        }

        $tgllahir = Date('Y-m-d', strtotime($request->tgllahir));
        $tglsurat = Date('Y-m-d', strtotime($request->tglsurat));

        $file = $request->file('pasphoto');
        // var_dump($file);
        // exit();
        if ($file == null )
        {
            $nama_file = null;
            DB::table('tb_guru')
            ->where('id_guru', $request->idguru)
            ->update([
                'nama_guru' => $request->namaguru,
                'jenis_kelamin' => $request->jk,
                'is_bk' => $a,
                'tempat_lahir' => $request->tempatlahir,
                'tanggal_lahir' => $tgllahir,
                'nik' => $request->nik,
                'notelp' => $request->notelp,
                'email_guru' => $request->emailguru,
                'agama' => $request->agama,
                'domisili' => $request->domisili,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'alamat' => $request->alamat,
                'desakelurahan' => $request->deskel,
                'status_nikah' => $request->statusnikah,
                'nama_ibu' => $request->namaibu,
                'nama_istrisuami' => $request->namaissu,
                'status_kepegawaian' => $request->statuskepeg,
                'jenis_ptk' => $request->jenisptk,
                'lembaga_pengangkatan' => $request->lembagapeng,
                'no_sk' => $request->nosk,
                'tgl_surat' => $tglsurat,
                'nuptk' => $request->nuptk,
                'tmt_tugas' => $request->tmt,
                'tugas_tambahan' => $request->tugastambahan,
                'mengajar' => $request->mengajar,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }
        if ($file != null )
        {
            $nama_file = $request->nip."_".$request->namaguru.".".$file->getClientOriginalExtension();
            $file->move('assets/images/photo/guru/',$nama_file);
            DB::table('tb_guru')
            ->where('id_guru', $request->idguru)
            ->update([
                'nama_guru' => $request->namaguru,
                'jenis_kelamin' => $request->jk,
                'is_bk' => $a,
                'tempat_lahir' => $request->tempatlahir,
                'tanggal_lahir' => $tgllahir,
                'nik' => $request->nik,
                'pas_photo_guru' => $nama_file,
                'notelp' => $request->notelp,
                'email_guru' => $request->emailguru,
                'agama' => $request->agama,
                'domisili' => $request->domisili,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'alamat' => $request->alamat,
                'desakelurahan' => $request->deskel,
                'status_nikah' => $request->statusnikah,
                'nama_ibu' => $request->namaibu,
                'nama_istrisuami' => $request->namaissu,
                'status_kepegawaian' => $request->statuskepeg,
                'jenis_ptk' => $request->jenisptk,
                'lembaga_pengangkatan' => $request->lembagapeng,
                'no_sk' => $request->nosk,
                'tgl_surat' => $tglsurat,
                'nuptk' => $request->nuptk,
                'tmt_tugas' => $request->tmt,
                'tugas_tambahan' => $request->tugastambahan,
                'mengajar' => $request->mengajar,
                'updated_by' => $user,
                'updated_timestamp' => date('Y-m-d H:i:s')
            ]);
        }

        return redirect('/guru')->with('success', 'Berhasil Update Data Guru.');
    }
    public function backedit()
    {
        return redirect('/guru');
    }

    public function nonactiveGuru(Request $request)
    { 
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");

        DB::table('tb_guru')
        ->where('id_guru', $request->idguru)
        ->update([
            'status'   => 'nonactive',
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return redirect('/guru')->with('success', 'Berhasil Update Data Guru Menjadi Nonactive.');
    }
}