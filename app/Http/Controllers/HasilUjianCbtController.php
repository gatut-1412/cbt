<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class hasilUjianCbtController extends Controller
{
    //
    public function tampilHasilUjianCbt()
    {

        return view('konten.hasil_ujian_cbt.hasil_ujian_cbt');

    }
    public function dataHasilUjian()
    {
        $user = Session::get('username');
        $level = Session::get('level');

        if ($level == 'GURU'){
            $mappcbt = DB::table('tb_mapping_cbt')
            ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
            ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
            ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
            ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
            ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
            ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
            ->where('tb_guru.nip',$user)
            ->whereIn('tb_mapping_cbt.status_mapping', array('PUBLISH','SELESAI'))
            ->orderBy('tb_mapping_cbt.tanggal','DESC')
        // ->groupby('')
        //->select('nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester')
            ->get();
            $result = array(
                'data' => $mappcbt 
            );
            $mappcbt = json_encode($result);
            return $mappcbt;
        }
        else{
            $mappcbt = DB::table('tb_mapping_cbt')
            ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
            ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
            ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
            ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
            ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
            ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
            ->whereIn('tb_mapping_cbt.status_mapping', array('PUBLISH','SELESAI'))
            ->orderBy('tb_mapping_cbt.tanggal','DESC')
            ->get();
            $result = array(
                'data' => $mappcbt 
            );
            $mappcbt = json_encode($result);
            return $mappcbt;
        }
    }

    public function detailHasilUjian($id)
    {
        $user = Session::get('username');
        $cbt = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
        ->join('tb_detail_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_detail_soal_cbt.id_soal_cbt')
        ->leftjoin('tr_ikut_ujian','tr_ikut_ujian.id_tes','=','tb_mapping_cbt.id_mapping_cbt')
        ->where('tb_mapping_cbt.id_mapping_cbt',$id)
        ->select('nama_guru','nama_kelas','nama_pelajaran','judul_soal','semester','jenis_soal','waktu','tb_soal_cbt.id_soal_cbt', 'tb_mapping_cbt.id_mapping_cbt', 'tahun_ajaran', DB::raw('max(nilai) as nilaimax'), DB::raw('min(nilai) as nilaimin'), DB::raw('round(avg(nilai),0) as nilairatarata'))
        ->groupBy('tb_mapping_cbt.id_mapping_cbt')
        ->get();

        foreach($cbt as $data)
        {

            $idsoal = $data->id_soal_cbt;
            $cbt2 = DB::table('tb_detail_soal_cbt')
            ->select('id_detail_soal_cbt','id_soal_cbt',DB::raw('count(tb_detail_soal_cbt.id_detail_soal_cbt) as jumlah'))
            ->where('id_soal_cbt',$idsoal)
            ->groupby('id_soal_cbt')
            ->get();
        }
        $this->dataDetailHasilUjianCbt($id);
        return view('konten.hasil_ujian_cbt.detail_hasil_ujian_cbt',compact(['cbt'],['cbt2']));
    }

    public function dataDetailHasilUjianCbt($id)
    {
        $user = Session::get('username');
        $level = Session::get('level');

        $mappcbt = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_mapping_cbt.id_guru_mapel', '=', 'tb_guru_mapel.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_kelas_siswa', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
        ->join('tb_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')  
        ->join('tb_soal_cbt','tb_soal_cbt.id_soal_cbt','=','tb_mapping_cbt.id_soal_cbt')
        ->join('tr_ikut_ujian as a','a.id_tes','=','tb_mapping_cbt.id_mapping_cbt')
        ->join('tr_ikut_ujian','tr_ikut_ujian.id_user','=','tb_siswa.id_siswa')
        ->where('tb_mapping_cbt.id_mapping_cbt',$id)
        ->where('tr_ikut_ujian.id_tes',$id)
        ->select('nama_siswa','nis','tr_ikut_ujian.nilai','tr_ikut_ujian.jml_benar','tr_ikut_ujian.nilai_bobot', 'tr_ikut_ujian.id as id_ujian','tr_ikut_ujian.tgl_mulai','tr_ikut_ujian.tgl_selesai',DB::raw("TIMEDIFF(tr_ikut_ujian.tgl_selesai,tr_ikut_ujian.tgl_mulai) AS waktu_mengerjakan"))
        ->groupby('nama_siswa')
        ->get();
        $result = array(
            'data' => $mappcbt 
        );
        $mappcbt = json_encode($result);
        return $mappcbt;
    }
}
