<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Session;
use DateTime;
use DatePeriod;
use DateInterval;
use Excel;


class mappingCbtController extends Controller
{
    public function tampilMapinganCbtPerkelas()
    {
        $user = Session::get('username');
        $level = Session::get('level');
        // var_dump($level);
        // exit();

        $filename = Str::random(8);
        if ($level == "ADMIN") {
            $datasoalcbt = DB::table('tb_soal_cbt')->where('status',"active")->get(); 
        }else{
              $datasoalcbt = DB::table('tb_soal_cbt')
              ->join('tb_guru', 'tb_guru.nip', '=', 'tb_soal_cbt.created_by')
              ->join('tb_guru_mapel', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
               ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
              ->where('tb_soal_cbt.created_by',$user)->where('tb_soal_cbt.status',"active")->groupby('tb_soal_cbt.id_soal_cbt')->get(); 
        }
        // $datasoalcbt = DB::table('tb_soal_cbt')->where('status',"active")->get(); 
        $mapelkelas = DB::table('tb_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_tahun_ajaran','tb_tahun_ajaran.id_tahun_ajaran','=','tb_master_kelas.id_tahun_ajaran')
        ->select('id_guru_mapel','nama_guru','nama_kelas','nama_pelajaran','tahun_ajaran','semester')
         ->where('tb_guru.nip',$user)
        ->get();
  
        // var_dump($mapelkelas);
        // exit();

        return view('konten.cbt.cbt_perkelas', compact(['datasoalcbt'],['mapelkelas'],['filename']));
    }

    public function dataMappingCbt()
    {
        $user = Session::get('username');
          $level = Session::get('level');
           if ($level == "ADMIN") {
              $datasoalcbt = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_guru_mapel.id_guru_mapel', '=', 'tb_mapping_cbt.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_mapping_cbt.id_soal_cbt')
        // ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->select('tb_mapping_cbt.id_mapping_cbt','nama_kelas','nama_pelajaran','judul_soal','jenis_soal','tanggal','status_mapping')
        ->where('tb_mapping_cbt.status',"active")
        ->orderby('tb_mapping_cbt.tanggal','DESC')
        ->get(); 

           }else{
        $datasoalcbt = DB::table('tb_mapping_cbt')
        ->join('tb_guru_mapel', 'tb_guru_mapel.id_guru_mapel', '=', 'tb_mapping_cbt.id_guru_mapel')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_guru_mapel.id_master_kelas')
        ->join('tb_pelajaran', 'tb_pelajaran.id_pelajaran', '=', 'tb_guru_mapel.id_pelajaran')
        ->join('tb_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_mapping_cbt.id_soal_cbt')
        // ->join('tb_guru', 'tb_guru_mapel.id_guru', '=', 'tb_guru.id_guru')
        ->select('tb_mapping_cbt.id_mapping_cbt','nama_kelas','nama_pelajaran','judul_soal','jenis_soal','tanggal','status_mapping')
        ->where('tb_mapping_cbt.status',"active")
        ->where('tb_mapping_cbt.created_by',$user)
        ->orderby('tb_mapping_cbt.tanggal','DESC')
        ->get(); 
    }

        $result = array(
            'data' => $datasoalcbt 
        );
        $datasoalcbt = json_encode($result);
        return $datasoalcbt;
    }

    public function tambahMappingCbt(Request $request)
    {

        $dataTanggal = Date('Y-m-d', strtotime($request->tanggal));
        $user = Session::get('username');
        $que = DB::table('tb_mapping_cbt')->insert([
            'id_guru_mapel' => $request->input('kelascbt1'),
            'id_soal_cbt' => $request->soalcbt1,
            'tanggal' => $request->tglcbt,
            'status_mapping' => 'DRAFT',
            'catatan' => $request->catatan,
            'created_by' => $user,
            'status' => "ACTIVE"
        ]);        

        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function updateKePublish($id)
    {

        $id_soal_cbt =  DB::table('tb_mapping_cbt')
         ->join('tb_soal_cbt', 'tb_soal_cbt.id_soal_cbt', '=', 'tb_mapping_cbt.id_soal_cbt')
         ->where('id_mapping_cbt',$id)->get();
         $result = array(
            'data' => $id_soal_cbt 
        );
        $id_soal_cbt = json_encode($result,true);
        $data2 = json_decode($id_soal_cbt,true);
   
        $minutes_to_add = $data2['data'][0]['waktu'];


        date_default_timezone_set("Asia/Jakarta");
        
        $date = date("Y-m-d H:i:s");
        $date = new DateTime($date);
        $date->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        $stamp = $date->format('Y-m-d H:i:s');
        // echo $stamp;
  //    exit();
        DB::table('tb_mapping_cbt')->where('id_mapping_cbt',$id)->update([
            'status_mapping' => "PUBLISH",
            // 'updated_by' => $updatedby,
             'updated_timestamp' => $stamp
        ]);
        return back()->with('success', 'Berhasil Update Data.');
    }

    public function updateKeSelesai($id)
    {

        DB::table('tb_mapping_cbt')->where('id_mapping_cbt',$id)->update([
            'status_mapping' => "SELESAI",
            // 'updated_by' => $updatedby,
            // 'updated_timestamp' => $datetime
        ]);
        return back()->with('success', 'Berhasil Update Data.');
    }

 public function hapusMappingCbt($id)
    {
    
   DB::table('tb_mapping_cbt')
     ->where('id_mapping_cbt',$id)->delete();
    return back()->with('success', 'Berhasil Update Data.');
}

}
