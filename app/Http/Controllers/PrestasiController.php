<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;


class PrestasiController extends Controller
{
    //
    public function tampilPrestasi()
    {  
        $siswa = DB::table('tb_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->where('tb_master_kelas.status',"active")
        ->select('tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas','jenis_kelamin')
        ->get();
        return view('konten.prestasi.prestasi', compact(['siswa']));
    }

    public function dataPrestasi()
    {
        $dataPrestasi = DB::table('tb_prestasi')
        ->join('tb_kelas_siswa', 'tb_prestasi.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        // ->select('tb_kelas_siswa.id_kelas_siswa','tb_siswa.id_siswa','nis','nama_siswa','nama_kelas','jenis_kelamin','catatan', DB::raw('DATE_FORMAT(tanggal, "%d %M, %Y") as tanggal'))
        ->where('tb_prestasi.status',"active")
        ->get();

        $result = array(
            'data' => $dataPrestasi 
        );
        $dataPrestasi = json_encode($result);
        return $dataPrestasi;
    }
    
    public function lihatPrestasiPerkelas()
    {
        return view('konten.prestasi.prestasi_perkelas');
    }

    public function dataPrestasiPerKelas()
    {
        $user = Session::get('username');   
        $level = Session::get('level');
        $walkes = Session::get('walikelas');
        $bk = Session::get('is_bk');
        if($level== "GURU" && $walkes=="Y" && $bk=="N" )
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->where('tb_guru.nip',$user)
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
        else
        {
            $kelassiswa = DB::table('tb_kelas_siswa')
            ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
            ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
            ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
            ->where('tb_master_kelas.status',"active")
            ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
            ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
            ->get();
            $result = array(
              'data' => $kelassiswa 
          );
            $kelassiswa = json_encode($result);
            return $kelassiswa;
        }
    }


    public function tambahPrestasi(Request $request)
    {
        $user = Session::get('username');
        $siswainput = $request->input('siswa');

        $prestasi = DB::table('tb_prestasi')
        ->select('id_kelas_siswa',DB::raw('count(id_kelas_siswa)+1 as jumlah'))
        ->groupBy('id_kelas_siswa')
        ->where('status',"ACTIVE")
        ->where('id_kelas_siswa',$siswainput)
        ->get();

        if ($prestasi ->isEmpty())
        {
             DB::table('tb_prestasi')->insert([
                    'id_kelas_siswa' => $request->input('siswa'),
                    'prestasike' => 'P1',
                    'keg1' => $request->kegiatan,
                    'ketkeg1' => $request->ket_kegiatan,
                    'created_by' => $user,
                    'status' => "ACTIVE"
                ]);

            }
            else
            {
               foreach($prestasi as $data)
               {
                $jml = $data->jumlah;
                $siswa = $data->id_kelas_siswa;
                DB::table('tb_prestasi')->insert
                ([
                    'id_kelas_siswa' => $request->input('siswa'),
                    'prestasike' => 'P'.$jml,
                    'keg1' => $request->kegiatan,
                    'ketkeg1' => $request->ket_kegiatan,
                    'created_by' => $user,
                    'status' => "ACTIVE"
                ]);
            }
        }


        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function prestasiKelasPersiswa($id)
    {
        $user = Session::get('username');
        $kelas = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_guru.id_guru', '=', 'tb_master_kelas.id_guru')
        // ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->groupBy('tb_master_kelas.id_master_kelas')
        ->get();

        $this->dataPrestasiPerKelasSiswa($id);
        return view('konten.prestasi.prestasi_kelas_persiswa', compact(['kelas']));
    }

    public function dataPrestasiPerKelasSiswa($id)
    {
        $data = DB::table('tb_kelas_siswa')
        ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_prestasi', 'tb_prestasi.id_kelas_siswa', '=', 'tb_kelas_siswa.id_kelas_siswa')
        // ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        // ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        // ->where('tb_master_kelas.status',"active")
        ->select('nama_siswa','jenis_kelamin','nama_kelas','nis', 'tb_prestasi.id_kelas_siswa',
            DB::raw('GROUP_CONCAT( if(prestasike="P1",keg1,NULL) ) AS prestasi1'),
            DB::raw('GROUP_CONCAT( if(prestasike="P1",keg1,NULL) ) AS ketpres1'),
            DB::raw('GROUP_CONCAT( if(prestasike="P2",keg1,NULL) ) AS prestasi2'),
            DB::raw('GROUP_CONCAT( if(prestasike="P2",keg1,NULL) ) AS ketpres2'),
            DB::raw('GROUP_CONCAT( if(prestasike="P3",keg1,NULL) ) AS prestasi3'),
            DB::raw('GROUP_CONCAT( if(prestasike="P3",keg1,NULL) ) AS ketpres3'),
            DB::raw('GROUP_CONCAT( if(prestasike="P4",keg1,NULL) ) AS prestasi4'),
            DB::raw('GROUP_CONCAT( if(prestasike="P4",keg1,NULL) ) AS ketpres4'),
            DB::raw('GROUP_CONCAT( if(prestasike="P5",keg1,NULL) ) AS prestasi5'),
            DB::raw('GROUP_CONCAT( if(prestasike="P5",keg1,NULL) ) AS ketpres5')
        )
        ->where('tb_master_kelas.id_master_kelas',$id)
        ->groupBy('tb_prestasi.id_kelas_siswa')
        ->get();
        $result = array(
          'data' => $data 
      );
        $data = json_encode($result);
        return $data;
    }

    public function detailPersiswaPrestasi($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester')
        ->get();

        $prestasi  = DB::table('tb_prestasi')
        ->where('id_kelas_siswa',$id)
        ->select('keg1','ketkeg1')
        ->get();
        return view('konten.prestasi.history_prestasi',compact(['datasiswa'],('prestasi')));
        //return redirect('/kelas');
    }

    public function dataUpdatePrestasi($id)
    {
        $data = DB::table('tb_prestasi')
        ->select('id_prestasi','keg1','ketkeg1')
        ->where('id_prestasi',$id)
        ->get();
      
       return response()->json([
          'data' => $data
        ]);
    }
    
    public function updatePrestasi(Request $request)
    {
        $user = Session::get('username');
        date_default_timezone_set("Asia/Bangkok");

        DB::table('tb_prestasi')
        ->where('id_prestasi', $request->idprestasi)
        ->update([
            'keg1' => $request->kegiatan,
            'ketkeg1' => $request->ketkegiatan,
            'updated_by' => $user,
            'updated_timestamp' => date('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Data berhasil diupdate.');
    }

    public function deletePrestasi(Request $request)
    {
        DB::table('tb_prestasi')
       ->where('id_prestasi',$request->idprestasi)->delete();

        return back()->with('success', 'Data berhasil Dihapus.');
    }

    //START
    //-----------------------------------------SISWA PRESTASI---------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
    public function siswaPrestasi()
    {
        return view('konten.prestasi.siswa_prestasi');
    }

    public function dataSiswaPrestasi()
    {
        $user = Session::get('username');   
        $prestasi = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->where('tb_master_kelas.status',"active")
        ->where('tb_siswa.nis',$user)
        ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas','tb_kelas_siswa.id_kelas_siswa')
        ->groupBy('tahun_ajaran','nama_kelas')
        ->get();
        $result = array(
          'data' => $prestasi 
      );
        $prestasi = json_encode($result);
        return $prestasi;
    }

    public function siswaPrestasiDetail($id)
    {
        $datasiswa = DB::table('tb_kelas_siswa')
        ->join('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
        ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
        ->join('tb_siswa', 'tb_siswa.id_siswa', '=', 'tb_kelas_siswa.id_siswa')
        ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
        ->where('tb_kelas_siswa.id_kelas_siswa',$id)
        ->select('nis','nama_siswa','nama_kelas','tahun_ajaran','semester','nama_guru')
        ->get();

        $prestasi  = DB::table('tb_prestasi')
        ->where('id_kelas_siswa',$id)
        ->select('keg1','ketkeg1')
        ->get();
        return view('konten.prestasi.siswa_history_prestasi',compact(['datasiswa'],('prestasi')));
    }
    //END
    //-----------------------------------------SISWA PRESTASI---------------------------------------//
    //--------------------------------------------- SMOY -------------------------------------------//
}
