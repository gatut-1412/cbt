<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class formatUploadController extends Controller
{
    //
    public function tampilFormatUpload()
    {
        $templateupload =  DB::table('tb_format_upload')->get();
        $matpelajaran = null;
        return view('konten.format_upload.format_upload', compact(['templateupload']));
    }
    public function dataMatPelajaran()
    {
        $mapel = DB::table('tb_pelajaran')
        ->join('tb_kelompok_pelajaran', 'tb_pelajaran.id_kel_pelajaran', '=', 'tb_kelompok_pelajaran.id_kel_pelajaran')
        ->where('tb_pelajaran.status',"active")
        ->select('tb_pelajaran.nama_pelajaran','tb_kelompok_pelajaran.nama_kel_pelajaran','tb_pelajaran.status')
        ->get();

        $result = array(
        'data' => $mapel 
        );
        $mapel = json_encode($result);
        return $mapel;
    }
    public function tambahMatPelajaran(Request $request)
    {
        DB::table('tb_pelajaran')->insert([
            'nama_pelajaran' => $request->namapel,
            'id_kel_pelajaran' => $request->idkel,
            'created_by' => "smoy",
            'status' => "active"
        ]);
        return back()->with('success', 'Berhasil Tambah Data.');
    }

    public function editMatPelajaran()
    {

        return redirect('/matpelajaran');
    }
}
