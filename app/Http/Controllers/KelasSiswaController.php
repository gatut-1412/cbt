<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class kelassiswaController extends Controller
{
    //
  public function tampilKelasSiswa()
  {
    $kelassiswa = null;
    return view('konten.kelassiswa.kelassiswa', compact(['kelassiswa']));
  }
  public function dataKelasSiswa()
  {
    $kelassiswa = DB::table('tb_kelas_siswa')
    ->rightjoin('tb_master_kelas', 'tb_master_kelas.id_master_kelas', '=', 'tb_kelas_siswa.id_master_kelas')
    ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
    ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
    ->where('tb_master_kelas.status',"active")
    ->select('nama_guru','nama_kelas','tb_master_kelas.id_master_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas',DB::raw('count(tb_kelas_siswa.id_kelas_siswa) as jumlah'))
    ->groupBy('tb_master_kelas.id_master_kelas','nama_kelas')
    ->get();
    $result = array(
      'data' => $kelassiswa 
    );
    $kelassiswa = json_encode($result);
    return $kelassiswa;
  }

  public function dataSiswa2()
  {

   $first = DB::table('tb_siswa')
   ->leftjoin('tb_kelas_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
   ->leftjoin('tb_master_kelas', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
   ->select('tb_siswa.id_siswa', 'nama_siswa', 'nis', 'jenis_kelamin',DB::raw('year(tanggal_dikelas) as angkatan'), 'nama_kelas')
   ->where('tb_siswa.status',"active")->whereNotExists(function ($query) {
     $query->select(DB::raw(1))
     ->from('tb_kelas_siswa')
     ->whereRaw('tb_kelas_siswa.id_siswa = tb_siswa.id_siswa')
     ->whereRaw('tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas');
   });

   $number = request()->segment(4);
 //$number = 14;

   $siswa = DB::table('tb_siswa')
   ->leftjoin('tb_kelas_siswa', 'tb_kelas_siswa.id_siswa', '=', 'tb_siswa.id_siswa')
   ->leftjoin('tb_master_kelas', 'tb_kelas_siswa.id_master_kelas', '=', 'tb_master_kelas.id_master_kelas')
   ->select('tb_siswa.id_siswa', 'nama_siswa', 'nis', 'jenis_kelamin',DB::raw('year(tanggal_dikelas) as angkatan'), 'nama_kelas')
   ->where('tb_siswa.status',"active")->whereExists(function ($query) use ($number) {      
     $query->select(DB::raw(1))
     ->from('tb_kelas_siswa')
     ->whereRaw('tb_kelas_siswa.id_siswa = tb_siswa.id_siswa')
     ->where('tb_kelas_siswa.id_master_kelas','=',$number);
   })->union($first)->get();


         // $siswa = DB::select('select sw.* from tb_siswa sw LEFT JOIN tb_kelas_siswa  tks on sw.id_siswa = tks.id_siswa
         // lrft join tb_master_kelas tms on tks.id_master_kelas = tms.id_master_kelas  where not EXISTS(select null from tb_master_kelas mkel where mkel.id_siswa = sw.id_siswa)')->get();


   $result = array(
    'data' => $siswa,
  );

   $siswa = json_encode($result);
        // var_dump($siswa);
        // exit();
   return $siswa;
 }

 public function editKelasSiswa($id)
 {

   //return $this->dataSiswa2($id);

   $kelassiswa = DB::table('tb_master_kelas')
   ->join('tb_tahun_ajaran', 'tb_master_kelas.id_tahun_ajaran', '=', 'tb_tahun_ajaran.id_tahun_ajaran')
   ->join('tb_guru', 'tb_master_kelas.id_guru', '=', 'tb_guru.id_guru')
   ->where('tb_master_kelas.id_master_kelas',$id)
   ->select('nama_guru','nama_kelas','tahun_ajaran','semester','tb_master_kelas.id_master_kelas')
   ->get();
   return view('konten.kelassiswa.update_kelas_siswa',compact('kelassiswa'));
 }



 public function updateKelasSiswa2(Request $request)
 {
  $nc = 0;
        //  var_dump($request->id_siswa);
        //  var_dump($request->id_siswa_un);
        // exit();

  $countsiswakelasn = DB::table('tb_kelas_siswa')->where('id_master_kelas',$request->id_master_kelas)->count();
  
  if(empty($request->id_siswa)){

   DB::table('tb_kelas_siswa')
     ->where('id_master_kelas',$request->id_master_kelas)->delete();
    return back()->with('success', 'Berhasil Update Data.');

  }

  if (count($request->id_siswa) < $countsiswakelasn ) {
    $arrsiswa[] = $request->id_siswa;
    $kelassiswa = DB::table('tb_kelas_siswa')->select('id_siswa')
    ->where('id_master_kelas',$request->id_master_kelas)->get();
    $result = array(
      'data' => $kelassiswa,
    );
    $kelassiswa = json_encode($result,true);
    $data2 = json_decode($kelassiswa, true);

    // echo  $data2['data'][0]['id_siswa'];
    // echo "\n" ;
    // var_dump($request->id_siswa);
    // exit();
    for ($i=0; $i <count($data2['data']); $i++) { 
      echo  $data2['data'][$i]['id_siswa'];
      $sama = 0;
      for ($j = 0; $j <count($request->id_siswa);  $j++) {

        if ($data2['data'][$i]['id_siswa'] == $request->id_siswa[$j]) {
         $sama--;
       }else{
        $sama++;
      }
    }
    if ($sama == count($request->id_siswa)) {
     DB::table('tb_kelas_siswa')->where('id_siswa',$data2['data'][$i]['id_siswa'])
     ->where('id_master_kelas',$request->id_master_kelas)->delete();
     return back()->with('success', 'Berhasil Update Data.');
   }

 }
  // exit();
}

else{

for ($i = 0; $i <count($request->id_siswa);  $i++) {

 $kelassiswa = DB::table('tb_kelas_siswa')->where('id_siswa',$request->id_siswa[$i])->get();
 $result = array(
  'data' => $kelassiswa,
);

 $kelassiswa = json_encode($result,true);
 $data2 = json_decode($kelassiswa, true);
 $id_siswa_cek = 0;
 if (count($data2['data']) > 0){
  $id_siswa_cek = $data2['data'][0]['id_siswa'];

}

if($id_siswa_cek == 0){
  $data[] = [
    'id_siswa' => $request->id_siswa[$i],
    'id_master_kelas' => $request->id_master_kelas[$i]
  ];
}else{
  $nc++;
}
}


if($nc < count($request->id_siswa)){
  DB::table('tb_kelas_siswa')->insert($data);
  return back()->with('success', 'Berhasil Tambah Data.');
}else{
  return back()->with('nochange', 'Tidak ada perubahan data.');
}

}

}



public function tambahMappingKelasSiswa()
{
  return view('konten.mappingkelas.tambah_mappingkelas');
}
public function proses(Request $request)
{
  DB::table('tb_kelas')->insert([
    'nama_kelas' => $request->namakelas,
    'created_by' => "smoy",
    'status' => "active"
  ]);
  return back()->with('success', 'Berhasil Tambah Data.');
        //return redirect('/kelas');
}


}
