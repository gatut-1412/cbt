<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'tb_pengusaha';
 
// Table's primary key
$primaryKey = 'id_pengusaha';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
// $columns = array(
//     array( 'db' => 'name', 'dt' => 0 ),
//     array( 'db' => 'salary',  'dt' => 1 ),
//     array( 'db' => 'age',   'dt' => 2 ),
// );
 
// // SQL server connection information
// $sql_details = array(
//     'user' => 'root',
//     'pass' => null,
//     'db'   => 'dbphpserverside',
//     'host' => 'localhost'
// );
 $columns = array(
    array( 'db' => 'kode_pengusaha', 'dt' => 0 ),
    array( 'db' => 'nama_pengusaha',   'dt' => 1 ),
    array( 'db' => 'npwp',  'dt' => 2 ),
    array( 'db' => 'direk', 'dt' => 3 ),
    array( 'db' => 'nama_perusahaan',  'dt' => 4 ),
    array( 'db' => 'alamat', 'dt' => 5 ),
    array( 'db' => 'jenis_perusahaan',  'dt' => 6 ),
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => null,
    'db'   => 'sipakaid_sipaka',
    'host' => 'localhost'
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
$where = "kode_pengusaha ='smoy'";
 
echo json_encode(
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where )
);