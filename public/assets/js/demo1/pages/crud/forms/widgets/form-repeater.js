// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('.kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                var limitcount = $(this).parents(".kt_repeater_1").data("limit");
                var itemcount = $(this).parents(".kt_repeater_1").find("div[data-repeater-item]").length;

                if (limitcount) {
                    if (itemcount <= limitcount) {
                        $(this).slideDown();
                    } else {
                        $(this).remove();
                        alert('Maksimal 5 untuk upload file');
                    }
                } else {
                    $(this).slideDown();
                }

                if (itemcount >= limitcount) {
                    $(".kt_repeater_1 input[data-repeater-create]").hide("slow");
                }
            },

            hide: function (deleteElement) {                
               var limitcount = $(this).parents(".kt_repeater_1").data("limit");
               var itemcount = $(this).parents(".kt_repeater_1").find("div[data-repeater-item]").length;

               if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }

            if (itemcount <= limitcount) {
                $(".kt_repeater_1 input[data-repeater-create]").show("slow");
            }                 
        }   
    });
    }


    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();



jQuery(document).ready(function() {
    KTFormRepeater.init();
});

