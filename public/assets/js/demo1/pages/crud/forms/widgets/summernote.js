"use strict";
// Class definition

//smoy
var KTSummernote = function () {    
    // Private functions
    var textarea = function () {

        $('#addtugasonline').summernote({
            height: 150,
            styleWithSpan: false
        });

        $('#detailtgsonline').summernote({
            height: 150
        });

        $('#addsoalpgcbttextarea').summernote({
            height: 120
        });

        $('#addsoalesaycbttextarea').summernote({
            height: 150
        });

        $('#addpengumuman').summernote({
            height: 150
        });

        $('#jawabanesaysiswa').summernote({
            height: 150
        });
    }




    return {
        // public functions
        init: function() {
            textarea(); 
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTSummernote.init();
});

