// Class definition

var KTFormControls = function () {
    // Private functions
    
    var demo1 = function () {
        $( "#form_users" ).validate({
            // define validation rules
            rules: {
                username: {
                    required: true,
                    minlength: 5 
                },
                email: {
                    required: false,
                    email: true,
                    minlength: 10 
                },
                password: {
                    required: true,
                    minlength: 8  
                },
                nama: {
                    required: true,
                },
                level: {
                    required: true
                },

            },
            messages :{
                username:{
                    required :"Silahkan Isi Username",
                    minlength : "Minimal 5 Karakter"
                },
                password:{
                    required :"Silahkan Isi password",
                    minlength : "Minimal 8 Karakter"
                },
                nama:{
                    required :"Silahkan Isi Nama Lengkap Anda",
                },
                level:{
                    required :"Silahkan Isi Level",
                },
                email:{
                    required :"Silahkan Isi email anda",
                    email :"Format email anda salah",
                },

            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            // submitHandler: function (form) {
            //     //form[0].submit(); // submit the form
            // }
        });       
    }

    var formsiswa = function () {
        $( "#form_siswa" ).validate({
            // define validation rules
            rules: {
                nis: {
                    required: true 
                },
                namasiswa: {
                    required: true 
                },
                tmptlahir: {
                    required: true 
                },
                tgllahir: {
                    required: true 
                },

            },
            messages :{
                nis:{
                    required :"Silahkan Isi NIS Siswa"
                },
                namasiswa:{
                    required :"Silahkan Isi Nama Siswa"
                },
                tmptlahir:{
                    required :"Silahkan Isi Tempat Lahir Siswa"
                },
                tgllahir:{
                    required :"Silahkan Isi Tanggal Lahir Siswa"
                },

            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },
        });       
    }

    var formguru = function () {
        $( "#form_guru" ).validate({
            // define validation rules
            rules: {
                nip: {
                    required: true 
                },
                namaguru: {
                    required: true 
                },

            },
            messages :{
                nip:{
                    required :"Silahkan Isi NIP Guru"
                },
                namaguru:{
                    required :"Silahkan Isi Nama Guru"
                },

            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },
        });       
    }

    var demo2 = function () {
        $( "#kt_form_2" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information
                billing_card_name: {
                    required: true
                },
                billing_card_number: {
                    required: true,
                    creditcard: true
                },
                billing_card_exp_month: {
                    required: true
                },
                billing_card_exp_year: {
                    required: true
                },
                billing_card_cvv: {
                    required: true,
                    minlength: 2,
                    maxlength: 3
                },

                // Billing Address
                billing_address_1: {
                    required: true
                },
                billing_address_2: {

                },
                billing_city: {
                    required: true
                },
                billing_state: {
                    required: true
                },
                billing_zip: {
                    required: true,
                    number: true
                },

                billing_delivery: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            // invalidHandler: function(event, validator) {
            //     swal.fire({
            //         "title": "", 
            //         "text": "There are some errors in your submission. Please correct them.", 
            //         "type": "error",
            //         "confirmButtonClass": "btn btn-secondary",
            //         "onClose": function(e) {
            //             console.log('on close event fired!');
            //         }
            //     });

            //     event.preventDefault();
            // },

            // submitHandler: function (form) {
            //     //form[0].submit(); // submit the form
            //     swal.fire({
            //         "title": "", 
            //         "text": "Form validation passed. All good!", 
            //         "type": "success",
            //         "confirmButtonClass": "btn btn-secondary"
            //     });

            //     return false;
            // }
        });       
    }

    return {
        // public functions
        init: function() {
            demo1(); 
            demo2();
            formsiswa();
            formguru();
        }
    };
}();

jQuery(document).ready(function() {    
    KTFormControls.init();
});