'use strict';
var CbtDataSourceAjaxClient = function() {
	var tampilsoalcbt = function() {
		var table = $('#table_soal_cbt').DataTable({
			responsive: true,
			ajax: {
				url: '/datasoalcbt',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'judul_soal'},
			{data: 'jenis_soal'},
			{data: 'tanggal_dibuat'},
			{data: 'waktu'},
			{data: 'token'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/soalcbt/tambahdetailsoalcbt/`+full.id_soal_cbt+`" >
					<button>Cek Soal</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();

		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var tampildetailsoalcbt = function() {
		var valuee3 = $('#idsoalcbt').val();
		var table = $('#table_detailsoalcbt').DataTable({
			responsive: true,
			ajax: {
				url: 'datadetailsoalcbt/'+valuee3,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{
				data: 'gambar_soal',
				render: function(data, type, full, meta) { 
					if( data === null) {
						return full.soal_pg;
					}
					else{ 
						return  full.soal_pg+`<br><img img onclick="onClick(this)" class="modal-hover-opacity"  src="https://siakad.resumeideas.org/soal_cbt/pg/soal/`+data+`" width="100px">'`;
					}
					colnsole.log(data);
				},
			},
			{
				data: 'pilihan_a',
				render : function(data, type, full, meta){
					var re = /^\d{2}\-\d{2}\-\d{4}$/;
					var res = data.substring(0, 10);
					var a = res;
					
					if (!re.test(a)) { 
						return data;
					} 
					else { 
						return `<img img onclick="onClick(this)" class="modal-hover-opacity" src="https://siakad.resumeideas.orgsoal_cbt/pg/option/`+data+`" width="100px">'`;
					}
				}
			},
			{
				data: 'pilihan_b',
				render : function(data, type, full, meta){
					var re = /^\d{2}\-\d{2}\-\d{4}$/;
					var res = data.substring(0, 10);
					var a = res;
					
					if (!re.test(a)) { 
						return data;
					} 
					else { 
						return '<img onclick="onClick(this)" class="modal-hover-opacity"  src="https://siakad.resumeideas.orgsoal_cbt/pg/option/'+data+'" width="100px">';
					}
				}
			},
			{
				data: 'pilihan_c',
				render : function(data, type, full, meta){
					var re = /^\d{2}\-\d{2}\-\d{4}$/;
					var res = data.substring(0, 10);
					var a = res;
					
					if (!re.test(a)) { 
						return data;
					} 
					else { 
						return '<img  onclick="onClick(this)" class="modal-hover-opacity" src="https://siakad.resumeideas.org/soal_cbt/pg/option/'+data+'" width="100px">';
					}
				}

			},
			{
				data: 'pilihan_d',
				render : function(data, type, full, meta){
					var re = /^\d{2}\-\d{2}\-\d{4}$/;
					var res = data.substring(0, 10);
					var a = res;
					
					if (!re.test(a)) { 
						return data;
					} 
					else { 
						return '<img  onclick="onClick(this)" class="modal-hover-opacity" src="https://siakad.resumeideas.org/soal_cbt/pg/option/'+data+'" width="100px">';
					}
				}
			},
			{
				data: 'pilihan_e',
				render : function(data, type, full, meta){
					var re = /^\d{2}\-\d{2}\-\d{4}$/;
					var res = data.substring(0, 10);
					var a = res;
					
					if (!re.test(a)) { 
						return data;
					} 
					else { 
						return '<img onclick="onClick(this)" class="modal-hover-opacity"  src="https://siakad.resumeideas.org/soal_cbt/pg/option/'+data+'" width="100px">';
					}
				}
			},
			{data: 'kunci_jawaban'},
			{data: 'bobot'},
			{data: 'Action', responsivePriority: -1},		
			],
			columnDefs: [
			{	
				targets: -10,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<a href="editsoalpgcbt/`+full.id_detail_soal_cbt+`" >
					<button>Edit</button>
					</a>
					<a href="tambahdetailsoalcbt/hapus/`+full.id_detail_soal_cbt+`" 
					onclick="return confirm('Apakah data akan dihapus ?');">
					<button>Hapus</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var tampildetailsoalcbtesay = function() {
		var valuee4 = $('#idsoalcbtesay').val();
		var table = $('#table_detailsoalcbtesay').DataTable({
			responsive: true,
			ajax: {
				url: 'datadetailsoalcbtesay/'+valuee4,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'soal_esay'},
			{data: 'bobot'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -4,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/soalcbt/tambahdetailsoalcbt/`+full.id_soal_cbt+`" >
					<button>Edit</button>
					</a>
					<a href="/soalcbt/tambahdetailsoalcbt/`+full.id_soal_cbt+`" >
					<button>Hapus</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var tampilMappingCbt = function() {
		
		var table = $('#table_mappingcbt').DataTable({
			responsive: true,
			ajax: {
				url: '/datamappingcbt',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_kelas'},
			{data: 'nama_pelajaran'},
			{data: 'judul_soal'},
			{data: 'jenis_soal'},
			{data: 'tanggal'},
			{data: 'status_mapping'},
			{data: 'Action'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -9,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -2,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					if(full.status_mapping == 'DRAFT'){
						return `
						<a href="/mapingan_cbt_perkelas/upatepublish/`+full.id_mapping_cbt+`" class="btn btn-warning btn-sm" >
						PUBLISH
						</a>
						`;
					}
					else if (full.status_mapping == 'PUBLISH')
					{
						return `
						<a target="_blank" href="/mapingan_cbt_perkelas/upateselesai/`+full.id_mapping_cbt+`" class="btn btn-primary btn-sm" >
						SELESAI
						</a>
						`;
					}

					else if (full.status_mapping == 'SELESAI')
					{
						return `
						<a href="/lihat_hasil_ujian_cbt" class="btn btn-danger btn-sm">HASIL </a>
						`;
					}				
				},
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					
					<a href="/mapingan_cbt_perkelas/hapus/`+full.id_mapping_cbt+`" 
					onclick="return confirm('Apakah data akan dihapus ?');">
					<button>Hapus</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();


		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var tampilpengumuman = function() {
		var table = $('#table_pengumumancbt').DataTable({
			responsive: true,
			ajax: {
				url: '/datapengumumancbt',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'judul_soal'},
			{data: 'jenis_soal'},
			{data: 'tanggal'},
			{data: 'nama_kelas'},
			{data: 'tahun_ajaran'},
			{data: 'semester'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -8,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/pengumuman_cbt/detailpengumumancbt/`+full.id_soal_cbt+`" >
					<button>Lihat Detail</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();

		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var tampilhasilujian = function() {
		var table = $('#table_hasil_ujiancbt').DataTable({
			responsive: true,
			ajax: {
				url: '/datahasilujian',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'judul_soal'},
			{data: 'jenis_soal'},
			{data: 'tanggal'},
			{data: 'nama_kelas'},
			{data: 'tahun_ajaran'},
			{data: 'waktu'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -8,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a target="_blank" href="/lihat_hasil_ujian_cbt/detailsiswa/`+full.id_mapping_cbt+`" >
					<button>Lihat Detail</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var detailhasilujian = function() {
		var valuee9 = $('#idmappingcbt').val();
		var table = $('#table_detailhasilujian').DataTable({
			responsive: true,
			ajax: {
				url: 'datadetailhasilujian/'+valuee9,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_siswa'},
			{data: 'jml_benar'},
			{data: 'nilai'},
			{data: 'nilai_bobot'},	
			{data: 'tgl_mulai'},
			{data: 'tgl_selesai'},	
			{data: 'waktu_mengerjakan'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -9,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href='detailsiswa/laporan_jawabansiswa/`+full.id_ujian+`' target='_blank'>
					<button>Print Jawaban Siswa</button>
					</a>
					`;
				},
			},
			],
		});



		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();


		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	return {

		init: function() {
			tampilsoalcbt();
			tampildetailsoalcbt();
			tampildetailsoalcbtesay();
			tampilMappingCbt();
			tampilpengumuman();
			tampilhasilujian();
			detailhasilujian();

		},

	};

}();

jQuery(document).ready(function() {
	CbtDataSourceAjaxClient.init();
});