'use strict';
var DataEskul = function() {
	var eskul = function() {
		var table1 = $('#table_eskul').DataTable({
			responsive: true,
			ajax: {
				url: '/dataeskul',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_eskul'},
			{data: 'nama_guru'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -4,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<button class='btn btn-sm btn-info update_eskul' data-id=`+full.id_eskul+` >Update</button>
					<button class='btn btn-sm btn-danger delete_eskul' data-id=`+full.id_eskul+` data-toggle='modal' data-target='#delete_eskul' >Hapus</button>
					`;
				},
			},
			],
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_eskul').on('click', '.update_eskul', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#ideskul').val(id);
			console.log(id);
			$.get('/eskul/dataediteskul/' + id + '/edit', function (data, type, full, meta)  {
				$('#update_eskul').modal('show');
				$('#smoy1').val(data.data[0].nama_eskul);
				$('.smoy2').val(data.data[0].id_guru);
			})
		});

		$('#table_eskul').on('click', '.delete_eskul', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#ideskul1').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table1.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table1.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table1.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table1.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table1.button(4).trigger();
		});
	};

	var mappingeskul = function() {
		var table1 = $('#table_mappingeskul').DataTable({
			responsive: true,
			ajax: {
				url: '/datamappingeskul',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_eskul'},
			{data: 'nama_siswa'},
			{data: 'nama_guru'},
			{data: 'keterangan'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: 0,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<button class='btn btn-sm btn-info update_mappingeskul' data-id=`+full.id_mapping_eskul+` >Update</button>
					<button class='btn btn-sm btn-danger delete_mappingeskul' data-id=`+full.id_mapping_eskul+` data-toggle='modal' data-target='#delete_mappingeskul' >Hapus</button>
					`;
				},
			},
			],
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_mappingeskul').on('click', '.update_mappingeskul', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmappingeskul').val(id);
			console.log(id);
			$.get('/mappingeskul/dataeditmappingeskul/' + id + '/edit', function (data, type, full, meta)  {
				$('#update_mappingeskul').modal('show');
				$('.smoy1').val(data.data[0].id_eskul);
				$('.smoy2').val(data.data[0].id_kelas_siswa);
				$('#smoy3').val(data.data[0].keterangan);
			})
		});
		$('#table_mappingeskul').on('click', '.delete_mappingeskul', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmappingeskul1').val(id);
		});

		var table2 = $('#table_mappingeskul_perkelas').DataTable({
			responsive: true,
			ajax: {
				url: '/datamappingeskulperkelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/mappingeskul_perkelas/detail/`+full.id_master_kelas+`" >
					<button class='btn btn-sm btn-info'>Detail Siswa</button>
					</a>
					`;
				},
			},
			],
		});

		$('#table-filter').on('change', function(){
			table2.search(this.value).draw();   
		});

		var valuee8 = $('#id_master_kelaseskul').val();
		var table3 = $('#table_mappingeskul_perkelas_siswa').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'datamappingeskulperkelassiswa/'+valuee8,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: 'jumlah'},
			{data: 'action'},
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<a href="history/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>Detail Siswa</button>
					</a>
					`;
				},
			},

			],
			'order': [[1, 'asc']]
		});

		var table4 = $('#table_siswa_eskul').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: '/datasiswaeskul',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'nama_guru',orderable:false},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/siswa_eskul/detail/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table1.button(0).trigger();
			table2.button(0).trigger();
			table3.button(0).trigger();
			table4.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table1.button(1).trigger();
			table2.button(1).trigger();
			table3.button(1).trigger();
			table4.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table1.button(2).trigger();
			table2.button(2).trigger();
			table3.button(2).trigger();
			table4.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table1.button(3).trigger();
			table2.button(3).trigger();
			table3.button(3).trigger();
			table4.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table1.button(4).trigger();
			table2.button(4).trigger();
			table3.button(4).trigger();
			table4.button(4).trigger();
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			eskul();
			mappingeskul();
		},
	};

}();

jQuery(document).ready(function() {
	DataEskul.init();
});