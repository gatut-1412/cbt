'use strict';
var Prestasi = function() {
	var prestasiSiswa = function() {
		var table1 = $('#table_prestasi').DataTable({
			responsive: true,
			ajax: {
				url: '/dataprestasi',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin'},
			{data: 'nama_kelas'},
			{data: 'prestasike'},
			{data: 'keg1'},
			{data: 'ketkeg1'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -9,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<button class='btn btn-sm btn-info update_prestasi' data-id=`+full.id_prestasi+` >Update</button>
					<button class='btn btn-sm btn-danger delete_prestasi' data-id=`+full.id_prestasi+` data-toggle='modal' data-target='#delete_prestasi' >Hapus</button>
					`;
				},
			},
			],
		});

		var table2 = $('#table_prestasi_perkelas').DataTable({
			responsive: true,
			ajax: {
				url: '/dataprestasiperkelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},	

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/lihat_prestasi/detail/`+full.id_master_kelas+`" >
					<button class='btn btn-sm btn-info'>Detail Siswa</button>
					</a>
					`;
				},
			},
			],
		});
		var valuee8 = $('#id_master_kelas_prestasi').val();
		var table3 = $('#table_prestasi_kelas_persiswa').DataTable({
			// responsive: true,
			scrollX: true,
			ajax: {
				url: 'dataprestasiperkelassiswa/'+valuee8,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},

			// fixedColumns:   {
			// 	leftColumns: 0,
			// 	rightColumns: 1,
			// },
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'prestasi1',orderable: false},
			{data: 'ketpres1',orderable: false},
			{data: 'prestasi2',orderable: false},
			{data: 'ketpres2',orderable: false},
			{data: 'prestasi3',orderable: false},
			{data: 'ketpres3',orderable: false},
			{data: 'prestasi4',orderable: false},
			{data: 'ketpres4',orderable: false},
			{data: 'prestasi5',orderable: false},
			{data: 'ketpres5',orderable: false},
			{data: 'Action',   responsivePriority: -1},	

			],
			columnDefs: [
			{	
				targets: -14,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				responsivePriority: -1,
				render: function(data, type, full, meta) {		

					return `
					<a href="detailpersiswa/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-primary'>Detail</button>
					</a>
					`;
				},
			},
			],
		});

		var table4 = $('#table_siswa_prestasi').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: '/datasiswaprestasi',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'nama_guru',orderable:false},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/siswa_prestasi/detail/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_prestasi').on('click', '.update_prestasi', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#txt_userid').val(id);
			console.log(id)
			$.get('/dataprestasisiswa/' + id + '/edit', function (data, type, full, meta)  {
				$('#update_prestasi').modal('show');
				$('#smoy2').val(data.data[0].keg1);
				$('#smoy1').val(data.data[0].ketkeg1);
			})
		});

		$('#table_prestasi').on('click', '.delete_prestasi', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idpres').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table1.button(0).trigger();
			table2.button(0).trigger();
			table3.button(0).trigger();
			table4.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table1.button(1).trigger();
			table2.button(1).trigger();
			table3.button(1).trigger();
			table4.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table1.button(2).trigger();
			table2.button(2).trigger();
			table3.button(2).trigger();
			table4.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table1.button(3).trigger();
			table2.button(3).trigger();
			table3.button(3).trigger();
			table4.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table1.button(4).trigger();
			table2.button(4).trigger();
			table3.button(4).trigger();
			table4.button(4).trigger();
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			prestasiSiswa();
		},

	};

}();

jQuery(document).ready(function() {
	Prestasi.init();
});