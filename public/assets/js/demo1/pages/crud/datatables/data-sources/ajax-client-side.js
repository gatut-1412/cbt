'use strict';
var KTDatatablesDataSourceAjaxClient = function() {
	var initTableUsers = function() {
		var table = $('#table_users').DataTable({
			responsive: true,
			ajax: {
				url: '/datausers',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'username'},
			{data: 'nama'},
			{data: 'email'},
			{data: 'level'},
			{data: 'created_by'},
			{data: 'updated_by'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -8,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="#" data-id=`+full.id_users+` data-toggle='modal' data-target='#users_update' class="btn btn-sm btn-clean btn-icon btn-icon-md update_users" title="View">
					<i class="la la-edit"></i>
					</a>
					<a href="#" data-id=`+full.id_users+` data-toggle='modal' data-target='#users_nonactive' class="btn btn-sm btn-clean btn-icon btn-icon-md users_nonactive" title="View">
					<i class="la la-trash"></i>
					</a>`;
				},
			},
			],
		});
		$('#table_users').on('click', '.update_users', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idusers1').val(id);
			console.log(id);
			$.get('/users/dataeditusers/' + id + '/edit', function (data, type, full, meta)  {
				$('#username').val(data.data[0].username);
				$('#nama').val(data.data[0].nama);
				$('#email').val(data.data[0].email);
				$('#level').val(data.data[0].level);

			})
		});

		$('#table_users').on('click', '.users_nonactive', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idusers2').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};
	var initTableKelas = function() {
		var table = $('#table_kelas').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_kelas'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -3,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="`+full.id_kelas+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit Kelas">
					<i class="la la-edit"></i>
					</a>
					<a href="#deleteEmployeeModal" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="modal" title="Hapus Kelas">
					<i class="la la-trash"></i>
					</a>`;
				},
				},
			],
				
		});
		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};
	var initTableGuruMapel = function() {
		var table = $('#table_gurumapel').DataTable({
			responsive: true,
			ajax: {
				url: '/datagurumapel',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nama_guru'},
			{data: 'nama_pelajaran'},
			{data: 'nama_kelas'},
			{data: 'tahun_ajaran'},
			{data: 'semester'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
                        <a href="gurumapel/hapus/`+full.id_guru_mapel+`" 
					onclick="return confirm('Apakah data akan dihapus ?');">
					<button>Hapus</button>`;
				},
			},
			],
		});
		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};
	var initTableMaster = function() {
		var table = $('#table_tahunajaran').DataTable({
			responsive: true,
			ajax: {
				url: '/datatahunajaran',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				      contentType: "application/json; charset=utf-8"
			},
			buttons: [
				'print',
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
			],
			columns: [
				{data: 'no'},
				{data: 'tahun_ajaran'},
				{data: 'status'},
				{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
				{	
					targets: -4,
					title: 'no',
					orderable: true,
					 "data": "id",
				    render: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
				    }
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		
		
					return `
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md update_tahunajaran" data-id=`+full.id_tahun_ajaran+` title="Update">
                          <i class="la la-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md hapus_tahunajaran" data-id=`+full.id_tahun_ajaran+` data-toggle='modal' data-target='#delete_tahunajaran' title="Delete">
                          <i class="la la-trash"></i>
                        </a>`;
					},
				},
			],
		});
		$('#table_tahunajaran').on('click', '.update_tahunajaran', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idtahunajaran').val(id);
			console.log(id);
			$.get('/tahunajaran/dataedittahunajaran/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_tahunajaran').modal('show');
				$('#smoy1').val(data.data[0].tahun_ajaran);

			})
		});

		$('#table_tahunajaran').on('click', '.hapus_tahunajaran', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idtahunajaran1').val(id);
		});
		var table1 = $('#table_guru').DataTable({
			responsive: true,
			ajax: {
				url: '/dataguru',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				      contentType: "application/json; charset=utf-8"
			},
			buttons: [
				'print',
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
			],
			columns: [
				{data: 'no'},
				{data: 'nip'},
				{data: 'nama_guru'},
				{data: 'jenis_kelamin'},
				{data: 'is_bk'},
				{data: 'status'},
				{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
				{	
					targets: -7,
					title: 'No',
					orderable: true,
					 "data": "id",
				    render: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
				    }
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		
					return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                 <a class="dropdown-item guru_nonactive" href="#" data-id=`+full.id_guru+` data-toggle='modal' data-target='#guru_nonactive'><i class="la la-leaf"></i> Update Status (non active)</a>
                                <a class="dropdown-item" href="/profilguru/print/`+full.id_guru+`" target="_blank"><i class="la la-print"></i> Generate Report</a>
                            </div>
                        </span>
                        <a href="/guru/edit/`+full.id_guru+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>`;
					},
				},
			],
		});
		$('#table_guru').on('click', '.guru_nonactive', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idguru1').val(id);
		});

		var table2 = $('#table_matpel').DataTable({
			responsive: true,
			ajax: {
				url: '/datamatpelajaran',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				      contentType: "application/json; charset=utf-8"
			},
			buttons: [
				'print',
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
			],
			columns: [
				{data: 'no'},
				{data: 'nama_pelajaran'},
				{data: 'nama_kel_pelajaran'},
				{data: 'status'},
				{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
				{	
					targets: -5,
					title: 'No',
					orderable: true,
					 "data": "id",
				    render: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
				    }
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		
		
					return `
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md update_matpel" data-id=`+full.id_pelajaran+` title="Update">
                          <i class="la la-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md hapus_matpel" data-id=`+full.id_pelajaran+` data-toggle='modal' data-target='#delete_matpel' title="Delete">
                          <i class="la la-trash"></i>
                        </a>`;
					},
				},
			],
		});

		$('#table_matpel').on('click', '.update_matpel', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmatpel').val(id);
			console.log(id);
			$.get('/matpelajaran/dataeditmatpel/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_matpel').modal('show');
				$('#smoy1').val(data.data[0].nama_pelajaran);
				$('.smoy2').val(data.data[0].id_kel_pelajaran);

			})
		});

		$('#table_matpel').on('click', '.hapus_matpel', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmatpel1').val(id);
		});

		var table3 = $('#table_siswa').DataTable({
			responsive: true,
			ajax: {
				url: '/datasiswa',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				      contentType: "application/json; charset=utf-8"
			},
			buttons: [
				'print',
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
			],
			columns: [
				{data: 'no'},
				{data: 'nis'},
				{data: 'nama_siswa'},
				{data: 'jenis_kelamin'},
				{data: 'tempat_lahir'},
				{data: 'tanggal_lahir'},
				{data: 'agama'},
				{data: 'status_keluarga'},
				{data: 'anak_ke'},
				{data: 'alamat'},
				{data: 'asal_sekolah'},
				{data: 'dikelas'},
				{data: 'tanggal_dikelas'},
				{data: 'nama_ayah'},
				{data: 'nama_ibu'},
				{data: 'alamat_orangtua'},
				{data: 'pekerjaan_ayah'},
				{data: 'pekerjaan_ibu'},
				{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
				{	
					targets: -19,
					title: 'No',
					orderable: true,
					 "data": "id",
				    render: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
				    }
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		
		
					return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item siswa_lulus" href="#" data-id=`+full.id_siswa+` data-toggle='modal' data-target='#siswa_lulus'><i class="la la-edit"></i> Update Lulus</a>
                                <a class="dropdown-item siswa_nonactive" href="#" data-id=`+full.id_siswa+` data-toggle='modal' data-target='#siswa_nonactive'><i class="la la-leaf"></i> Update Status (non active)</a>
                                 <a class="dropdown-item" href="/profilsiswa/print/`+full.id_siswa+`" target="_blank"><i class="la la-print"></i> Generate Report</a>
                            </div>
                        </span>
                        <a href="/siswa/edit/`+full.id_siswa+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>`;
					},
				},
			],
		});
		$('#table_siswa').on('click', '.siswa_lulus', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idsiswa1').val(id);
		});
		$('#table_siswa').on('click', '.siswa_nonactive', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idsiswa2').val(id);
		});

		var table4 = $('#table_kelmatpel').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelmatpelajaran',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				      contentType: "application/json; charset=utf-8"
			},
			buttons: [
				'print',
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
			],
			columns: [
				{data: 'no'},
				{data: 'nama_kel_pelajaran'},
				{data: 'status'},
				{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
				{	
					targets: -4,
					title: 'No',
					orderable: true,
					 "data": "id",
				    render: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
				    }
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		
		
					return `
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md update_kelmatpel" data-id=`+full.id_kel_pelajaran+` title="Update">
                          <i class="la la-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md hapus_kelmatpel" data-id=`+full.id_kel_pelajaran+` data-toggle='modal' data-target='#delete_kelmatpel' title="Delete">
                          <i class="la la-trash"></i>
                        </a>`;
					},
				},
			],
		});

		setInterval( function () {
    		table4.ajax.reload( null, false ); 
		}, 10000 );

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_kelmatpel').on('click', '.update_kelmatpel', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idkelmatpel').val(id);
			console.log(id);
			$.get('/kelmatpelajaran/dataeditkelmatpel/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_kelmatpel').modal('show');
				$('#smoy1').val(data.data[0].nama_kel_pelajaran);

			})
		});

		$('#table_kelmatpel').on('click', '.hapus_kelmatpel', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idkelmatpel1').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
			table1.button(0).trigger();
			table2.button(0).trigger();
			table3.button(0).trigger();
			table4.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
			table1.button(1).trigger();
			table2.button(1).trigger();
			table3.button(1).trigger();
			table4.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
			table1.button(2).trigger();
			table2.button(2).trigger();
			table3.button(2).trigger();
			table4.button(2).trigger();

		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
			table1.button(3).trigger();
			table2.button(3).trigger();
			table3.button(3).trigger();
			table4.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
			table1.button(4).trigger();
			table2.button(4).trigger();
			table3.button(4).trigger();
			table4.button(4).trigger();
		});
	};
	var initTableMappingKelas = function() {
		var table = $('#table_mapping_kelas').DataTable({
			responsive: true,
			ajax: {
				url: '/datamappingkelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'nama_guru'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
					}         
				},

				{	
					targets: -1,
					title: 'Action',
					orderable: false,
					render: function(data, type, full, meta) {		

						return `
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md update_mappingkelas" data-id=`+full.id_master_kelas+` title="Update">
                          <i class="la la-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md hapus_mappingkelas" data-id=`+full.id_master_kelas+` data-toggle='modal' data-target='#delete_mappingkelas' title="Delete">
                          <i class="la la-trash"></i>
                        </a>`;
					},
				},
				],
		});

		$('#table_mapping_kelas').on('click', '.update_mappingkelas', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmappingkelas').val(id);
			console.log(id);
			$.get('/mappingkelas/dataeditmappingkelas/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_mappingkelas').modal('show');
				$('.smoyx1').val(data.data[0].id_tahun_ajaran);
				$('#smoy2').val(data.data[0].nama_kelas);
				$('.smoyx3').val(data.data[0].semester);
				$('.smoyx4').val(data.data[0].id_guru);

			})
		});

		$('#table_mapping_kelas').on('click', '.hapus_mappingkelas', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idmappingkelas1').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};
	var initTableKelasSiswa = function() {
		var table = $('#table_kelas_siswa').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelassiswa',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/kelassiswa/edit/`+full.id_master_kelas+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit Kelas">
					<i class="la la-edit"></i>
					</a>`;
				},
			},
			],
		});
		var table1 = $('#table_penilaian').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelassiswa',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/penilaian/edit/`+full.id_master_kelas+`" >
					<button>Update</button>
					</a>
					`;
				},
			},
			],
		});
	

			$('#export_print').on('click', function(e) {
				e.preventDefault();
				table.button(0).trigger();
				table1.button(0).trigger();
			});

			$('#export_copy').on('click', function(e) {
				e.preventDefault();
				table.button(1).trigger();
				table1.button(1).trigger();
			});

			$('#export_excel').on('click', function(e) {
				e.preventDefault();
				table.button(2).trigger();
				table1.button(2).trigger();
			});

			$('#export_csv').on('click', function(e) {
				e.preventDefault();
				table.button(3).trigger();
				table1.button(3).trigger();
			});

			$('#export_pdf').on('click', function(e) {
				e.preventDefault();
				table.button(4).trigger();
				table1.button(4).trigger();
			});
	};
	var initTablesiswa2 = function() {
		var valuee = $('#id_master_kelas').val();
		var table = $('#table_siswa2').DataTable({
			responsive: true,
			ajax: {
				type : 'GET',
				url: 'edit/datasiswa2/'+valuee,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin'},
			{data: 'angkatan'},
			{data: 'nama_kelas'},

			],
			columnDefs: [
			{	
				'targets': 0,
				'orderable': false,
				'className': 'dt-body-center',
				'render': function (data, type, full, meta){
					if (full.nama_kelas != null) {
						return '<input checked type="checkbox" name="id_siswa[]" value="' + $('<div/>').text(full.id_siswa).html() + '">';
					} else {
						return '<input type="checkbox" name="id_siswa[]" value="' + $('<div/>').text(full.id_siswa).html() + '">';

					}
				}
			},
			{	
				'targets': 7,
				'orderable': false,
				'className': 'dt-body-center',
				'render': function (data, type, full, meta){


					return '<input type="hidden" name="id_master_kelas[]" value="'+document.getElementById("id_master_kelas").value+'">';
				}
			},

			{	
				targets: 1,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			],
			'order': [[1, 'asc']]
		});
		var valuee4 = $('#id_master_kelas3').val();
		var table1 = $('#table_siswa_penilaian').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'edit/datasiswapenilaian/'+valuee4,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: '1'},
			{data: '2'},
			{data: '3'},
			{data: '4'},		
			{data: '5'},
			{data: '6'},
			{data: '7'},
			{data: '8'},
			{data: '9'},
			{data: '10'},
			{data: '11'},
			{data: '12'},
			{data: '13'},
			{data: '14'},
			],
			columnDefs: [
			{	
				targets: -18,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: 4,
				title: '1',
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  class="form-control form-control-sm" name="1" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 5,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="2" maxlength="3" style="width: 50px;"></input>`;;
				},
			},
			{	
				targets: 6,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="3" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 7,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="4" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 8,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="5" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 9,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="6" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 10,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="7" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 11,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="8" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 12,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="9" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 13,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="10" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 14,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="ratarata" maxlength="3" style="width: 50px;" disabled></input>`;
				},
			},
			{	
				targets: 15,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="uts" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 16,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="uas" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 17,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="nilaiakhir" maxlength="3" style="width: 50px;" disabled></input>`;
				},
			},

			],
			'order': [[1, 'asc']]
		});

		$('#export_print').on('click', function(e) {
				e.preventDefault();
				table.button(0).trigger();
				table1.button(0).trigger();
			});

			$('#export_copy').on('click', function(e) {
				e.preventDefault();
				table.button(1).trigger();
				table1.button(1).trigger();
			});

			$('#export_excel').on('click', function(e) {
				e.preventDefault();
				table.button(2).trigger();
				table1.button(2).trigger();
			});

			$('#export_csv').on('click', function(e) {
				e.preventDefault();
				table.button(3).trigger();
				table1.button(3).trigger();
			});

			$('#export_pdf').on('click', function(e) {
				e.preventDefault();
				table.button(4).trigger();
				table1.button(4).trigger();
			});
	};

	var initCatwalkes = function() {
		var valuee5 = $('#id_master_kelas4').val();
		var table1 = $('#table_catatan_walikelas').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'detail/datacatatanwalikelas/'+valuee5,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: 'catatan'},
			{data: 'action'},
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		
					if (full.id == null)
					{
						return `
						<button class='btn btn-sm btn-info' disabled >Update</button>
						<button class='btn btn-sm btn-danger' disabled >Hapus</button>
						`;
					}
					else
					{
						return `
						<button class='btn btn-sm btn-info update_cat_walkes' data-id=`+full.id+` >Update</button>
						<button class='btn btn-sm btn-danger delete_cat_walkes' data-id=`+full.id+` data-toggle='modal' data-target='#delete_catatan_walkes' >Hapus</button>
						`;
					}
				},
			},

			],
			'order': [[1, 'asc']]
		});

		var table2 = $('#table_catatanwalikelas').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelassiswa_catatanwalkes',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/catatan_walikelas/detail/`+full.id_master_kelas+`" >
					<button>Detail</button>
					</a>

					`;
				},
			},
			],
		});

		var table3 = $('#table_siswa_catatan_walkes').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: '/datasiswacatatanwalkes',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'nama_guru',orderable:false},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/siswa_catatan_walkes/detail/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_catatan_walikelas').on('click', '.update_cat_walkes', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#txt_userid').val(id);
			console.log(id);
			$.get('asin/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_catatan_walkes').modal('show');
				$('#smoy2').val(data.data[0].nis+ " - " +data.data[0].nama_siswa);
				$('#smoy1').val(data.data[0].catatan);
			})
		});

		$('#table_catatan_walikelas').on('click', '.delete_cat_walkes', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idcat').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table1.button(0).trigger();
			table2.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table1.button(1).trigger();
			table2.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table1.button(2).trigger();
			table2.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table1.button(3).trigger();
			table2.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table1.button(4).trigger();
			table2.button(4).trigger();

		});
	};

	var initCatatanBK = function() {
		var table1 = $('#table_catatanbk').DataTable({
			responsive: true,
			ajax: {
				url: '/datacatatanbk',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin'},
			{data: 'nama_kelas'},
			{data: 'tanggal'},
			{data: 'catatan'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -8,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
						<button class='btn btn-sm btn-info update_cat_bk' data-id=`+full.id+` >Update</button>
						<button class='btn btn-sm btn-danger delete_cat_bk' data-id=`+full.id+` data-toggle='modal' data-target='#delete_catatan_bk' >Hapus</button>
						`;
				},
			},
			],
		});

		var table2 = $('#table_catatanbk_perkelas').DataTable({
			"scrollX": true,
			ajax: {
				url: '/datacatatanbkkelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: 6,
				title: 'Catatan Wali Kelas',
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<a href="/lihat_catatan_bk_perkelas/detail/`+full.id_master_kelas+`" >
					<button class='btn btn-sm btn-info'>Detail Siswa</button>
					</a>
					`;
				},
			}

			],
			'order': [[1, 'asc']]
		});

		var valuee7 = $('#id_master_kelas_bk').val();
		var table3 = $('#table_catatanbk_perkelassiswa').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'datacatatanbkperkelassiswa/'+valuee7,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: 'jumlah'},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="history/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		var table4 = $('#table_siswa_catatan_bk').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: '/datasiswacatatanbk',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'nama_guru',orderable:false},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/siswa_catatan_bk/detail/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_catatanbk').on('click', '.update_cat_bk', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#txt_userid').val(id);
			console.log(id);
			$.get('/catatan_bk/dataeditcatbk/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_catatan_bk').modal('show');
				$('#smoy1').val(data.data[0].nis + ' - ' + data.data[0].nama_siswa);
				$('.smoy2').val(data.data[0].tanggal);
				$('#smoy3').val(data.data[0].catatan);
			})
		});

		$('#table_catatanbk').on('click', '.delete_cat_bk', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idcat').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table1.button(0).trigger();
			table2.button(0).trigger();
			table3.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table2.button(1).trigger();
			table1.button(1).trigger();
			table3.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table1.button(2).trigger();
			table2.button(2).trigger();
			table3.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table1.button(3).trigger();
			table2.button(3).trigger();
			table3.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table1.button(4).trigger();
			table2.button(4).trigger();
			table3.button(4).trigger();
		});
	};

	return {
		init: function() {
			initTableUsers();
			initTableKelas();
			initTableGuruMapel();
			initTableMaster();
			initTableMappingKelas();
			initTableKelasSiswa();
			initTablesiswa2();
			initCatatanBK();
			initCatwalkes();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
});