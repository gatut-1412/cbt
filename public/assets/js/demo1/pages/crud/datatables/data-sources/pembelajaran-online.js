'use strict';
var PembelajaranDataSourceAjaxClient = function() {
	var a = function() {
		var table = $('#table_pembelajaran_perkelas').DataTable({
			responsive: true,
			ajax: {
				url: '/data_pembelajaranperkelas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			// {data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -6,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/daftartugas/tambah_tugas/`+full.id_master_kelas+`" >
					<button>Detail Tugas</button>
					</a>
					`;
				},
			},
			],
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var b = function() {
		var valuee2 = $('#id_master_kelas1').val();
		var table1 = $('#table_datatugas').DataTable({
			responsive: true,
			ajax: {
				type : 'GET',
				url: 'tambah_tugas/datatugas/'+valuee2,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			columns: [
			{data: 'no'},
			{data: 'judul_tugas'},
			{data: 'nama_pelajaran'},
			// {data: 'file'},
			{data: 'tanggal_tugas',orderable:false},
			{data: 'status_tugas1',orderable:false},
			{data: 'action_tugas',orderable:false},
			{data: 'Action'},
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -3,
				orderable: false,
				render: function(data, type, full, meta) {		

					return full.status_tugas + `<a href="datalihattugasguru/`+full.id_tugas_online+`"> (detail tugas)</a>`;
				},
			},
			{	
				targets: -2,
				title: 'Action Tugas',
				orderable: false,
				render: function(data, type, full, meta) {		

					if(full.status_tugas == 'DRAFT'){
						return `
						<a href="upatepublish/`+full.id_tugas_online+`" class="btn btn-warning btn-sm" >
						PUBLISH
						</a>
						`;
					}
					else if (full.status_tugas == 'PUBLISH')
					{
						return `
						<a href="upateselesai/`+full.id_tugas_online+`" class="btn btn-primary btn-sm" >
						SELESAI
						</a>
						`;
					}

					else if (full.status_tugas == 'EXPIRED')
					{
						return 'Selesai';
					}	
				},
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="detailtugas/`+full.id_tugas_online+`" class="btn btn-success btn-sm">
					Detail siswa
					</a>
					<a href="hapustugas/`+full.id_tugas_online+`" class="btn btn-danger btn-sm" onclick="return confirm('Apakah data akan dihapus ?');">
					Hapus
					</a>
					`;
				},
			},
			// {	
			// 	targets: -5,
			// 	orderable: false,
			// 	render: function(data, type, full, meta) {		

			// 		return full.file_upload1 + ',' + full.file_upload2 + ',' + full.file_upload3;
			// 	},
			// },
			],
			'order': [[1, 'asc']]
		});
		var table2 = $('#table_lihat').DataTable({
			"scrollX": true,
			ajax: {
				url: '/datalihattugas',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			columns: [
			{data: 'no'},
			{data: 'nama_pelajaran'},
			{data: 'judul_tugas'},
			{data: 'start_date1'},
			{data: 'end_date1'},
			{
				data: 'statusdto',
				render: function(data, type, full, meta) {	
					if ( data === 'SUDAH UPLOAD')	
					{
						// return data+` <a href="/data_file/upload_tugas/`+full.filesiswa+`" download>(lihat file)</a>`;
						return data+` <a href="/lihat_tugas/lihat_pengerjaan/`+full.id_tugas_online+`,`+full.id_detail_tugas_online+`">(lihat file)</a>`;
					}else{
						return data;
					}
				},
			},
			{data: 'Upload'},
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Detail',
				orderable: false,
				render: function(data, type, full, meta) {	
					if (full.status_tugas === 'PUBLISH')	
					{
						if (full.statusdto === 'SUDAH UPLOAD')	
						{
							return `
							<a href="/lihat_tugas/upload_tugas/`+full.id_tugas_online+`" 
							onclick="return confirm('Apakah anda yakin akan revisi jawaban soal ini ? Jika melakukan revisi jawaban anda yang sudah di upload sebelumnya akan hilang');" class="btn btn-brand btn-warning btn-icon-sm">
							Revisi
							</a>
							`;
						}else{
							return `
							<a href="/lihat_tugas/upload_tugas/`+full.id_tugas_online+`" class="btn btn-brand btn-elevate btn-icon-sm">
							Kerjakan
							</a>
							`;
						}
					}
					else
					{
						return 'Sudah Expired/Selesai';
					}
				},
			},
			],
			'order': [[1, 'asc']]
		});
		var valuee3 = $('#kelas3').val();
		var table3 = $('#table_detailtugas').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'detailtugas/data_detailsiswatugas/'+valuee3,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			// {data: 'jenis_kelamin',orderable:false},
			{data: 'status_tugas'},
			{data: 'detailstatus'},
			{data: 'tanggal_upload'},
			{data: 'lihatfile'}, 
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Lihat File',
				orderable: false,
				render: function(data, type, full, meta) {		
					if (full.detailstatus === 'SUDAH UPLOAD')
					{
						// return `
						// <a href="../data_file/upload_tugas/`+full.uploadsiswa+`" download class="btn btn-brand btn-elevate btn-icon-sm" >
						// Lihat File
						// </a>
						// `;
						return `
						<a href="lihat_pengerjaan_siswa/`+full.id_to+`,`+full.id_detail_tugas_online+`" class="btn btn-brand btn-elevate btn-icon-sm" >
						Lihat File
						</a>
						`;
					}
					else {
						return '<button type="button" class="btn btn-primary disabled" disabled="">Lihat File</button>';
					}
				},
			},
			],
			'order': [[1, 'asc']]
		});

		setInterval( function () {
    		table2.ajax.reload( null, false ); // user paging is not reset on reload
    	}, 10000 );
	};

	return {

		//main function to initiate the module
		init: function() {
			a();
			b();

		},

	};

}();

jQuery(document).ready(function() {
	PembelajaranDataSourceAjaxClient.init();
});