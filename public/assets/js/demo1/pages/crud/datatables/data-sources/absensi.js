'use strict';
var Absensi = function() {
	var absensi1 = function() {
		var table = $('#table_tambah_absen').DataTable({
			responsive: true,
			ajax: {
				url: '/dataabsensi',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'nama_kelas'},
			{data: 'type_absen'},
			{data: 'tanggal_absen'},
			{data: 'keterangan'},
			{data: 'Action', responsivePriority: -1},
			],
			columnDefs: [
			{	
				targets: -8,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
						<button class='btn btn-sm btn-info update_absensi' data-id=`+full.id_absensi+` >Update</button>
						<button class='btn btn-sm btn-danger delete_absensi' data-id=`+full.id_absensi+` data-toggle='modal' data-target='#delete_absensi' >Hapus</button>
					`;
				},
			},
			],
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#table_tambah_absen').on('click', '.update_absensi', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idabsensi').val(id);
			console.log(id);
			//alert(id);
			$.get('/absensi/dataeditabsensi/' + id + '/edit', function (data, type, full, meta)  {
				$('#submit').val("Edit category");
				$('#update_absensi').modal('show');
				$('#smoy1').val(data.data[0].nis + ' - ' + data.data[0].nama_siswa);
				$('.smoy2').val(data.data[0].type_absen);
				$('.smoy3').val(data.data[0].tanggal_absen);
				$('#smoy4').val(data.data[0].keterangan);
			})
		});

		$('#table_tambah_absen').on('click', '.delete_absensi', function (event) {
			event.preventDefault();
			var id = $(this).data('id');
			$('#idabsensi1').val(id);
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	var absensi2 = function() {
		var valuee3 = $('#id_master_kelas2').val();
		var table = $('#table_siswa_absensi').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'detail_absensi/datasiswaabsensi/'+valuee3,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: 'sakit1'},
			{data: 'ijin1'},
			{data: 'alpa1'},
			{data: 'jumlah'},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="detail_absensi/history/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		var table1 = $('#table_absensi').DataTable({
			responsive: true,
			ajax: {
				url: '/datakelassiswa',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'semester'},
			{data: 'nama_guru'},
			{data: 'jumlah'},
			{data: 'Action', responsivePriority: -1},		

			],
			columnDefs: [
			{	
				targets: -7,
				title: 'No',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},

			{	
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/detail_absensi/`+full.id_master_kelas+`" >
					<button class='btn btn-sm btn-info'>Detail Siswa</button>
					</a>
					`;
				},
			},
			],
		});

		var table2 = $('#table_siswa_absensi_siswa').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: '/datasiswaabsensisiswa',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'tahun_ajaran'},
			{data: 'nama_kelas'},
			{data: 'nama_guru',orderable:false},
			{data: 'Action'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function(data, type, full, meta) {		

					return `
					<a href="/siswa_absensi/detail/`+full.id_kelas_siswa+`" >
					<button class='btn btn-sm btn-info'>History</button>
					</a>
					`;
				},
			}],
			'order': [[1, 'asc']]
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
			table1.button(0).trigger();
			table2.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
			table1.button(1).trigger();
			table2.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
			table1.button(2).trigger();
			table2.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
			table1.button(3).trigger();
			table2.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
			table1.button(4).trigger();
			table2.button(4).trigger();

		});
	};

	var templatelama = function() {
		var valuee3 = $('#id_master_kelas2').val();
		var table = $('#table_siswa_absensi').DataTable({
			"scrollX": true,
			ajax: {
				type : 'GET',
				url: 'detail_absensi/datasiswaabsensi/'+valuee3,
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
			buttons: [
			'print',
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			],
			columns: [
			{data: 'no'},
			{data: 'nis'},
			{data: 'nama_siswa'},
			{data: 'jenis_kelamin',orderable:false},
			{data: 'Sakit'},
			{data: 'izin'},
			{data: 'Alpa'},
			{data: 'Jumlah'}
			],
			columnDefs: [
			{	
				targets: 0,
				title: 'no',
				orderable: true,
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{	
				targets: 4,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  class="form-control form-control-sm" name="1" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 5,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="2" maxlength="3" style="width: 50px;"></input>`;;
				},
			},
			{	
				targets: 6,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="3" maxlength="3" style="width: 50px;"></input>`;
				},
			},
			{	
				targets: 7,
				orderable: false,
				render: function(data, type, full, meta) {		
					return `
					<input type="text" class="form-control form-control-sm"  name="4" maxlength="3" style="width: 50px;"></input>`;
				},
			}

			],
			
		});

		$('#export_print').on('click', function(e) {
			e.preventDefault();
			table.button(0).trigger();
		});

		$('#export_copy').on('click', function(e) {
			e.preventDefault();
			table.button(1).trigger();
		});

		$('#export_excel').on('click', function(e) {
			e.preventDefault();
			table.button(2).trigger();
		});

		$('#export_csv').on('click', function(e) {
			e.preventDefault();
			table.button(3).trigger();
		});

		$('#export_pdf').on('click', function(e) {
			e.preventDefault();
			table.button(4).trigger();
		});
	};

	return {
		init: function() {
			absensi1();
			absensi2();
		},
	};

}();

jQuery(document).ready(function() {
	Absensi.init();
});