<?php
error_reporting(0);
$id = ($_GET[id]);
// $id = base64_decode($id1);
$today = date("d-M-Y");
require('fpdf/fpdf.php');
include "koneksi.php";
class PDF extends FPDF
{
	function Footer()
	{
        // Go to 1.5 cm from bottom
		$this->SetY(-15);
        // Select Arial italic 8
		$this->SetFont('Arial','I',8);
        // Print centered page number
		$this->Cell(0,10,'SMA MUHAMMADIYAH 1 PRAMBANAN',0,0,'R');
	}
}
$pdf = new PDF();
// $pdf=new PDF_ImageA+lpha();
$pdf->AddPage();
$image = 'logo.png';
$image1 = 'logo1.png';
$pdf->Image($image, 9, 10, 23.63);

// $pdf->Ln(3);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0,3,'MAJELIS PENDIDIKAN DASAR DAN MENENGAH PIMPINAN DAERAH MUHAMMADIYAH KABUPATEN SLEMAN ','0','1','C',false);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,6,'SMA MUHAMMADIYAH 1 PRAMBANAN','0','1','C',false);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,5,'TERAKREDITASI "A"','0','1','C',false);
$pdf->SetFont('Arial','B',9);
$pdf->Ln(2);
$pdf->Cell(0,2,'Gatak Rt 01/Rw 09, Bokoharjo, Prambanan Sleman, Yogyakarta 55572, Telp. (0274) 496 208','0','1','C',false);
$pdf->Cell(0,6,'Email. sma_muh1pramb@yahoo.com, Website : sma-muh1pramb.sch.id','0','1','C',false);
$pdf->Cell(190,0.6,'','0','1','C',true);
$pdf->Ln(3);

$pdf->SetFont('Arial','',10);

$pdf->Cell(30,5,'Mata Pelajaran :','0','0','L');
$pdf->Cell(50,5,'Matematika','0','0','L');

$pdf->Ln(8);


$sql = mysql_query("select * from tb_soal_cbt join tb_detail_soal_cbt on tb_soal_cbt.id_soal_cbt = tb_detail_soal_cbt.id_soal_cbt where tb_soal_cbt.id_soal_cbt='$id'");
$n = 0;
while($data = mysql_fetch_array($sql)) {
	$n++;

// $pdf->Cell(65,8,' Kode Pengusaha',0,0, 'L',1,false ) ;
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(5,5,$n.'.',0,0, 'L') ;
	$pdf->MultiCell(0,5,$data['soal_pg']. ' :','0','0');
	$pdf->SetFont('Arial','',9);

	$a = $data['pilihan_a'];
	$b = $data['pilihan_b'];
	$c = $data['pilihan_c'];
	$d = $data['pilihan_d'];
	$e = $data['pilihan_e'];

	$date_a =  substr($a,0,10);
	$date_b =  substr($b,0,10);
	$date_c =  substr($c,0,10);
	$date_d =  substr($d,0,10);
	$date_d =  substr($e,0,10);
	$format = 'd-m-Y' ;
	$dd = DateTime::createFromFormat($format, $date_a);
	$isdate = $dd && $dd->format($format) === $date_a;

	if ($isdate){

		$pdf->Cell(10,5,'A.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_a'] ,0,L ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'B.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_b']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'C.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_c']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'D.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_d']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'E.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_e']  ,0,'L' ) ;
		$pdf->Ln(2);
	}
	else{
		$pdf->Cell(10,5,'A.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_a'] ,0,L ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'B.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_b']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'C.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_c']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'D.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_d']  ,0,'L' ) ;
		$pdf->Ln(1);
		$pdf->Cell(10,5,'E.',0,0,R);
		$pdf->MultiCell(0,5, $data['pilihan_e']  ,0,'L' ) ;
		$pdf->Ln(2);
	}
}

$pdf->Footer();
$pdf->Output();
?>
