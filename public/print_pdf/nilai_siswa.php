<?php
error_reporting(0);
$id = ($_GET[id]);
// $id = base64_decode($id1);
$today = date("d-M-Y");
require('fpdf/fpdf.php');
include "koneksi.php";
class PDF extends FPDF
{
	function Footer()
	{
        // Go to 1.5 cm from bottom
		$this->SetY(-15);
        // Select Arial italic 8
		$this->SetFont('Arial','I',8);
        // Print centered page number
		$this->Cell(0,10,'SMA MUHAMMADIYAH 1 PRAMBANAN',0,0,'R');
	}
}
$pdf = new PDF();
// $pdf=new PDF_ImageA+lpha();
$pdf->AddPage();
$image = 'logo.png';
$image1 = 'logo1.png';
$pdf->Image($image, 9, 10, 23.63);

// $pdf->Ln(3);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0,3,'MAJELIS PENDIDIKAN DASAR DAN MENENGAH PIMPINAN DAERAH MUHAMMADIYAH KABUPATEN SLEMAN ','0','1','C',false);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,6,'SMA MUHAMMADIYAH 1 PRAMBANAN','0','1','C',false);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,5,'TERAKREDITASI "A"','0','1','C',false);
$pdf->SetFont('Arial','B',9);
$pdf->Ln(2);
$pdf->Cell(0,2,'Gatak Rt 01/Rw 09, Bokoharjo, Prambanan Sleman, Yogyakarta 55572, Telp. (0274) 496 208','0','1','C',false);
$pdf->Cell(0,6,'Email. sma_muh1pramb@yahoo.com, Website : sma-muh1pramb.sch.id','0','1','C',false);
$pdf->Cell(190,0.6,'','0','1','C',true);
$pdf->Ln(3);

$pdf->SetFont('Arial','',10);

$pdf->Cell(30,5,'Mata Pelajaran :','0','0','L');
$pdf->Cell(50,5,'Matematika','0','0','L');

$pdf->Ln(8);


$sql = mysql_query("select * from tb_soal_cbt join tb_detail_soal_cbt on tb_soal_cbt.id_soal_cbt = tb_detail_soal_cbt.id_soal_cbt where tb_soal_cbt.id_soal_cbt='$id'");
$sql1 = mysql_query("select nama_siswa,tr_ikut_ujian.list_jawaban, tr_ikut_ujian.list_soal from tb_mapping_cbt
	join tb_guru_mapel on tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
	join tb_master_kelas on tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
	join tb_pelajaran on tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
	join tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
	join tb_siswa on tb_kelas_siswa.id_siswa = tb_siswa.id_siswa
	join tb_soal_cbt on tb_soal_cbt.id_soal_cbt=tb_mapping_cbt.id_soal_cbt
	join tr_ikut_ujian as aa on aa.id_tes= tb_mapping_cbt.id_mapping_cbt
	join tr_ikut_ujian on tr_ikut_ujian.id_user=tb_siswa.id_siswa
	where tr_ikut_ujian.id='$id'
	GROUP by nama_siswa");
$n = 0;
while($data = mysql_fetch_array($sql1)) {
	

	//$listsoal = $data['list_soal'];
	$listjawaban = $data['list_jawaban'];
	// $arai1 = explode(",", $listsoal);
	$arai2 = explode(",", $listjawaban);

	for($i = 0; $i<count($arai2); $i++){
		$n++;
		$int = $arai2[$i];
		// var_dump($int); 
		$arai3 = explode(":", $int);
		// $arai3 = sort($arai3);
		// var_dump(count($arai3));
		for($s = 0; $s<count($arai3); $s++){
			if ($s % 2 == 0) {
				$soal1 = $arai3[$s];
				//var_dump($soal1);
				$sql2 = mysql_query("select id_detail_soal_cbt,soal_pg from tb_detail_soal_cbt where id_detail_soal_cbt = ('$soal1') ");
				while($data2 = mysql_fetch_array($sql2)) {
					//echo $data2['id_detail_soal_cbt']." ".$data2['soal_pg']."<br>";
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(5,5,$n.'.',0,0, 'L') ;
					$pdf->MultiCell(0,5,$data2['soal_pg']. ' :','0','0');
					$pdf->SetFont('Arial','',9);
				}
			}
			else{
				$jawaban = $arai3[$s];
				//var_dump($jawaban);
				$sql3 = mysql_query("select pilihan_a, pilihan_b, pilihan_c, pilihan_d, pilihan_e from tb_detail_soal_cbt where id_detail_soal_cbt = ('$soal1')");
				while($data3 = mysql_fetch_array($sql3)) {

					if($jawaban == 'A'){
						$pdf->Cell(10,5,"A",0,0,R);
						$pdf->MultiCell(0,5, $data3['pilihan_a'] ,0,L ) ;
						$pdf->Ln(2);
						// echo $data3['pilihan_a']."<br>";

					}else if($jawaban == 'B'){
						$pdf->Cell(10,5,"B",0,0,R);
						$pdf->MultiCell(0,5, $data3['pilihan_b'] ,0,L ) ;
						$pdf->Ln(2);
						// echo $data3['pilihan_b']."<br>";
					}else if($jawaban == 'C'){
						$pdf->Cell(10,5,"C",0,0,R);
						$pdf->MultiCell(0,5, $data3['pilihan_c'] ,0,L ) ;
						$pdf->Ln(2);
						// echo $data3['pilihan_c']."<br>";
					}else if($jawaban == 'D'){
						$pdf->Cell(10,5,"D",0,0,R);
						$pdf->MultiCell(0,5, $data3['pilihan_d'] ,0,L ) ;
						$pdf->Ln(2);
						// echo $data3['pilihan_d']."<br>";
					}else {
						$pdf->Cell(10,5,"E",0,0,R);
						$pdf->MultiCell(0,5, $data3['pilihan_e'] ,0,L ) ;
						$pdf->Ln(2);
						//echo $data3['pilihan_e']."<br>";
					}
					
				}
				// echo "<br>";
			}
		}
	}
}
// $pdf->Footer();
$pdf->Output();
?>
