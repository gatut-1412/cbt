<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//smoy
Route::get('/clear_cache', function() {
	Artisan::call('cache:clear');
	Artisan::call('config:clear');
	Artisan::call('config:cache');
	Artisan::call('view:clear');
   //Artisan::call('route:cache');
	Artisan::call('route:clear');

	return "Cleared!";
    // return what you want
});

//Route Tampill utama --smoy--
Route::get('/','RouteController@tampilUtama');
Route::get('/dashboardSiswa','RouteController@tampilSiswa');
Route::get('/dashboardGuru','RouteController@tampilGuru');

//Route Users --smoy--
Route::get('/users1','UsersController@tampilUsers1');
Route::get('/users','UsersController@tampilUsers');
Route::get('/datausers','UsersController@dataUsers');
Route::get('/datausersserver','UsersController@dataUsersServer');
Route::get('/users/tambahuser','UsersController@tambahUsers');
Route::post('/users/tambahuser/proses','UsersController@prosesTambahUsers');
// Route::get('/users/edit/{id}','UsersController@editUsers');
Route::get('/users/dataeditusers/{id}/edit','UsersController@fetchdata');
Route::post('/users/update','UsersController@prosesEditUsers');

//Route Kelas --smoy--
Route::get('/kelas','KelasController@tampilKelas');
Route::get('/datakelas','KelasController@dataKelas');
Route::post('/kelas/tambahkelas','KelasController@tambahKelas');
Route::get('/kelas/edit/{id}','UsersController@editkelas');

Route::get('/masterkelas','MasterKelasController@tampilMasterKelas');
Route::get('/datamasterkelas','MasterKelasController@dataMasterKelas');
Route::post('/masterkelas/tambahmasterkelas','MasterKelasController@tambahMaterKelas');
Route::get('/kelas/edit/{id}','UsersController@editkelas');

//Route Tahun Ajaran --smoy--
Route::get('/tahunajaran','TahunAjaranController@tampilTahunAjaran');
Route::get('/datatahunajaran','TahunAjaranController@dataTahunAjaran');
Route::post('/tahunajaran/tambahtahunajaran','TahunAjaranController@tambahTahunAjaran');
Route::get('/tahunajaran/dataedittahunajaran/{id}/edit','TahunAjaranController@fetchdata');
Route::post('/tahunajaran/update','TahunAjaranController@updateTahunAjaran');
Route::post('/tahunajaran/delete','TahunAjaranController@deleteTahunAjaran');
//Route::resource('/TahunAjaran', 'TAController');

//Route Eskul --smoy--
Route::get('/eskul','EskulController@tampilEskul');
Route::get('/dataeskul','EskulController@dataEskul');
Route::post('/eskul/tambah','EskulController@tambahEskul');
Route::get('/eskul/dataediteskul/{id}/edit','EskulController@fetchdata');
Route::post('/eskul/update','EskulController@updateEskul');
Route::post('/eskul/delete','EskulController@deleteEskul');
Route::get('/mappingeskul','EskulController@tampilMappingEskul');
Route::get('/datamappingeskul','EskulController@dataMappingEskul');
Route::post('/mappingeskul/tambah','EskulController@tambahMappingEskul');
Route::get('/mappingeskul/dataeditmappingeskul/{id}/edit','EskulController@fetchdata2');
Route::post('/mappingeskul/update','EskulController@updateMappingEskul');
Route::post('/mappingeskul/delete','EskulController@deleteMappingEskul');
Route::get('/mappingeskul_perkelas','EskulController@tampilMappingEskulPerkelas');
Route::get('/datamappingeskulperkelas','EskulController@dataMappingEskulPerkelas');
Route::get('/mappingeskul_perkelas/detail/{id}','EskulController@tampilMappingEskulPerkelasSiswa');
Route::get('/mappingeskul_perkelas/datamappingeskulperkelassiswa/{id}','EskulController@dataMappingEskulPerkelasSiswa');
Route::get('/mappingeskul_perkelas/history/{id}','EskulController@historyEskul');
//catatan BK hal siswa siswa
Route::get('/siswa_eskul','EskulController@siswaEskul');
Route::get('/datasiswaeskul','EskulController@dataSiswaEskul');
Route::get('/siswa_eskul/detail/{id}','EskulController@siswaEskuldetail');

//Route Pembelajaran Online --smoy--
Route::get('/daftartugas','PembelajaranOnlineController@tampilDafttarTugas');
Route::get('/data_pembelajaranperkelas','PembelajaranOnlineController@dataPembelajaranPerkelas');
Route::get('/daftartugas/tambah_tugas/{id}','PembelajaranOnlineController@tambahTugas'); 
Route::get('/daftartugas/tambah_tugas/datatugas/{id}','PembelajaranOnlineController@dataTugasOnline'); //bin
Route::get('/datalihattugas','PembelajaranOnlineController@dataTugaslihatSiswa');
Route::get('/daftartugas/datalihattugasguru/{iddto2}','PembelajaranOnlineController@detailTugasGuru');
Route::post('/daftartugas/tambah_tugas/proses','PembelajaranOnlineController@TambahTugasProses');
Route::get('/daftartugas/detailtugas/{id2}','PembelajaranOnlineController@lihatDetailTugasGuru');
Route::get('/lihat_tugas','PembelajaranOnlineController@lihatTugasSiswa'); 
Route::get('/lihat_tugas/upload_tugas/{id3}','PembelajaranOnlineController@uploadTugasSiswa');
Route::get('/daftartugas/data_detailsiswatugas','PembelajaranOnlineController@dataDetailSiswaTugas');
Route::get('/daftartugas/detailtugas/data_detailsiswatugas/{id}','PembelajaranOnlineController@dataDetailSiswaTugas');
Route::post('/lihat_tugas/uploadproses','PembelajaranOnlineController@updateUploadTugas'); 
Route::get('/lihat_tugas/lihat_pengerjaan/{idto},{iddet}','PembelajaranOnlineController@lihatPengerjaan'); 
Route::get('/daftartugas/lihat_pengerjaan_siswa/{idto},{iddet}','PembelajaranOnlineController@lihatPengerjaanSiswa'); 
Route::get('/daftartugas/upatepublish/{id}','PembelajaranOnlineController@updateKePublish');
Route::get('/daftartugas/upateselesai/{id2}','PembelajaranOnlineController@updateKeSelesai'); 
Route::get('/daftartugas/hapustugas/{id}','PembelajaranOnlineController@hapusTugas');

//Route Mapping Kelas --smoy--datasiswa2
Route::get('/mappingkelas','MappingKelasController@tampilMappingKelas')->name('tahunajaran');;
Route::get('/datamappingkelas','MappingKelasController@dataMappingKelas');
Route::post('/mappingkelas/tambah','MappingKelasController@TambahMappingKelas');
Route::get('/mappingkelas/dataeditmappingkelas/{id}/edit', 'MappingKelasController@fetchdata');
Route::post('/mappingkelas/delete','MappingKelasController@deleteMappingKelas');
Route::post('/mappingkelas/update','MappingKelasController@updateMappingKelas');
// Route::resource('MappingKelas', 'MapKelasController');


//Route Kelas Siswa --smoy--
Route::get('/kelassiswa','KelasSiswaController@tampilKelasSiswa');
Route::get('/datakelassiswa','KelasSiswaController@dataKelasSiswa');
Route::get('/kelassiswa/edit/datasiswa2/{id}','KelasSiswaController@dataSiswa2');
Route::post('/kelassiswa/updateKelasSiswa2','KelasSiswaController@updateKelasSiswa2');
Route::post('/kelassiswa/tambahkelassiswa','KelasSiswaController@TambahKelasSiswa');
Route::get('/kelassiswa/edit/{id}','KelasSiswaController@editKelasSiswa');
Route::post('/kelassiswa/edit/{id}','KelasSiswaController@editKelasSiswa');

//smoy
Route::get('back','SiswaController@back');

//Route guru mapel --robin--
Route::get('/gurumapel','GuruMapelController@tampilGuruMapel');
Route::get('/datagurumapel','GuruMapelController@dataGuruMapel');
Route::post('/gurumapel/updategurumapel','GuruMapelController@updateGuruMapel');
Route::get('/gurumapel/hapus/{id}','GuruMapelController@hapusGuruMapel');
//smoy
Route::post('/gurumapel/tambah','GuruMapelController@tambahGuruMapel');
Route::get('/gurumapel/edit/{id}','GuruMapelController@editGuruMapel');

//Route siswa --smoy--
Route::get('/siswa','SiswaController@tampilSiswa');
Route::get('/datasiswa','SiswaController@dataSiswa');
Route::get('/siswa/tambahsiswa','SiswaController@tambahSiswa');
Route::post('/siswa/tambahsiswa/proses','SiswaController@prosesTambahSiswa');
Route::get('/siswa/edit/{id}','SiswaController@editSiswa');
Route::get('/siswa/back','SiswaController@backedit');
Route::post('/siswa/edit/proses','SiswaController@editSiswaProses');
Route::post('/siswa/lulus','SiswaController@lulusSiswa');
Route::post('/siswa/nonactive','SiswaController@nonactiveSiswa');

//Route guru --smoy--
Route::get('/guru','GuruController@tampilGuru');
Route::get('/dataguru','GuruController@dataGuru');
Route::get('/guru/tambahguru','GuruController@tambahGuru');
Route::post('/guru/tambahguru/proses','GuruController@prosesTambahGuru');
Route::get('/guru/edit/{id}','GuruController@editGuru');
Route::get('/guru/back','GuruController@backedit');
Route::post('/guru/edit/proses','GuruController@editGuruProses');
Route::post('/guru/nonactive','GuruController@nonactiveGuru');

//Route penilaian --smoy--
Route::get('/penilaian','PenilaianController@tampilPenilaian');
// Route::get('/penilaian/datasiswapenilaian','PenilaianController@dataSiswaPenilaian');
// Route::get('/datasiswapenilaian','PenilaianController@dataSiswaPenilaian');
Route::get('/penilaian/edit/datasiswapenilaian/{id}','PenilaianController@dataSiswaPenilaian');//bin
Route::get('/dataguru','GuruController@dataGuru');
Route::get('/guru/tambahguru','GuruController@tambahGuru');
Route::post('/guru/tambahguru/proses','GuruController@prosesTambahGuru');
Route::get('/penilaian/edit/{id}','PenilaianController@editPenilaian');

//Route absensi --smoy--
Route::get('/absensi','AbsensiController@tampilAbsensi');
Route::get('/absensi_perkelas','AbsensiController@tampilAbsensiPerkelas');
Route::post('/absensi/tambah','AbsensiController@proses');
Route::get('/detail_absensi/datasiswaabsensi/{id}','AbsensiController@dataSiswaAbsensi'); 
Route::get('/dataabsensi','AbsensiController@dataAbsensi');
Route::get('/detail_absensi/{id}','AbsensiController@detailAbsensi');
Route::get('/detail_absensi/history/{id}','AbsensiController@historyAbsensi');
Route::get('/absensi/dataeditabsensi/{id}/edit', 'AbsensiController@fetchdata');
Route::post('/absensi/delete','AbsensiController@deleteAbsensi');
Route::post('/absensi/update','AbsensiController@updateAbsensi');
//catatan walkes hal siswa siswa
Route::get('/siswa_absensi','AbsensiController@siswaAbsensi');
Route::get('/datasiswaabsensisiswa','AbsensiController@dataSiswaAbsensiSiswa');
Route::get('/siswa_absensi/detail/{id}','AbsensiController@siswaAbsensiDetail');

//Route Catatan Walkes --smoy--
Route::get('/catatan_walikelas','CatatanWalkesController@tampilCatatanWalkes');
Route::post('/catatan_walikelas/tambah','CatatanWalkesController@prosesTambah');
Route::get('/datakelassiswa_catatanwalkes','CatatanWalkesController@dataCatatanWalkesPerkelas');
Route::get('/catatan_walikelas/detail/datacatatanwalikelas/{id}','CatatanWalkesController@dataCatatanWalkes');
Route::post('/catatan_walikelas/detail/datacatatanwalikelas/{id}','CatatanWalkesController@dataCatatanWalkes');
Route::get('/catatan_walikelas/detail/{id}','CatatanWalkesController@detailCatatanWalkes');
// Route::get('/ajaxdata/fetchdata', 'CatatanWalkesController@fetchdata')->name('ajaxdata.fetchdata');
// Route::get('/catatan_walikelas/asin','CatatanWalkesController@fetchdata');
Route::get('/catatan_walikelas/asin/{id}/edit', 'CatatanWalkesController@fetchdata');
Route::post('/catatan_walikelas/update','CatatanWalkesController@updateCatatanWalkes');
Route::post('/catatan_walikelas/delete','CatatanWalkesController@deleteCatatanWalkes');
//catatan walkes hal siswa siswa
Route::get('/siswa_catatan_walkes','CatatanWalkesController@siswaCatatanWalkes');
Route::get('/datasiswacatatanwalkes','CatatanWalkesController@dataSiswaCatatanWalkes');
Route::get('/siswa_catatan_walkes/detail/{id}','CatatanWalkesController@siswaCatatanWalkesdetail');

//Route Catatan BK --smoy--
Route::get('/catatan_bk','CatatanBKController@tampilCatatanBK1');
Route::get('/lihat_catatan_bk_perkelas','CatatanBKController@lihatCatBKPerkelas');
Route::get('/lihat_catatan_bk_perkelas/detail/{id}','CatatanBKController@lihatCatBKPerkelasSiswa');
Route::get('/lihat_catatan_bk_perkelas/datacatatanbkperkelassiswa/{id}','CatatanBKController@dataCatatanBKPerKelasSiswa');
Route::get('/lihat_catatan_bk_perkelas/history/{id}','CatatanBKController@historyCatatanBK');
Route::get('/datacatatanbkkelas','CatatanBKController@dataCatatanBKPerKelas');
Route::get('/catatan_bk/datacatatanbk','CatatanBKController@dataCatatan');
Route::get('/datacatatanbk','CatatanBKController@dataCatatan');
Route::post('/catatan_bk/tambah','CatatanBKController@tambahCatatanBK');
Route::get('/catatan_bk/dataeditcatbk/{id}/edit', 'CatatanBKController@fetchdata');
Route::post('/catatan_bk/delete','CatatanBKController@deleteCatatanBK');
Route::post('/catatan_bk/update','CatatanBKController@updateCatatanBK');
//catatan BK hal siswa siswa
Route::get('/siswa_catatan_bk','CatatanBKController@siswaCatatanBK');
Route::get('/datasiswacatatanbk','CatatanBKController@dataSiswaCatatanBK');
Route::get('/siswa_catatan_bk/detail/{id}','CatatanBKController@siswaCatatanBKdetail');


//Route Prestasi --smoy--
Route::get('/prestasi','PrestasiController@tampilPrestasi');
Route::get('/lihat_prestasi','PrestasiController@lihatPrestasiPerkelas');
Route::get('/dataprestasi','PrestasiController@dataPrestasi');
Route::get('/dataprestasiperkelas','PrestasiController@dataPrestasiPerKelas');
Route::post('/prestasi/tambah','PrestasiController@tambahPrestasi');
Route::post('/prestasi/update','PrestasiController@updatePrestasi');
Route::post('/prestasi/delete','PrestasiController@deletePrestasi');
Route::get('/lihat_prestasi/detail/{id}','PrestasiController@prestasiKelasPersiswa');
Route::get('/lihat_prestasi/dataprestasiperkelassiswa/{id}','PrestasiController@dataPrestasiPerKelasSiswa');
Route::get('/lihat_prestasi/detailpersiswa/{id}','PrestasiController@detailPersiswaPrestasi');
Route::get('/dataprestasisiswa/{id}/edit','PrestasiController@dataUpdatePrestasi');
//catatan Prestasi hal siswa siswa
Route::get('/siswa_prestasi','PrestasiController@siswaPrestasi');
Route::get('/datasiswaprestasi','PrestasiController@dataSiswaPrestasi');
Route::get('/siswa_prestasi/detail/{id}','PrestasiController@siswaPrestasiDetail');

//Route cbt --smoy--
Route::get('/soalcbt','CbtController@tampilSoalCbt');
Route::get('/datasoalcbt','CbtController@dataSOalCbt');
Route::get('/soalcbt/datadetailsoalcbt/{que}','CbtController@dataDetailSOalCbt');
Route::get('/soalcbt/datadetailsoalcbtesay/{que}','CbtController@dataDetailSOalCbtEsay');
Route::post('/soalcbt/tambah','CbtController@tambahSoalCbt'); 
Route::get('/soalcbt/tambahdetailsoalcbt/{que}','CbtController@tambahDetailSoalCbt');
Route::get('/soalcbt/editsoalpgcbt/{id}','CbtController@editsoalpgcbt');
Route::post('/soalcbt/tambahdetailsoalcbt/prosespg','CbtController@tambahSoalCbtDetailPg'); 
Route::post('/soalcbt/editdetailsoalcbt/prosespg','CbtController@editSoalCbtDetailPg'); 
Route::post('/soalcbt/tambahdetailsoalcbt/prosesesay','CbtController@tambahSoalCbtDetailEsay'); 
Route::post('/soalcbt/tambahdetailsoalcbt/import','CbtController@importSoalCbt'); 
Route::get('/soalcbt/tambahdetailsoalcbt/hapus/{id_detail}','CbtController@hapusSoalCbtDetailPg'); 
Route::get('/soalcbt/tambahdetailsoalcbt/hapussemua/{id}','CbtController@hapusSemuaSoalPg'); 

//mappingan cbt perkelas --smoy
Route::get('/mapingan_cbt_perkelas','MappingCbtController@tampilMapinganCbtPerkelas');
Route::post('/mapingan_cbt_perkelas/tambah','MappingCbtController@tambahMappingCbt'); 
Route::get('/datamappingcbt','MappingCbtController@dataMappingCbt');
Route::get('/mapingan_cbt_perkelas/upatepublish/{id}','MappingCbtController@updateKePublish');
Route::get('/mapingan_cbt_perkelas/upateselesai/{id2}','MappingCbtController@updateKeSelesai'); 
Route::get('/mapingan_cbt_perkelas/hapus/{id}','MappingCbtController@hapusMappingCbt'); 

//Route matPelajaran --smoy--
Route::get('/matpelajaran','MatPelajaranController@tampilMatPelajaran');
Route::get('/datamatpelajaran','MatPelajaranController@dataMatPelajaran');
Route::post('/matpelajaran/tambah','MatPelajaranController@tambahMatPelajaran');
Route::get('/matpelajaran/dataeditmatpel/{id}/edit','MatPelajaranController@fetchdata');
Route::post('/matpelajaran/update','MatPelajaranController@updateMatpel');
Route::post('/matpelajaran/delete','MatPelajaranController@deleteMatpel');

//Route Service --smoy--
Route::get('/services','ServicesController@tampilServices');
Route::get('/dataservices','ServicesController@dataServices');
Route::get('/services/tambahservices','ServicesController@tambahUsers');
Route::post('/users/tambahservices/proses','ServicesController@prosesTambahUsers');
Route::get('/services/edit/{id}','ServicesController@editServices');
Route::get('/services/approve/{id}','ServicesController@approveServices');
Route::get('/services/reject/{id}','ServicesController@rejectServices');
Route::get('/services/hapus/{id}','ServicesController@hapusServices');

//Route kelmatpel --smoy--
Route::get('/kelmatpelajaran','KelMatPelajaranController@tampilKelMatPelajaran');
Route::get('/datakelmatpelajaran','KelMatPelajaranController@dataKelMatPelajaran');
Route::post('/kelmatpelajaran/tambah','KelMatPelajaranController@tambaKelhMatPelajaran');
Route::get('/kelmatpelajaran/dataeditkelmatpel/{id}/edit','KelMatPelajaranController@fetchdata');
Route::post('/kelmatpelajaran/update','KelMatPelajaranController@updateKelMatpel');
Route::post('/kelmatpelajaran/delete','KelMatPelajaranController@deleteKelMatpel');

//Route Member --smoy--
Route::get('/members','MembersController@tampilMembers');
Route::get('/datamembers','MembersController@dataMembers');
Route::get('/members/tambahcategory','MembersController@tambahMembers');
Route::post('/members/tambahcategory/proses','MembersController@prosesTambahMembers');
Route::get('/members/edit/{id}','MembersController@editMembers');
Route::get('/members/hapus/{id}','MembersController@hapusMembers');

//smoy
Route::post('/guru/import', 'GuruController@import');
Route::post('/siswa/import', 'SiswaController@import');

//smoy
Route::get('/format_upload','FormatUploadController@tampilFormatUpload');
Route::post('/format_upload/proses','FormatUploadController@tambaKelhMatPelajaran');
Route::get('/format_upload/edit/{id}','FormatUploadController @editKelMatPelajaran');

//login robin
//Route::get('/dashboard', 'LoginController@index');
Route::get('/login','LoginController@login');
Route::get('/gantiPassword','LoginController@gantiPassword');
Route::post('gantiPassword/proses','LoginController@updatePassword');
Route::post('/loginPost', 'LoginController@loginPost');
Route::get('/logout', 'LoginController@logout');

//smoy
Route::get('/pengumuman_cbt','PengumumanCbtController@tampilPengumumanCbt');
Route::get('/datapengumumancbt','PengumumanCbtController@dataPengumumanCbt');
Route::get('/pengumuman_cbt/detailpengumumancbt/{id}','PengumumanCbtController@detailPengumumanCbt');

//smoy
Route::get('/lihat_hasil_ujian_cbt','HasilUjianCbtController@tampilHasilUjianCbt');
Route::get('/datahasilujian','HasilUjianCbtController@dataHasilUjian');
Route::get('/lihat_hasil_ujian_cbt/detailsiswa/{id}','HasilUjianCbtController@detailHasilUjian');
Route::get('/lihat_hasil_ujian_cbt/datadetailhasilujian/{id}','HasilUjianCbtController@dataDetailHasilUjianCbt');
Route::get('soalcbt/laporan_soal/{idsoal}','PrintController@generatePDF');
Route::get('lihat_hasil_ujian_cbt/detailsiswa/laporan_jawabansiswa/{idujian}','PrintController@generatePDFJawabanSiswa');

//profil
Route::get('/profilsiswa','ProfilController@profilSiswa');
Route::post('/profilsiswa/update','ProfilController@updateProfilSiswa');
Route::get('/profilsiswa/print/{id}','PrintController@generatePDFSiswa');
Route::get('/profilguru/print/{id}','PrintController@generatePDFGuru');
Route::get('/profilguru','ProfilController@profilGuru');
Route::post('/profilguru/update','ProfilController@updateProfilGuru');

//scheduller
Route::get('/runscheduler','UtilsController@scheduler');


